package org.pragmaticmodeling.pxmanual.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.pragmaticmodeling.pxmanual.services.PxManualGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalPxManualParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_INT", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'top'", "'middle'", "'bottom'", "'baseline'", "'document'", "'id:'", "'firstHeadingLevel:'", "'{'", "'}'", "'toc'", "'maxLevel:'", "'list'", "'item'", "'samePage'", "'table'", "'style:'", "'className:'", "'row'", "'cell'", "'span:'", "'width:'", "'vAlign:'", "'image'", "'section'", "'page'", "'example'", "'screenshot:'", "'usage'", "'@'", "'class:'", "'keyword'", "'doc:'", "'prettyTitle:'", "'conclusion:'", "'attribute'", "'possibleValues:'", "'optional'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=6;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalPxManualParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPxManualParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPxManualParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPxManual.g"; }


    	private PxManualGrammarAccess grammarAccess;

    	public void setGrammarAccess(PxManualGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleDocument"
    // InternalPxManual.g:53:1: entryRuleDocument : ruleDocument EOF ;
    public final void entryRuleDocument() throws RecognitionException {
        try {
            // InternalPxManual.g:54:1: ( ruleDocument EOF )
            // InternalPxManual.g:55:1: ruleDocument EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleDocument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDocument"


    // $ANTLR start "ruleDocument"
    // InternalPxManual.g:62:1: ruleDocument : ( ( rule__Document__Group__0 ) ) ;
    public final void ruleDocument() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:66:2: ( ( ( rule__Document__Group__0 ) ) )
            // InternalPxManual.g:67:2: ( ( rule__Document__Group__0 ) )
            {
            // InternalPxManual.g:67:2: ( ( rule__Document__Group__0 ) )
            // InternalPxManual.g:68:3: ( rule__Document__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getGroup()); 
            }
            // InternalPxManual.g:69:3: ( rule__Document__Group__0 )
            // InternalPxManual.g:69:4: rule__Document__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Document__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDocument"


    // $ANTLR start "entryRuleDocBody"
    // InternalPxManual.g:78:1: entryRuleDocBody : ruleDocBody EOF ;
    public final void entryRuleDocBody() throws RecognitionException {
        try {
            // InternalPxManual.g:79:1: ( ruleDocBody EOF )
            // InternalPxManual.g:80:1: ruleDocBody EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocBodyRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleDocBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocBodyRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDocBody"


    // $ANTLR start "ruleDocBody"
    // InternalPxManual.g:87:1: ruleDocBody : ( ( rule__DocBody__Group__0 ) ) ;
    public final void ruleDocBody() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:91:2: ( ( ( rule__DocBody__Group__0 ) ) )
            // InternalPxManual.g:92:2: ( ( rule__DocBody__Group__0 ) )
            {
            // InternalPxManual.g:92:2: ( ( rule__DocBody__Group__0 ) )
            // InternalPxManual.g:93:3: ( rule__DocBody__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocBodyAccess().getGroup()); 
            }
            // InternalPxManual.g:94:3: ( rule__DocBody__Group__0 )
            // InternalPxManual.g:94:4: rule__DocBody__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DocBody__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocBodyAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDocBody"


    // $ANTLR start "entryRuleManualElement"
    // InternalPxManual.g:103:1: entryRuleManualElement : ruleManualElement EOF ;
    public final void entryRuleManualElement() throws RecognitionException {
        try {
            // InternalPxManual.g:104:1: ( ruleManualElement EOF )
            // InternalPxManual.g:105:1: ruleManualElement EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getManualElementRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleManualElement();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getManualElementRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleManualElement"


    // $ANTLR start "ruleManualElement"
    // InternalPxManual.g:112:1: ruleManualElement : ( ( rule__ManualElement__Alternatives ) ) ;
    public final void ruleManualElement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:116:2: ( ( ( rule__ManualElement__Alternatives ) ) )
            // InternalPxManual.g:117:2: ( ( rule__ManualElement__Alternatives ) )
            {
            // InternalPxManual.g:117:2: ( ( rule__ManualElement__Alternatives ) )
            // InternalPxManual.g:118:3: ( rule__ManualElement__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getManualElementAccess().getAlternatives()); 
            }
            // InternalPxManual.g:119:3: ( rule__ManualElement__Alternatives )
            // InternalPxManual.g:119:4: rule__ManualElement__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ManualElement__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getManualElementAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleManualElement"


    // $ANTLR start "entryRuleToc"
    // InternalPxManual.g:128:1: entryRuleToc : ruleToc EOF ;
    public final void entryRuleToc() throws RecognitionException {
        try {
            // InternalPxManual.g:129:1: ( ruleToc EOF )
            // InternalPxManual.g:130:1: ruleToc EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTocRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleToc();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTocRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleToc"


    // $ANTLR start "ruleToc"
    // InternalPxManual.g:137:1: ruleToc : ( ( rule__Toc__Group__0 ) ) ;
    public final void ruleToc() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:141:2: ( ( ( rule__Toc__Group__0 ) ) )
            // InternalPxManual.g:142:2: ( ( rule__Toc__Group__0 ) )
            {
            // InternalPxManual.g:142:2: ( ( rule__Toc__Group__0 ) )
            // InternalPxManual.g:143:3: ( rule__Toc__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTocAccess().getGroup()); 
            }
            // InternalPxManual.g:144:3: ( rule__Toc__Group__0 )
            // InternalPxManual.g:144:4: rule__Toc__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Toc__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTocAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleToc"


    // $ANTLR start "entryRuleList"
    // InternalPxManual.g:153:1: entryRuleList : ruleList EOF ;
    public final void entryRuleList() throws RecognitionException {
        try {
            // InternalPxManual.g:154:1: ( ruleList EOF )
            // InternalPxManual.g:155:1: ruleList EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleList();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getListRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleList"


    // $ANTLR start "ruleList"
    // InternalPxManual.g:162:1: ruleList : ( ( rule__List__Group__0 ) ) ;
    public final void ruleList() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:166:2: ( ( ( rule__List__Group__0 ) ) )
            // InternalPxManual.g:167:2: ( ( rule__List__Group__0 ) )
            {
            // InternalPxManual.g:167:2: ( ( rule__List__Group__0 ) )
            // InternalPxManual.g:168:3: ( rule__List__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListAccess().getGroup()); 
            }
            // InternalPxManual.g:169:3: ( rule__List__Group__0 )
            // InternalPxManual.g:169:4: rule__List__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__List__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getListAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleList"


    // $ANTLR start "entryRuleListItem"
    // InternalPxManual.g:178:1: entryRuleListItem : ruleListItem EOF ;
    public final void entryRuleListItem() throws RecognitionException {
        try {
            // InternalPxManual.g:179:1: ( ruleListItem EOF )
            // InternalPxManual.g:180:1: ruleListItem EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListItemRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleListItem();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getListItemRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleListItem"


    // $ANTLR start "ruleListItem"
    // InternalPxManual.g:187:1: ruleListItem : ( ( rule__ListItem__Group__0 ) ) ;
    public final void ruleListItem() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:191:2: ( ( ( rule__ListItem__Group__0 ) ) )
            // InternalPxManual.g:192:2: ( ( rule__ListItem__Group__0 ) )
            {
            // InternalPxManual.g:192:2: ( ( rule__ListItem__Group__0 ) )
            // InternalPxManual.g:193:3: ( rule__ListItem__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListItemAccess().getGroup()); 
            }
            // InternalPxManual.g:194:3: ( rule__ListItem__Group__0 )
            // InternalPxManual.g:194:4: rule__ListItem__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ListItem__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getListItemAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleListItem"


    // $ANTLR start "entryRuleSamePage"
    // InternalPxManual.g:203:1: entryRuleSamePage : ruleSamePage EOF ;
    public final void entryRuleSamePage() throws RecognitionException {
        try {
            // InternalPxManual.g:204:1: ( ruleSamePage EOF )
            // InternalPxManual.g:205:1: ruleSamePage EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSamePageRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleSamePage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSamePageRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSamePage"


    // $ANTLR start "ruleSamePage"
    // InternalPxManual.g:212:1: ruleSamePage : ( ( rule__SamePage__Group__0 ) ) ;
    public final void ruleSamePage() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:216:2: ( ( ( rule__SamePage__Group__0 ) ) )
            // InternalPxManual.g:217:2: ( ( rule__SamePage__Group__0 ) )
            {
            // InternalPxManual.g:217:2: ( ( rule__SamePage__Group__0 ) )
            // InternalPxManual.g:218:3: ( rule__SamePage__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSamePageAccess().getGroup()); 
            }
            // InternalPxManual.g:219:3: ( rule__SamePage__Group__0 )
            // InternalPxManual.g:219:4: rule__SamePage__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SamePage__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSamePageAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSamePage"


    // $ANTLR start "entryRuleTable"
    // InternalPxManual.g:228:1: entryRuleTable : ruleTable EOF ;
    public final void entryRuleTable() throws RecognitionException {
        try {
            // InternalPxManual.g:229:1: ( ruleTable EOF )
            // InternalPxManual.g:230:1: ruleTable EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleTable();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTable"


    // $ANTLR start "ruleTable"
    // InternalPxManual.g:237:1: ruleTable : ( ( rule__Table__Group__0 ) ) ;
    public final void ruleTable() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:241:2: ( ( ( rule__Table__Group__0 ) ) )
            // InternalPxManual.g:242:2: ( ( rule__Table__Group__0 ) )
            {
            // InternalPxManual.g:242:2: ( ( rule__Table__Group__0 ) )
            // InternalPxManual.g:243:3: ( rule__Table__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getGroup()); 
            }
            // InternalPxManual.g:244:3: ( rule__Table__Group__0 )
            // InternalPxManual.g:244:4: rule__Table__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Table__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTable"


    // $ANTLR start "entryRuleRow"
    // InternalPxManual.g:253:1: entryRuleRow : ruleRow EOF ;
    public final void entryRuleRow() throws RecognitionException {
        try {
            // InternalPxManual.g:254:1: ( ruleRow EOF )
            // InternalPxManual.g:255:1: ruleRow EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleRow();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRow"


    // $ANTLR start "ruleRow"
    // InternalPxManual.g:262:1: ruleRow : ( ( rule__Row__Group__0 ) ) ;
    public final void ruleRow() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:266:2: ( ( ( rule__Row__Group__0 ) ) )
            // InternalPxManual.g:267:2: ( ( rule__Row__Group__0 ) )
            {
            // InternalPxManual.g:267:2: ( ( rule__Row__Group__0 ) )
            // InternalPxManual.g:268:3: ( rule__Row__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getGroup()); 
            }
            // InternalPxManual.g:269:3: ( rule__Row__Group__0 )
            // InternalPxManual.g:269:4: rule__Row__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Row__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRow"


    // $ANTLR start "entryRuleCell"
    // InternalPxManual.g:278:1: entryRuleCell : ruleCell EOF ;
    public final void entryRuleCell() throws RecognitionException {
        try {
            // InternalPxManual.g:279:1: ( ruleCell EOF )
            // InternalPxManual.g:280:1: ruleCell EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleCell();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCell"


    // $ANTLR start "ruleCell"
    // InternalPxManual.g:287:1: ruleCell : ( ( rule__Cell__Group__0 ) ) ;
    public final void ruleCell() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:291:2: ( ( ( rule__Cell__Group__0 ) ) )
            // InternalPxManual.g:292:2: ( ( rule__Cell__Group__0 ) )
            {
            // InternalPxManual.g:292:2: ( ( rule__Cell__Group__0 ) )
            // InternalPxManual.g:293:3: ( rule__Cell__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getGroup()); 
            }
            // InternalPxManual.g:294:3: ( rule__Cell__Group__0 )
            // InternalPxManual.g:294:4: rule__Cell__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Cell__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCell"


    // $ANTLR start "entryRuleImage"
    // InternalPxManual.g:303:1: entryRuleImage : ruleImage EOF ;
    public final void entryRuleImage() throws RecognitionException {
        try {
            // InternalPxManual.g:304:1: ( ruleImage EOF )
            // InternalPxManual.g:305:1: ruleImage EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleImage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // InternalPxManual.g:312:1: ruleImage : ( ( rule__Image__Group__0 ) ) ;
    public final void ruleImage() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:316:2: ( ( ( rule__Image__Group__0 ) ) )
            // InternalPxManual.g:317:2: ( ( rule__Image__Group__0 ) )
            {
            // InternalPxManual.g:317:2: ( ( rule__Image__Group__0 ) )
            // InternalPxManual.g:318:3: ( rule__Image__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getGroup()); 
            }
            // InternalPxManual.g:319:3: ( rule__Image__Group__0 )
            // InternalPxManual.g:319:4: rule__Image__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Image__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleAbstractSection"
    // InternalPxManual.g:328:1: entryRuleAbstractSection : ruleAbstractSection EOF ;
    public final void entryRuleAbstractSection() throws RecognitionException {
        try {
            // InternalPxManual.g:329:1: ( ruleAbstractSection EOF )
            // InternalPxManual.g:330:1: ruleAbstractSection EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAbstractSectionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleAbstractSection();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAbstractSectionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractSection"


    // $ANTLR start "ruleAbstractSection"
    // InternalPxManual.g:337:1: ruleAbstractSection : ( ( rule__AbstractSection__Alternatives ) ) ;
    public final void ruleAbstractSection() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:341:2: ( ( ( rule__AbstractSection__Alternatives ) ) )
            // InternalPxManual.g:342:2: ( ( rule__AbstractSection__Alternatives ) )
            {
            // InternalPxManual.g:342:2: ( ( rule__AbstractSection__Alternatives ) )
            // InternalPxManual.g:343:3: ( rule__AbstractSection__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAbstractSectionAccess().getAlternatives()); 
            }
            // InternalPxManual.g:344:3: ( rule__AbstractSection__Alternatives )
            // InternalPxManual.g:344:4: rule__AbstractSection__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AbstractSection__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAbstractSectionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractSection"


    // $ANTLR start "entryRuleSection"
    // InternalPxManual.g:353:1: entryRuleSection : ruleSection EOF ;
    public final void entryRuleSection() throws RecognitionException {
        try {
            // InternalPxManual.g:354:1: ( ruleSection EOF )
            // InternalPxManual.g:355:1: ruleSection EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSectionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleSection();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSectionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSection"


    // $ANTLR start "ruleSection"
    // InternalPxManual.g:362:1: ruleSection : ( ( rule__Section__Group__0 ) ) ;
    public final void ruleSection() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:366:2: ( ( ( rule__Section__Group__0 ) ) )
            // InternalPxManual.g:367:2: ( ( rule__Section__Group__0 ) )
            {
            // InternalPxManual.g:367:2: ( ( rule__Section__Group__0 ) )
            // InternalPxManual.g:368:3: ( rule__Section__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSectionAccess().getGroup()); 
            }
            // InternalPxManual.g:369:3: ( rule__Section__Group__0 )
            // InternalPxManual.g:369:4: rule__Section__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Section__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSectionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSection"


    // $ANTLR start "entryRulePage"
    // InternalPxManual.g:378:1: entryRulePage : rulePage EOF ;
    public final void entryRulePage() throws RecognitionException {
        try {
            // InternalPxManual.g:379:1: ( rulePage EOF )
            // InternalPxManual.g:380:1: rulePage EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPageRule()); 
            }
            pushFollow(FOLLOW_1);
            rulePage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPageRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePage"


    // $ANTLR start "rulePage"
    // InternalPxManual.g:387:1: rulePage : ( ( rule__Page__Group__0 ) ) ;
    public final void rulePage() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:391:2: ( ( ( rule__Page__Group__0 ) ) )
            // InternalPxManual.g:392:2: ( ( rule__Page__Group__0 ) )
            {
            // InternalPxManual.g:392:2: ( ( rule__Page__Group__0 ) )
            // InternalPxManual.g:393:3: ( rule__Page__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPageAccess().getGroup()); 
            }
            // InternalPxManual.g:394:3: ( rule__Page__Group__0 )
            // InternalPxManual.g:394:4: rule__Page__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Page__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPageAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePage"


    // $ANTLR start "entryRuleAbstractExample"
    // InternalPxManual.g:403:1: entryRuleAbstractExample : ruleAbstractExample EOF ;
    public final void entryRuleAbstractExample() throws RecognitionException {
        try {
            // InternalPxManual.g:404:1: ( ruleAbstractExample EOF )
            // InternalPxManual.g:405:1: ruleAbstractExample EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAbstractExampleRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleAbstractExample();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAbstractExampleRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractExample"


    // $ANTLR start "ruleAbstractExample"
    // InternalPxManual.g:412:1: ruleAbstractExample : ( ( rule__AbstractExample__Alternatives ) ) ;
    public final void ruleAbstractExample() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:416:2: ( ( ( rule__AbstractExample__Alternatives ) ) )
            // InternalPxManual.g:417:2: ( ( rule__AbstractExample__Alternatives ) )
            {
            // InternalPxManual.g:417:2: ( ( rule__AbstractExample__Alternatives ) )
            // InternalPxManual.g:418:3: ( rule__AbstractExample__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAbstractExampleAccess().getAlternatives()); 
            }
            // InternalPxManual.g:419:3: ( rule__AbstractExample__Alternatives )
            // InternalPxManual.g:419:4: rule__AbstractExample__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AbstractExample__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAbstractExampleAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractExample"


    // $ANTLR start "entryRuleExample"
    // InternalPxManual.g:428:1: entryRuleExample : ruleExample EOF ;
    public final void entryRuleExample() throws RecognitionException {
        try {
            // InternalPxManual.g:429:1: ( ruleExample EOF )
            // InternalPxManual.g:430:1: ruleExample EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExampleRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleExample();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExampleRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExample"


    // $ANTLR start "ruleExample"
    // InternalPxManual.g:437:1: ruleExample : ( ( rule__Example__Group__0 ) ) ;
    public final void ruleExample() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:441:2: ( ( ( rule__Example__Group__0 ) ) )
            // InternalPxManual.g:442:2: ( ( rule__Example__Group__0 ) )
            {
            // InternalPxManual.g:442:2: ( ( rule__Example__Group__0 ) )
            // InternalPxManual.g:443:3: ( rule__Example__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExampleAccess().getGroup()); 
            }
            // InternalPxManual.g:444:3: ( rule__Example__Group__0 )
            // InternalPxManual.g:444:4: rule__Example__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Example__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExampleAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExample"


    // $ANTLR start "entryRuleUsage"
    // InternalPxManual.g:453:1: entryRuleUsage : ruleUsage EOF ;
    public final void entryRuleUsage() throws RecognitionException {
        try {
            // InternalPxManual.g:454:1: ( ruleUsage EOF )
            // InternalPxManual.g:455:1: ruleUsage EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUsageRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleUsage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUsageRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUsage"


    // $ANTLR start "ruleUsage"
    // InternalPxManual.g:462:1: ruleUsage : ( ( rule__Usage__Group__0 ) ) ;
    public final void ruleUsage() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:466:2: ( ( ( rule__Usage__Group__0 ) ) )
            // InternalPxManual.g:467:2: ( ( rule__Usage__Group__0 ) )
            {
            // InternalPxManual.g:467:2: ( ( rule__Usage__Group__0 ) )
            // InternalPxManual.g:468:3: ( rule__Usage__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUsageAccess().getGroup()); 
            }
            // InternalPxManual.g:469:3: ( rule__Usage__Group__0 )
            // InternalPxManual.g:469:4: rule__Usage__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Usage__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUsageAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUsage"


    // $ANTLR start "entryRuleText"
    // InternalPxManual.g:478:1: entryRuleText : ruleText EOF ;
    public final void entryRuleText() throws RecognitionException {
        try {
            // InternalPxManual.g:479:1: ( ruleText EOF )
            // InternalPxManual.g:480:1: ruleText EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleText();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleText"


    // $ANTLR start "ruleText"
    // InternalPxManual.g:487:1: ruleText : ( ( rule__Text__Group__0 ) ) ;
    public final void ruleText() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:491:2: ( ( ( rule__Text__Group__0 ) ) )
            // InternalPxManual.g:492:2: ( ( rule__Text__Group__0 ) )
            {
            // InternalPxManual.g:492:2: ( ( rule__Text__Group__0 ) )
            // InternalPxManual.g:493:3: ( rule__Text__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextAccess().getGroup()); 
            }
            // InternalPxManual.g:494:3: ( rule__Text__Group__0 )
            // InternalPxManual.g:494:4: rule__Text__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Text__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleText"


    // $ANTLR start "entryRuleKeyword"
    // InternalPxManual.g:503:1: entryRuleKeyword : ruleKeyword EOF ;
    public final void entryRuleKeyword() throws RecognitionException {
        try {
            // InternalPxManual.g:504:1: ( ruleKeyword EOF )
            // InternalPxManual.g:505:1: ruleKeyword EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleKeyword();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleKeyword"


    // $ANTLR start "ruleKeyword"
    // InternalPxManual.g:512:1: ruleKeyword : ( ( rule__Keyword__Group__0 ) ) ;
    public final void ruleKeyword() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:516:2: ( ( ( rule__Keyword__Group__0 ) ) )
            // InternalPxManual.g:517:2: ( ( rule__Keyword__Group__0 ) )
            {
            // InternalPxManual.g:517:2: ( ( rule__Keyword__Group__0 ) )
            // InternalPxManual.g:518:3: ( rule__Keyword__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getGroup()); 
            }
            // InternalPxManual.g:519:3: ( rule__Keyword__Group__0 )
            // InternalPxManual.g:519:4: rule__Keyword__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Keyword__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleKeyword"


    // $ANTLR start "entryRuleKeywordAttribute"
    // InternalPxManual.g:528:1: entryRuleKeywordAttribute : ruleKeywordAttribute EOF ;
    public final void entryRuleKeywordAttribute() throws RecognitionException {
        try {
            // InternalPxManual.g:529:1: ( ruleKeywordAttribute EOF )
            // InternalPxManual.g:530:1: ruleKeywordAttribute EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleKeywordAttribute();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleKeywordAttribute"


    // $ANTLR start "ruleKeywordAttribute"
    // InternalPxManual.g:537:1: ruleKeywordAttribute : ( ( rule__KeywordAttribute__Group__0 ) ) ;
    public final void ruleKeywordAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:541:2: ( ( ( rule__KeywordAttribute__Group__0 ) ) )
            // InternalPxManual.g:542:2: ( ( rule__KeywordAttribute__Group__0 ) )
            {
            // InternalPxManual.g:542:2: ( ( rule__KeywordAttribute__Group__0 ) )
            // InternalPxManual.g:543:3: ( rule__KeywordAttribute__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getGroup()); 
            }
            // InternalPxManual.g:544:3: ( rule__KeywordAttribute__Group__0 )
            // InternalPxManual.g:544:4: rule__KeywordAttribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleKeywordAttribute"


    // $ANTLR start "ruleVerticalAlignment"
    // InternalPxManual.g:553:1: ruleVerticalAlignment : ( ( rule__VerticalAlignment__Alternatives ) ) ;
    public final void ruleVerticalAlignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:557:1: ( ( ( rule__VerticalAlignment__Alternatives ) ) )
            // InternalPxManual.g:558:2: ( ( rule__VerticalAlignment__Alternatives ) )
            {
            // InternalPxManual.g:558:2: ( ( rule__VerticalAlignment__Alternatives ) )
            // InternalPxManual.g:559:3: ( rule__VerticalAlignment__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVerticalAlignmentAccess().getAlternatives()); 
            }
            // InternalPxManual.g:560:3: ( rule__VerticalAlignment__Alternatives )
            // InternalPxManual.g:560:4: rule__VerticalAlignment__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__VerticalAlignment__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVerticalAlignmentAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVerticalAlignment"


    // $ANTLR start "rule__ManualElement__Alternatives"
    // InternalPxManual.g:568:1: rule__ManualElement__Alternatives : ( ( ruleKeyword ) | ( ruleAbstractSection ) | ( ruleText ) | ( ruleImage ) | ( ruleAbstractExample ) | ( ruleTable ) | ( ruleSamePage ) | ( ruleList ) | ( ruleToc ) );
    public final void rule__ManualElement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:572:1: ( ( ruleKeyword ) | ( ruleAbstractSection ) | ( ruleText ) | ( ruleImage ) | ( ruleAbstractExample ) | ( ruleTable ) | ( ruleSamePage ) | ( ruleList ) | ( ruleToc ) )
            int alt1=9;
            switch ( input.LA(1) ) {
            case 41:
                {
                alt1=1;
                }
                break;
            case 34:
            case 35:
                {
                alt1=2;
                }
                break;
            case RULE_STRING:
            case 39:
            case 40:
                {
                alt1=3;
                }
                break;
            case 33:
                {
                alt1=4;
                }
                break;
            case 36:
            case 38:
                {
                alt1=5;
                }
                break;
            case 25:
                {
                alt1=6;
                }
                break;
            case 24:
                {
                alt1=7;
                }
                break;
            case 22:
                {
                alt1=8;
                }
                break;
            case 20:
                {
                alt1=9;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalPxManual.g:573:2: ( ruleKeyword )
                    {
                    // InternalPxManual.g:573:2: ( ruleKeyword )
                    // InternalPxManual.g:574:3: ruleKeyword
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getManualElementAccess().getKeywordParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleKeyword();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getManualElementAccess().getKeywordParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:579:2: ( ruleAbstractSection )
                    {
                    // InternalPxManual.g:579:2: ( ruleAbstractSection )
                    // InternalPxManual.g:580:3: ruleAbstractSection
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getManualElementAccess().getAbstractSectionParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleAbstractSection();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getManualElementAccess().getAbstractSectionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalPxManual.g:585:2: ( ruleText )
                    {
                    // InternalPxManual.g:585:2: ( ruleText )
                    // InternalPxManual.g:586:3: ruleText
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getManualElementAccess().getTextParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleText();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getManualElementAccess().getTextParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalPxManual.g:591:2: ( ruleImage )
                    {
                    // InternalPxManual.g:591:2: ( ruleImage )
                    // InternalPxManual.g:592:3: ruleImage
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getManualElementAccess().getImageParserRuleCall_3()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleImage();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getManualElementAccess().getImageParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalPxManual.g:597:2: ( ruleAbstractExample )
                    {
                    // InternalPxManual.g:597:2: ( ruleAbstractExample )
                    // InternalPxManual.g:598:3: ruleAbstractExample
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getManualElementAccess().getAbstractExampleParserRuleCall_4()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleAbstractExample();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getManualElementAccess().getAbstractExampleParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalPxManual.g:603:2: ( ruleTable )
                    {
                    // InternalPxManual.g:603:2: ( ruleTable )
                    // InternalPxManual.g:604:3: ruleTable
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getManualElementAccess().getTableParserRuleCall_5()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleTable();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getManualElementAccess().getTableParserRuleCall_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalPxManual.g:609:2: ( ruleSamePage )
                    {
                    // InternalPxManual.g:609:2: ( ruleSamePage )
                    // InternalPxManual.g:610:3: ruleSamePage
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getManualElementAccess().getSamePageParserRuleCall_6()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleSamePage();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getManualElementAccess().getSamePageParserRuleCall_6()); 
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalPxManual.g:615:2: ( ruleList )
                    {
                    // InternalPxManual.g:615:2: ( ruleList )
                    // InternalPxManual.g:616:3: ruleList
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getManualElementAccess().getListParserRuleCall_7()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleList();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getManualElementAccess().getListParserRuleCall_7()); 
                    }

                    }


                    }
                    break;
                case 9 :
                    // InternalPxManual.g:621:2: ( ruleToc )
                    {
                    // InternalPxManual.g:621:2: ( ruleToc )
                    // InternalPxManual.g:622:3: ruleToc
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getManualElementAccess().getTocParserRuleCall_8()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleToc();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getManualElementAccess().getTocParserRuleCall_8()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManualElement__Alternatives"


    // $ANTLR start "rule__AbstractSection__Alternatives"
    // InternalPxManual.g:631:1: rule__AbstractSection__Alternatives : ( ( ruleSection ) | ( rulePage ) );
    public final void rule__AbstractSection__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:635:1: ( ( ruleSection ) | ( rulePage ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==34) ) {
                alt2=1;
            }
            else if ( (LA2_0==35) ) {
                alt2=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalPxManual.g:636:2: ( ruleSection )
                    {
                    // InternalPxManual.g:636:2: ( ruleSection )
                    // InternalPxManual.g:637:3: ruleSection
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAbstractSectionAccess().getSectionParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleSection();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAbstractSectionAccess().getSectionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:642:2: ( rulePage )
                    {
                    // InternalPxManual.g:642:2: ( rulePage )
                    // InternalPxManual.g:643:3: rulePage
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAbstractSectionAccess().getPageParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    rulePage();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAbstractSectionAccess().getPageParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AbstractSection__Alternatives"


    // $ANTLR start "rule__AbstractExample__Alternatives"
    // InternalPxManual.g:652:1: rule__AbstractExample__Alternatives : ( ( ruleExample ) | ( ruleUsage ) );
    public final void rule__AbstractExample__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:656:1: ( ( ruleExample ) | ( ruleUsage ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==36) ) {
                alt3=1;
            }
            else if ( (LA3_0==38) ) {
                alt3=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalPxManual.g:657:2: ( ruleExample )
                    {
                    // InternalPxManual.g:657:2: ( ruleExample )
                    // InternalPxManual.g:658:3: ruleExample
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAbstractExampleAccess().getExampleParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleExample();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAbstractExampleAccess().getExampleParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:663:2: ( ruleUsage )
                    {
                    // InternalPxManual.g:663:2: ( ruleUsage )
                    // InternalPxManual.g:664:3: ruleUsage
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAbstractExampleAccess().getUsageParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleUsage();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAbstractExampleAccess().getUsageParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AbstractExample__Alternatives"


    // $ANTLR start "rule__VerticalAlignment__Alternatives"
    // InternalPxManual.g:673:1: rule__VerticalAlignment__Alternatives : ( ( ( 'top' ) ) | ( ( 'middle' ) ) | ( ( 'bottom' ) ) | ( ( 'baseline' ) ) );
    public final void rule__VerticalAlignment__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:677:1: ( ( ( 'top' ) ) | ( ( 'middle' ) ) | ( ( 'bottom' ) ) | ( ( 'baseline' ) ) )
            int alt4=4;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt4=1;
                }
                break;
            case 12:
                {
                alt4=2;
                }
                break;
            case 13:
                {
                alt4=3;
                }
                break;
            case 14:
                {
                alt4=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalPxManual.g:678:2: ( ( 'top' ) )
                    {
                    // InternalPxManual.g:678:2: ( ( 'top' ) )
                    // InternalPxManual.g:679:3: ( 'top' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVerticalAlignmentAccess().getTopEnumLiteralDeclaration_0()); 
                    }
                    // InternalPxManual.g:680:3: ( 'top' )
                    // InternalPxManual.g:680:4: 'top'
                    {
                    match(input,11,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVerticalAlignmentAccess().getTopEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:684:2: ( ( 'middle' ) )
                    {
                    // InternalPxManual.g:684:2: ( ( 'middle' ) )
                    // InternalPxManual.g:685:3: ( 'middle' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVerticalAlignmentAccess().getMiddleEnumLiteralDeclaration_1()); 
                    }
                    // InternalPxManual.g:686:3: ( 'middle' )
                    // InternalPxManual.g:686:4: 'middle'
                    {
                    match(input,12,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVerticalAlignmentAccess().getMiddleEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalPxManual.g:690:2: ( ( 'bottom' ) )
                    {
                    // InternalPxManual.g:690:2: ( ( 'bottom' ) )
                    // InternalPxManual.g:691:3: ( 'bottom' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVerticalAlignmentAccess().getBottomEnumLiteralDeclaration_2()); 
                    }
                    // InternalPxManual.g:692:3: ( 'bottom' )
                    // InternalPxManual.g:692:4: 'bottom'
                    {
                    match(input,13,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVerticalAlignmentAccess().getBottomEnumLiteralDeclaration_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalPxManual.g:696:2: ( ( 'baseline' ) )
                    {
                    // InternalPxManual.g:696:2: ( ( 'baseline' ) )
                    // InternalPxManual.g:697:3: ( 'baseline' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVerticalAlignmentAccess().getBaselineEnumLiteralDeclaration_3()); 
                    }
                    // InternalPxManual.g:698:3: ( 'baseline' )
                    // InternalPxManual.g:698:4: 'baseline'
                    {
                    match(input,14,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVerticalAlignmentAccess().getBaselineEnumLiteralDeclaration_3()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VerticalAlignment__Alternatives"


    // $ANTLR start "rule__Document__Group__0"
    // InternalPxManual.g:706:1: rule__Document__Group__0 : rule__Document__Group__0__Impl rule__Document__Group__1 ;
    public final void rule__Document__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:710:1: ( rule__Document__Group__0__Impl rule__Document__Group__1 )
            // InternalPxManual.g:711:2: rule__Document__Group__0__Impl rule__Document__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Document__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Document__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__0"


    // $ANTLR start "rule__Document__Group__0__Impl"
    // InternalPxManual.g:718:1: rule__Document__Group__0__Impl : ( () ) ;
    public final void rule__Document__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:722:1: ( ( () ) )
            // InternalPxManual.g:723:1: ( () )
            {
            // InternalPxManual.g:723:1: ( () )
            // InternalPxManual.g:724:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getDocumentAction_0()); 
            }
            // InternalPxManual.g:725:2: ()
            // InternalPxManual.g:725:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getDocumentAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__0__Impl"


    // $ANTLR start "rule__Document__Group__1"
    // InternalPxManual.g:733:1: rule__Document__Group__1 : rule__Document__Group__1__Impl rule__Document__Group__2 ;
    public final void rule__Document__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:737:1: ( rule__Document__Group__1__Impl rule__Document__Group__2 )
            // InternalPxManual.g:738:2: rule__Document__Group__1__Impl rule__Document__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Document__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Document__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__1"


    // $ANTLR start "rule__Document__Group__1__Impl"
    // InternalPxManual.g:745:1: rule__Document__Group__1__Impl : ( 'document' ) ;
    public final void rule__Document__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:749:1: ( ( 'document' ) )
            // InternalPxManual.g:750:1: ( 'document' )
            {
            // InternalPxManual.g:750:1: ( 'document' )
            // InternalPxManual.g:751:2: 'document'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getDocumentKeyword_1()); 
            }
            match(input,15,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getDocumentKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__1__Impl"


    // $ANTLR start "rule__Document__Group__2"
    // InternalPxManual.g:760:1: rule__Document__Group__2 : rule__Document__Group__2__Impl rule__Document__Group__3 ;
    public final void rule__Document__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:764:1: ( rule__Document__Group__2__Impl rule__Document__Group__3 )
            // InternalPxManual.g:765:2: rule__Document__Group__2__Impl rule__Document__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Document__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Document__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__2"


    // $ANTLR start "rule__Document__Group__2__Impl"
    // InternalPxManual.g:772:1: rule__Document__Group__2__Impl : ( ( rule__Document__UnorderedGroup_2 ) ) ;
    public final void rule__Document__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:776:1: ( ( ( rule__Document__UnorderedGroup_2 ) ) )
            // InternalPxManual.g:777:1: ( ( rule__Document__UnorderedGroup_2 ) )
            {
            // InternalPxManual.g:777:1: ( ( rule__Document__UnorderedGroup_2 ) )
            // InternalPxManual.g:778:2: ( rule__Document__UnorderedGroup_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getUnorderedGroup_2()); 
            }
            // InternalPxManual.g:779:2: ( rule__Document__UnorderedGroup_2 )
            // InternalPxManual.g:779:3: rule__Document__UnorderedGroup_2
            {
            pushFollow(FOLLOW_2);
            rule__Document__UnorderedGroup_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getUnorderedGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__2__Impl"


    // $ANTLR start "rule__Document__Group__3"
    // InternalPxManual.g:787:1: rule__Document__Group__3 : rule__Document__Group__3__Impl ;
    public final void rule__Document__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:791:1: ( rule__Document__Group__3__Impl )
            // InternalPxManual.g:792:2: rule__Document__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Document__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__3"


    // $ANTLR start "rule__Document__Group__3__Impl"
    // InternalPxManual.g:798:1: rule__Document__Group__3__Impl : ( ( rule__Document__BodyAssignment_3 ) ) ;
    public final void rule__Document__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:802:1: ( ( ( rule__Document__BodyAssignment_3 ) ) )
            // InternalPxManual.g:803:1: ( ( rule__Document__BodyAssignment_3 ) )
            {
            // InternalPxManual.g:803:1: ( ( rule__Document__BodyAssignment_3 ) )
            // InternalPxManual.g:804:2: ( rule__Document__BodyAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getBodyAssignment_3()); 
            }
            // InternalPxManual.g:805:2: ( rule__Document__BodyAssignment_3 )
            // InternalPxManual.g:805:3: rule__Document__BodyAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Document__BodyAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getBodyAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__3__Impl"


    // $ANTLR start "rule__Document__Group_2_1__0"
    // InternalPxManual.g:814:1: rule__Document__Group_2_1__0 : rule__Document__Group_2_1__0__Impl rule__Document__Group_2_1__1 ;
    public final void rule__Document__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:818:1: ( rule__Document__Group_2_1__0__Impl rule__Document__Group_2_1__1 )
            // InternalPxManual.g:819:2: rule__Document__Group_2_1__0__Impl rule__Document__Group_2_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Document__Group_2_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Document__Group_2_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_2_1__0"


    // $ANTLR start "rule__Document__Group_2_1__0__Impl"
    // InternalPxManual.g:826:1: rule__Document__Group_2_1__0__Impl : ( 'id:' ) ;
    public final void rule__Document__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:830:1: ( ( 'id:' ) )
            // InternalPxManual.g:831:1: ( 'id:' )
            {
            // InternalPxManual.g:831:1: ( 'id:' )
            // InternalPxManual.g:832:2: 'id:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getIdKeyword_2_1_0()); 
            }
            match(input,16,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getIdKeyword_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_2_1__0__Impl"


    // $ANTLR start "rule__Document__Group_2_1__1"
    // InternalPxManual.g:841:1: rule__Document__Group_2_1__1 : rule__Document__Group_2_1__1__Impl ;
    public final void rule__Document__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:845:1: ( rule__Document__Group_2_1__1__Impl )
            // InternalPxManual.g:846:2: rule__Document__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Document__Group_2_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_2_1__1"


    // $ANTLR start "rule__Document__Group_2_1__1__Impl"
    // InternalPxManual.g:852:1: rule__Document__Group_2_1__1__Impl : ( ( rule__Document__IdAssignment_2_1_1 ) ) ;
    public final void rule__Document__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:856:1: ( ( ( rule__Document__IdAssignment_2_1_1 ) ) )
            // InternalPxManual.g:857:1: ( ( rule__Document__IdAssignment_2_1_1 ) )
            {
            // InternalPxManual.g:857:1: ( ( rule__Document__IdAssignment_2_1_1 ) )
            // InternalPxManual.g:858:2: ( rule__Document__IdAssignment_2_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getIdAssignment_2_1_1()); 
            }
            // InternalPxManual.g:859:2: ( rule__Document__IdAssignment_2_1_1 )
            // InternalPxManual.g:859:3: rule__Document__IdAssignment_2_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Document__IdAssignment_2_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getIdAssignment_2_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_2_1__1__Impl"


    // $ANTLR start "rule__Document__Group_2_2__0"
    // InternalPxManual.g:868:1: rule__Document__Group_2_2__0 : rule__Document__Group_2_2__0__Impl rule__Document__Group_2_2__1 ;
    public final void rule__Document__Group_2_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:872:1: ( rule__Document__Group_2_2__0__Impl rule__Document__Group_2_2__1 )
            // InternalPxManual.g:873:2: rule__Document__Group_2_2__0__Impl rule__Document__Group_2_2__1
            {
            pushFollow(FOLLOW_7);
            rule__Document__Group_2_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Document__Group_2_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_2_2__0"


    // $ANTLR start "rule__Document__Group_2_2__0__Impl"
    // InternalPxManual.g:880:1: rule__Document__Group_2_2__0__Impl : ( 'firstHeadingLevel:' ) ;
    public final void rule__Document__Group_2_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:884:1: ( ( 'firstHeadingLevel:' ) )
            // InternalPxManual.g:885:1: ( 'firstHeadingLevel:' )
            {
            // InternalPxManual.g:885:1: ( 'firstHeadingLevel:' )
            // InternalPxManual.g:886:2: 'firstHeadingLevel:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getFirstHeadingLevelKeyword_2_2_0()); 
            }
            match(input,17,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getFirstHeadingLevelKeyword_2_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_2_2__0__Impl"


    // $ANTLR start "rule__Document__Group_2_2__1"
    // InternalPxManual.g:895:1: rule__Document__Group_2_2__1 : rule__Document__Group_2_2__1__Impl ;
    public final void rule__Document__Group_2_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:899:1: ( rule__Document__Group_2_2__1__Impl )
            // InternalPxManual.g:900:2: rule__Document__Group_2_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Document__Group_2_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_2_2__1"


    // $ANTLR start "rule__Document__Group_2_2__1__Impl"
    // InternalPxManual.g:906:1: rule__Document__Group_2_2__1__Impl : ( ( rule__Document__FirstHeadingLevelAssignment_2_2_1 ) ) ;
    public final void rule__Document__Group_2_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:910:1: ( ( ( rule__Document__FirstHeadingLevelAssignment_2_2_1 ) ) )
            // InternalPxManual.g:911:1: ( ( rule__Document__FirstHeadingLevelAssignment_2_2_1 ) )
            {
            // InternalPxManual.g:911:1: ( ( rule__Document__FirstHeadingLevelAssignment_2_2_1 ) )
            // InternalPxManual.g:912:2: ( rule__Document__FirstHeadingLevelAssignment_2_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getFirstHeadingLevelAssignment_2_2_1()); 
            }
            // InternalPxManual.g:913:2: ( rule__Document__FirstHeadingLevelAssignment_2_2_1 )
            // InternalPxManual.g:913:3: rule__Document__FirstHeadingLevelAssignment_2_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Document__FirstHeadingLevelAssignment_2_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getFirstHeadingLevelAssignment_2_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_2_2__1__Impl"


    // $ANTLR start "rule__DocBody__Group__0"
    // InternalPxManual.g:922:1: rule__DocBody__Group__0 : rule__DocBody__Group__0__Impl rule__DocBody__Group__1 ;
    public final void rule__DocBody__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:926:1: ( rule__DocBody__Group__0__Impl rule__DocBody__Group__1 )
            // InternalPxManual.g:927:2: rule__DocBody__Group__0__Impl rule__DocBody__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__DocBody__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DocBody__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocBody__Group__0"


    // $ANTLR start "rule__DocBody__Group__0__Impl"
    // InternalPxManual.g:934:1: rule__DocBody__Group__0__Impl : ( () ) ;
    public final void rule__DocBody__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:938:1: ( ( () ) )
            // InternalPxManual.g:939:1: ( () )
            {
            // InternalPxManual.g:939:1: ( () )
            // InternalPxManual.g:940:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocBodyAccess().getDocBodyAction_0()); 
            }
            // InternalPxManual.g:941:2: ()
            // InternalPxManual.g:941:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocBodyAccess().getDocBodyAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocBody__Group__0__Impl"


    // $ANTLR start "rule__DocBody__Group__1"
    // InternalPxManual.g:949:1: rule__DocBody__Group__1 : rule__DocBody__Group__1__Impl rule__DocBody__Group__2 ;
    public final void rule__DocBody__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:953:1: ( rule__DocBody__Group__1__Impl rule__DocBody__Group__2 )
            // InternalPxManual.g:954:2: rule__DocBody__Group__1__Impl rule__DocBody__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__DocBody__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DocBody__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocBody__Group__1"


    // $ANTLR start "rule__DocBody__Group__1__Impl"
    // InternalPxManual.g:961:1: rule__DocBody__Group__1__Impl : ( '{' ) ;
    public final void rule__DocBody__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:965:1: ( ( '{' ) )
            // InternalPxManual.g:966:1: ( '{' )
            {
            // InternalPxManual.g:966:1: ( '{' )
            // InternalPxManual.g:967:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocBodyAccess().getLeftCurlyBracketKeyword_1()); 
            }
            match(input,18,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocBodyAccess().getLeftCurlyBracketKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocBody__Group__1__Impl"


    // $ANTLR start "rule__DocBody__Group__2"
    // InternalPxManual.g:976:1: rule__DocBody__Group__2 : rule__DocBody__Group__2__Impl rule__DocBody__Group__3 ;
    public final void rule__DocBody__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:980:1: ( rule__DocBody__Group__2__Impl rule__DocBody__Group__3 )
            // InternalPxManual.g:981:2: rule__DocBody__Group__2__Impl rule__DocBody__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__DocBody__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DocBody__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocBody__Group__2"


    // $ANTLR start "rule__DocBody__Group__2__Impl"
    // InternalPxManual.g:988:1: rule__DocBody__Group__2__Impl : ( ( rule__DocBody__ElementsAssignment_2 )* ) ;
    public final void rule__DocBody__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:992:1: ( ( ( rule__DocBody__ElementsAssignment_2 )* ) )
            // InternalPxManual.g:993:1: ( ( rule__DocBody__ElementsAssignment_2 )* )
            {
            // InternalPxManual.g:993:1: ( ( rule__DocBody__ElementsAssignment_2 )* )
            // InternalPxManual.g:994:2: ( rule__DocBody__ElementsAssignment_2 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocBodyAccess().getElementsAssignment_2()); 
            }
            // InternalPxManual.g:995:2: ( rule__DocBody__ElementsAssignment_2 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_STRING||LA5_0==20||LA5_0==22||(LA5_0>=24 && LA5_0<=25)||(LA5_0>=33 && LA5_0<=36)||(LA5_0>=38 && LA5_0<=41)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalPxManual.g:995:3: rule__DocBody__ElementsAssignment_2
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__DocBody__ElementsAssignment_2();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocBodyAccess().getElementsAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocBody__Group__2__Impl"


    // $ANTLR start "rule__DocBody__Group__3"
    // InternalPxManual.g:1003:1: rule__DocBody__Group__3 : rule__DocBody__Group__3__Impl ;
    public final void rule__DocBody__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1007:1: ( rule__DocBody__Group__3__Impl )
            // InternalPxManual.g:1008:2: rule__DocBody__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DocBody__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocBody__Group__3"


    // $ANTLR start "rule__DocBody__Group__3__Impl"
    // InternalPxManual.g:1014:1: rule__DocBody__Group__3__Impl : ( '}' ) ;
    public final void rule__DocBody__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1018:1: ( ( '}' ) )
            // InternalPxManual.g:1019:1: ( '}' )
            {
            // InternalPxManual.g:1019:1: ( '}' )
            // InternalPxManual.g:1020:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocBodyAccess().getRightCurlyBracketKeyword_3()); 
            }
            match(input,19,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocBodyAccess().getRightCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocBody__Group__3__Impl"


    // $ANTLR start "rule__Toc__Group__0"
    // InternalPxManual.g:1030:1: rule__Toc__Group__0 : rule__Toc__Group__0__Impl rule__Toc__Group__1 ;
    public final void rule__Toc__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1034:1: ( rule__Toc__Group__0__Impl rule__Toc__Group__1 )
            // InternalPxManual.g:1035:2: rule__Toc__Group__0__Impl rule__Toc__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Toc__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Toc__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__Group__0"


    // $ANTLR start "rule__Toc__Group__0__Impl"
    // InternalPxManual.g:1042:1: rule__Toc__Group__0__Impl : ( () ) ;
    public final void rule__Toc__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1046:1: ( ( () ) )
            // InternalPxManual.g:1047:1: ( () )
            {
            // InternalPxManual.g:1047:1: ( () )
            // InternalPxManual.g:1048:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTocAccess().getTocAction_0()); 
            }
            // InternalPxManual.g:1049:2: ()
            // InternalPxManual.g:1049:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTocAccess().getTocAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__Group__0__Impl"


    // $ANTLR start "rule__Toc__Group__1"
    // InternalPxManual.g:1057:1: rule__Toc__Group__1 : rule__Toc__Group__1__Impl rule__Toc__Group__2 ;
    public final void rule__Toc__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1061:1: ( rule__Toc__Group__1__Impl rule__Toc__Group__2 )
            // InternalPxManual.g:1062:2: rule__Toc__Group__1__Impl rule__Toc__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__Toc__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Toc__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__Group__1"


    // $ANTLR start "rule__Toc__Group__1__Impl"
    // InternalPxManual.g:1069:1: rule__Toc__Group__1__Impl : ( 'toc' ) ;
    public final void rule__Toc__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1073:1: ( ( 'toc' ) )
            // InternalPxManual.g:1074:1: ( 'toc' )
            {
            // InternalPxManual.g:1074:1: ( 'toc' )
            // InternalPxManual.g:1075:2: 'toc'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTocAccess().getTocKeyword_1()); 
            }
            match(input,20,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTocAccess().getTocKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__Group__1__Impl"


    // $ANTLR start "rule__Toc__Group__2"
    // InternalPxManual.g:1084:1: rule__Toc__Group__2 : rule__Toc__Group__2__Impl ;
    public final void rule__Toc__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1088:1: ( rule__Toc__Group__2__Impl )
            // InternalPxManual.g:1089:2: rule__Toc__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Toc__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__Group__2"


    // $ANTLR start "rule__Toc__Group__2__Impl"
    // InternalPxManual.g:1095:1: rule__Toc__Group__2__Impl : ( ( rule__Toc__UnorderedGroup_2 ) ) ;
    public final void rule__Toc__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1099:1: ( ( ( rule__Toc__UnorderedGroup_2 ) ) )
            // InternalPxManual.g:1100:1: ( ( rule__Toc__UnorderedGroup_2 ) )
            {
            // InternalPxManual.g:1100:1: ( ( rule__Toc__UnorderedGroup_2 ) )
            // InternalPxManual.g:1101:2: ( rule__Toc__UnorderedGroup_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTocAccess().getUnorderedGroup_2()); 
            }
            // InternalPxManual.g:1102:2: ( rule__Toc__UnorderedGroup_2 )
            // InternalPxManual.g:1102:3: rule__Toc__UnorderedGroup_2
            {
            pushFollow(FOLLOW_2);
            rule__Toc__UnorderedGroup_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTocAccess().getUnorderedGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__Group__2__Impl"


    // $ANTLR start "rule__Toc__Group_2_1__0"
    // InternalPxManual.g:1111:1: rule__Toc__Group_2_1__0 : rule__Toc__Group_2_1__0__Impl rule__Toc__Group_2_1__1 ;
    public final void rule__Toc__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1115:1: ( rule__Toc__Group_2_1__0__Impl rule__Toc__Group_2_1__1 )
            // InternalPxManual.g:1116:2: rule__Toc__Group_2_1__0__Impl rule__Toc__Group_2_1__1
            {
            pushFollow(FOLLOW_7);
            rule__Toc__Group_2_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Toc__Group_2_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__Group_2_1__0"


    // $ANTLR start "rule__Toc__Group_2_1__0__Impl"
    // InternalPxManual.g:1123:1: rule__Toc__Group_2_1__0__Impl : ( 'maxLevel:' ) ;
    public final void rule__Toc__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1127:1: ( ( 'maxLevel:' ) )
            // InternalPxManual.g:1128:1: ( 'maxLevel:' )
            {
            // InternalPxManual.g:1128:1: ( 'maxLevel:' )
            // InternalPxManual.g:1129:2: 'maxLevel:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTocAccess().getMaxLevelKeyword_2_1_0()); 
            }
            match(input,21,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTocAccess().getMaxLevelKeyword_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__Group_2_1__0__Impl"


    // $ANTLR start "rule__Toc__Group_2_1__1"
    // InternalPxManual.g:1138:1: rule__Toc__Group_2_1__1 : rule__Toc__Group_2_1__1__Impl ;
    public final void rule__Toc__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1142:1: ( rule__Toc__Group_2_1__1__Impl )
            // InternalPxManual.g:1143:2: rule__Toc__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Toc__Group_2_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__Group_2_1__1"


    // $ANTLR start "rule__Toc__Group_2_1__1__Impl"
    // InternalPxManual.g:1149:1: rule__Toc__Group_2_1__1__Impl : ( ( rule__Toc__MaxLevelAssignment_2_1_1 ) ) ;
    public final void rule__Toc__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1153:1: ( ( ( rule__Toc__MaxLevelAssignment_2_1_1 ) ) )
            // InternalPxManual.g:1154:1: ( ( rule__Toc__MaxLevelAssignment_2_1_1 ) )
            {
            // InternalPxManual.g:1154:1: ( ( rule__Toc__MaxLevelAssignment_2_1_1 ) )
            // InternalPxManual.g:1155:2: ( rule__Toc__MaxLevelAssignment_2_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTocAccess().getMaxLevelAssignment_2_1_1()); 
            }
            // InternalPxManual.g:1156:2: ( rule__Toc__MaxLevelAssignment_2_1_1 )
            // InternalPxManual.g:1156:3: rule__Toc__MaxLevelAssignment_2_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Toc__MaxLevelAssignment_2_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTocAccess().getMaxLevelAssignment_2_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__Group_2_1__1__Impl"


    // $ANTLR start "rule__List__Group__0"
    // InternalPxManual.g:1165:1: rule__List__Group__0 : rule__List__Group__0__Impl rule__List__Group__1 ;
    public final void rule__List__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1169:1: ( rule__List__Group__0__Impl rule__List__Group__1 )
            // InternalPxManual.g:1170:2: rule__List__Group__0__Impl rule__List__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__List__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__List__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__0"


    // $ANTLR start "rule__List__Group__0__Impl"
    // InternalPxManual.g:1177:1: rule__List__Group__0__Impl : ( () ) ;
    public final void rule__List__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1181:1: ( ( () ) )
            // InternalPxManual.g:1182:1: ( () )
            {
            // InternalPxManual.g:1182:1: ( () )
            // InternalPxManual.g:1183:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListAccess().getListAction_0()); 
            }
            // InternalPxManual.g:1184:2: ()
            // InternalPxManual.g:1184:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getListAccess().getListAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__0__Impl"


    // $ANTLR start "rule__List__Group__1"
    // InternalPxManual.g:1192:1: rule__List__Group__1 : rule__List__Group__1__Impl rule__List__Group__2 ;
    public final void rule__List__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1196:1: ( rule__List__Group__1__Impl rule__List__Group__2 )
            // InternalPxManual.g:1197:2: rule__List__Group__1__Impl rule__List__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__List__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__List__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__1"


    // $ANTLR start "rule__List__Group__1__Impl"
    // InternalPxManual.g:1204:1: rule__List__Group__1__Impl : ( 'list' ) ;
    public final void rule__List__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1208:1: ( ( 'list' ) )
            // InternalPxManual.g:1209:1: ( 'list' )
            {
            // InternalPxManual.g:1209:1: ( 'list' )
            // InternalPxManual.g:1210:2: 'list'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListAccess().getListKeyword_1()); 
            }
            match(input,22,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getListAccess().getListKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__1__Impl"


    // $ANTLR start "rule__List__Group__2"
    // InternalPxManual.g:1219:1: rule__List__Group__2 : rule__List__Group__2__Impl rule__List__Group__3 ;
    public final void rule__List__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1223:1: ( rule__List__Group__2__Impl rule__List__Group__3 )
            // InternalPxManual.g:1224:2: rule__List__Group__2__Impl rule__List__Group__3
            {
            pushFollow(FOLLOW_13);
            rule__List__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__List__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__2"


    // $ANTLR start "rule__List__Group__2__Impl"
    // InternalPxManual.g:1231:1: rule__List__Group__2__Impl : ( '{' ) ;
    public final void rule__List__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1235:1: ( ( '{' ) )
            // InternalPxManual.g:1236:1: ( '{' )
            {
            // InternalPxManual.g:1236:1: ( '{' )
            // InternalPxManual.g:1237:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListAccess().getLeftCurlyBracketKeyword_2()); 
            }
            match(input,18,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getListAccess().getLeftCurlyBracketKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__2__Impl"


    // $ANTLR start "rule__List__Group__3"
    // InternalPxManual.g:1246:1: rule__List__Group__3 : rule__List__Group__3__Impl rule__List__Group__4 ;
    public final void rule__List__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1250:1: ( rule__List__Group__3__Impl rule__List__Group__4 )
            // InternalPxManual.g:1251:2: rule__List__Group__3__Impl rule__List__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__List__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__List__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__3"


    // $ANTLR start "rule__List__Group__3__Impl"
    // InternalPxManual.g:1258:1: rule__List__Group__3__Impl : ( ( rule__List__ItemsAssignment_3 )* ) ;
    public final void rule__List__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1262:1: ( ( ( rule__List__ItemsAssignment_3 )* ) )
            // InternalPxManual.g:1263:1: ( ( rule__List__ItemsAssignment_3 )* )
            {
            // InternalPxManual.g:1263:1: ( ( rule__List__ItemsAssignment_3 )* )
            // InternalPxManual.g:1264:2: ( rule__List__ItemsAssignment_3 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListAccess().getItemsAssignment_3()); 
            }
            // InternalPxManual.g:1265:2: ( rule__List__ItemsAssignment_3 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==23) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalPxManual.g:1265:3: rule__List__ItemsAssignment_3
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__List__ItemsAssignment_3();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getListAccess().getItemsAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__3__Impl"


    // $ANTLR start "rule__List__Group__4"
    // InternalPxManual.g:1273:1: rule__List__Group__4 : rule__List__Group__4__Impl ;
    public final void rule__List__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1277:1: ( rule__List__Group__4__Impl )
            // InternalPxManual.g:1278:2: rule__List__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__List__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__4"


    // $ANTLR start "rule__List__Group__4__Impl"
    // InternalPxManual.g:1284:1: rule__List__Group__4__Impl : ( '}' ) ;
    public final void rule__List__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1288:1: ( ( '}' ) )
            // InternalPxManual.g:1289:1: ( '}' )
            {
            // InternalPxManual.g:1289:1: ( '}' )
            // InternalPxManual.g:1290:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListAccess().getRightCurlyBracketKeyword_4()); 
            }
            match(input,19,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getListAccess().getRightCurlyBracketKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__4__Impl"


    // $ANTLR start "rule__ListItem__Group__0"
    // InternalPxManual.g:1300:1: rule__ListItem__Group__0 : rule__ListItem__Group__0__Impl rule__ListItem__Group__1 ;
    public final void rule__ListItem__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1304:1: ( rule__ListItem__Group__0__Impl rule__ListItem__Group__1 )
            // InternalPxManual.g:1305:2: rule__ListItem__Group__0__Impl rule__ListItem__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__ListItem__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__ListItem__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListItem__Group__0"


    // $ANTLR start "rule__ListItem__Group__0__Impl"
    // InternalPxManual.g:1312:1: rule__ListItem__Group__0__Impl : ( 'item' ) ;
    public final void rule__ListItem__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1316:1: ( ( 'item' ) )
            // InternalPxManual.g:1317:1: ( 'item' )
            {
            // InternalPxManual.g:1317:1: ( 'item' )
            // InternalPxManual.g:1318:2: 'item'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListItemAccess().getItemKeyword_0()); 
            }
            match(input,23,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getListItemAccess().getItemKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListItem__Group__0__Impl"


    // $ANTLR start "rule__ListItem__Group__1"
    // InternalPxManual.g:1327:1: rule__ListItem__Group__1 : rule__ListItem__Group__1__Impl ;
    public final void rule__ListItem__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1331:1: ( rule__ListItem__Group__1__Impl )
            // InternalPxManual.g:1332:2: rule__ListItem__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ListItem__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListItem__Group__1"


    // $ANTLR start "rule__ListItem__Group__1__Impl"
    // InternalPxManual.g:1338:1: rule__ListItem__Group__1__Impl : ( ( rule__ListItem__BodyAssignment_1 ) ) ;
    public final void rule__ListItem__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1342:1: ( ( ( rule__ListItem__BodyAssignment_1 ) ) )
            // InternalPxManual.g:1343:1: ( ( rule__ListItem__BodyAssignment_1 ) )
            {
            // InternalPxManual.g:1343:1: ( ( rule__ListItem__BodyAssignment_1 ) )
            // InternalPxManual.g:1344:2: ( rule__ListItem__BodyAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListItemAccess().getBodyAssignment_1()); 
            }
            // InternalPxManual.g:1345:2: ( rule__ListItem__BodyAssignment_1 )
            // InternalPxManual.g:1345:3: rule__ListItem__BodyAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ListItem__BodyAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getListItemAccess().getBodyAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListItem__Group__1__Impl"


    // $ANTLR start "rule__SamePage__Group__0"
    // InternalPxManual.g:1354:1: rule__SamePage__Group__0 : rule__SamePage__Group__0__Impl rule__SamePage__Group__1 ;
    public final void rule__SamePage__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1358:1: ( rule__SamePage__Group__0__Impl rule__SamePage__Group__1 )
            // InternalPxManual.g:1359:2: rule__SamePage__Group__0__Impl rule__SamePage__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__SamePage__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__SamePage__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SamePage__Group__0"


    // $ANTLR start "rule__SamePage__Group__0__Impl"
    // InternalPxManual.g:1366:1: rule__SamePage__Group__0__Impl : ( 'samePage' ) ;
    public final void rule__SamePage__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1370:1: ( ( 'samePage' ) )
            // InternalPxManual.g:1371:1: ( 'samePage' )
            {
            // InternalPxManual.g:1371:1: ( 'samePage' )
            // InternalPxManual.g:1372:2: 'samePage'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSamePageAccess().getSamePageKeyword_0()); 
            }
            match(input,24,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSamePageAccess().getSamePageKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SamePage__Group__0__Impl"


    // $ANTLR start "rule__SamePage__Group__1"
    // InternalPxManual.g:1381:1: rule__SamePage__Group__1 : rule__SamePage__Group__1__Impl ;
    public final void rule__SamePage__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1385:1: ( rule__SamePage__Group__1__Impl )
            // InternalPxManual.g:1386:2: rule__SamePage__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SamePage__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SamePage__Group__1"


    // $ANTLR start "rule__SamePage__Group__1__Impl"
    // InternalPxManual.g:1392:1: rule__SamePage__Group__1__Impl : ( ( rule__SamePage__BodyAssignment_1 ) ) ;
    public final void rule__SamePage__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1396:1: ( ( ( rule__SamePage__BodyAssignment_1 ) ) )
            // InternalPxManual.g:1397:1: ( ( rule__SamePage__BodyAssignment_1 ) )
            {
            // InternalPxManual.g:1397:1: ( ( rule__SamePage__BodyAssignment_1 ) )
            // InternalPxManual.g:1398:2: ( rule__SamePage__BodyAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSamePageAccess().getBodyAssignment_1()); 
            }
            // InternalPxManual.g:1399:2: ( rule__SamePage__BodyAssignment_1 )
            // InternalPxManual.g:1399:3: rule__SamePage__BodyAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__SamePage__BodyAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSamePageAccess().getBodyAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SamePage__Group__1__Impl"


    // $ANTLR start "rule__Table__Group__0"
    // InternalPxManual.g:1408:1: rule__Table__Group__0 : rule__Table__Group__0__Impl rule__Table__Group__1 ;
    public final void rule__Table__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1412:1: ( rule__Table__Group__0__Impl rule__Table__Group__1 )
            // InternalPxManual.g:1413:2: rule__Table__Group__0__Impl rule__Table__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Table__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Table__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__0"


    // $ANTLR start "rule__Table__Group__0__Impl"
    // InternalPxManual.g:1420:1: rule__Table__Group__0__Impl : ( () ) ;
    public final void rule__Table__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1424:1: ( ( () ) )
            // InternalPxManual.g:1425:1: ( () )
            {
            // InternalPxManual.g:1425:1: ( () )
            // InternalPxManual.g:1426:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getTableAction_0()); 
            }
            // InternalPxManual.g:1427:2: ()
            // InternalPxManual.g:1427:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getTableAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__0__Impl"


    // $ANTLR start "rule__Table__Group__1"
    // InternalPxManual.g:1435:1: rule__Table__Group__1 : rule__Table__Group__1__Impl rule__Table__Group__2 ;
    public final void rule__Table__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1439:1: ( rule__Table__Group__1__Impl rule__Table__Group__2 )
            // InternalPxManual.g:1440:2: rule__Table__Group__1__Impl rule__Table__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Table__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Table__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__1"


    // $ANTLR start "rule__Table__Group__1__Impl"
    // InternalPxManual.g:1447:1: rule__Table__Group__1__Impl : ( 'table' ) ;
    public final void rule__Table__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1451:1: ( ( 'table' ) )
            // InternalPxManual.g:1452:1: ( 'table' )
            {
            // InternalPxManual.g:1452:1: ( 'table' )
            // InternalPxManual.g:1453:2: 'table'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getTableKeyword_1()); 
            }
            match(input,25,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getTableKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__1__Impl"


    // $ANTLR start "rule__Table__Group__2"
    // InternalPxManual.g:1462:1: rule__Table__Group__2 : rule__Table__Group__2__Impl rule__Table__Group__3 ;
    public final void rule__Table__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1466:1: ( rule__Table__Group__2__Impl rule__Table__Group__3 )
            // InternalPxManual.g:1467:2: rule__Table__Group__2__Impl rule__Table__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Table__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Table__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__2"


    // $ANTLR start "rule__Table__Group__2__Impl"
    // InternalPxManual.g:1474:1: rule__Table__Group__2__Impl : ( ( rule__Table__UnorderedGroup_2 ) ) ;
    public final void rule__Table__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1478:1: ( ( ( rule__Table__UnorderedGroup_2 ) ) )
            // InternalPxManual.g:1479:1: ( ( rule__Table__UnorderedGroup_2 ) )
            {
            // InternalPxManual.g:1479:1: ( ( rule__Table__UnorderedGroup_2 ) )
            // InternalPxManual.g:1480:2: ( rule__Table__UnorderedGroup_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getUnorderedGroup_2()); 
            }
            // InternalPxManual.g:1481:2: ( rule__Table__UnorderedGroup_2 )
            // InternalPxManual.g:1481:3: rule__Table__UnorderedGroup_2
            {
            pushFollow(FOLLOW_2);
            rule__Table__UnorderedGroup_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getUnorderedGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__2__Impl"


    // $ANTLR start "rule__Table__Group__3"
    // InternalPxManual.g:1489:1: rule__Table__Group__3 : rule__Table__Group__3__Impl rule__Table__Group__4 ;
    public final void rule__Table__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1493:1: ( rule__Table__Group__3__Impl rule__Table__Group__4 )
            // InternalPxManual.g:1494:2: rule__Table__Group__3__Impl rule__Table__Group__4
            {
            pushFollow(FOLLOW_17);
            rule__Table__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Table__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__3"


    // $ANTLR start "rule__Table__Group__3__Impl"
    // InternalPxManual.g:1501:1: rule__Table__Group__3__Impl : ( '{' ) ;
    public final void rule__Table__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1505:1: ( ( '{' ) )
            // InternalPxManual.g:1506:1: ( '{' )
            {
            // InternalPxManual.g:1506:1: ( '{' )
            // InternalPxManual.g:1507:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,18,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__3__Impl"


    // $ANTLR start "rule__Table__Group__4"
    // InternalPxManual.g:1516:1: rule__Table__Group__4 : rule__Table__Group__4__Impl rule__Table__Group__5 ;
    public final void rule__Table__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1520:1: ( rule__Table__Group__4__Impl rule__Table__Group__5 )
            // InternalPxManual.g:1521:2: rule__Table__Group__4__Impl rule__Table__Group__5
            {
            pushFollow(FOLLOW_17);
            rule__Table__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Table__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__4"


    // $ANTLR start "rule__Table__Group__4__Impl"
    // InternalPxManual.g:1528:1: rule__Table__Group__4__Impl : ( ( rule__Table__RowsAssignment_4 )* ) ;
    public final void rule__Table__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1532:1: ( ( ( rule__Table__RowsAssignment_4 )* ) )
            // InternalPxManual.g:1533:1: ( ( rule__Table__RowsAssignment_4 )* )
            {
            // InternalPxManual.g:1533:1: ( ( rule__Table__RowsAssignment_4 )* )
            // InternalPxManual.g:1534:2: ( rule__Table__RowsAssignment_4 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getRowsAssignment_4()); 
            }
            // InternalPxManual.g:1535:2: ( rule__Table__RowsAssignment_4 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==28) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalPxManual.g:1535:3: rule__Table__RowsAssignment_4
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__Table__RowsAssignment_4();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getRowsAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__4__Impl"


    // $ANTLR start "rule__Table__Group__5"
    // InternalPxManual.g:1543:1: rule__Table__Group__5 : rule__Table__Group__5__Impl ;
    public final void rule__Table__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1547:1: ( rule__Table__Group__5__Impl )
            // InternalPxManual.g:1548:2: rule__Table__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Table__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__5"


    // $ANTLR start "rule__Table__Group__5__Impl"
    // InternalPxManual.g:1554:1: rule__Table__Group__5__Impl : ( '}' ) ;
    public final void rule__Table__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1558:1: ( ( '}' ) )
            // InternalPxManual.g:1559:1: ( '}' )
            {
            // InternalPxManual.g:1559:1: ( '}' )
            // InternalPxManual.g:1560:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getRightCurlyBracketKeyword_5()); 
            }
            match(input,19,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getRightCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__5__Impl"


    // $ANTLR start "rule__Table__Group_2_0__0"
    // InternalPxManual.g:1570:1: rule__Table__Group_2_0__0 : rule__Table__Group_2_0__0__Impl rule__Table__Group_2_0__1 ;
    public final void rule__Table__Group_2_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1574:1: ( rule__Table__Group_2_0__0__Impl rule__Table__Group_2_0__1 )
            // InternalPxManual.g:1575:2: rule__Table__Group_2_0__0__Impl rule__Table__Group_2_0__1
            {
            pushFollow(FOLLOW_6);
            rule__Table__Group_2_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Table__Group_2_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group_2_0__0"


    // $ANTLR start "rule__Table__Group_2_0__0__Impl"
    // InternalPxManual.g:1582:1: rule__Table__Group_2_0__0__Impl : ( 'style:' ) ;
    public final void rule__Table__Group_2_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1586:1: ( ( 'style:' ) )
            // InternalPxManual.g:1587:1: ( 'style:' )
            {
            // InternalPxManual.g:1587:1: ( 'style:' )
            // InternalPxManual.g:1588:2: 'style:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getStyleKeyword_2_0_0()); 
            }
            match(input,26,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getStyleKeyword_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group_2_0__0__Impl"


    // $ANTLR start "rule__Table__Group_2_0__1"
    // InternalPxManual.g:1597:1: rule__Table__Group_2_0__1 : rule__Table__Group_2_0__1__Impl ;
    public final void rule__Table__Group_2_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1601:1: ( rule__Table__Group_2_0__1__Impl )
            // InternalPxManual.g:1602:2: rule__Table__Group_2_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Table__Group_2_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group_2_0__1"


    // $ANTLR start "rule__Table__Group_2_0__1__Impl"
    // InternalPxManual.g:1608:1: rule__Table__Group_2_0__1__Impl : ( ( rule__Table__StyleAssignment_2_0_1 ) ) ;
    public final void rule__Table__Group_2_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1612:1: ( ( ( rule__Table__StyleAssignment_2_0_1 ) ) )
            // InternalPxManual.g:1613:1: ( ( rule__Table__StyleAssignment_2_0_1 ) )
            {
            // InternalPxManual.g:1613:1: ( ( rule__Table__StyleAssignment_2_0_1 ) )
            // InternalPxManual.g:1614:2: ( rule__Table__StyleAssignment_2_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getStyleAssignment_2_0_1()); 
            }
            // InternalPxManual.g:1615:2: ( rule__Table__StyleAssignment_2_0_1 )
            // InternalPxManual.g:1615:3: rule__Table__StyleAssignment_2_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Table__StyleAssignment_2_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getStyleAssignment_2_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group_2_0__1__Impl"


    // $ANTLR start "rule__Table__Group_2_1__0"
    // InternalPxManual.g:1624:1: rule__Table__Group_2_1__0 : rule__Table__Group_2_1__0__Impl rule__Table__Group_2_1__1 ;
    public final void rule__Table__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1628:1: ( rule__Table__Group_2_1__0__Impl rule__Table__Group_2_1__1 )
            // InternalPxManual.g:1629:2: rule__Table__Group_2_1__0__Impl rule__Table__Group_2_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Table__Group_2_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Table__Group_2_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group_2_1__0"


    // $ANTLR start "rule__Table__Group_2_1__0__Impl"
    // InternalPxManual.g:1636:1: rule__Table__Group_2_1__0__Impl : ( 'className:' ) ;
    public final void rule__Table__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1640:1: ( ( 'className:' ) )
            // InternalPxManual.g:1641:1: ( 'className:' )
            {
            // InternalPxManual.g:1641:1: ( 'className:' )
            // InternalPxManual.g:1642:2: 'className:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getClassNameKeyword_2_1_0()); 
            }
            match(input,27,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getClassNameKeyword_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group_2_1__0__Impl"


    // $ANTLR start "rule__Table__Group_2_1__1"
    // InternalPxManual.g:1651:1: rule__Table__Group_2_1__1 : rule__Table__Group_2_1__1__Impl ;
    public final void rule__Table__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1655:1: ( rule__Table__Group_2_1__1__Impl )
            // InternalPxManual.g:1656:2: rule__Table__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Table__Group_2_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group_2_1__1"


    // $ANTLR start "rule__Table__Group_2_1__1__Impl"
    // InternalPxManual.g:1662:1: rule__Table__Group_2_1__1__Impl : ( ( rule__Table__ClassNameAssignment_2_1_1 ) ) ;
    public final void rule__Table__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1666:1: ( ( ( rule__Table__ClassNameAssignment_2_1_1 ) ) )
            // InternalPxManual.g:1667:1: ( ( rule__Table__ClassNameAssignment_2_1_1 ) )
            {
            // InternalPxManual.g:1667:1: ( ( rule__Table__ClassNameAssignment_2_1_1 ) )
            // InternalPxManual.g:1668:2: ( rule__Table__ClassNameAssignment_2_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getClassNameAssignment_2_1_1()); 
            }
            // InternalPxManual.g:1669:2: ( rule__Table__ClassNameAssignment_2_1_1 )
            // InternalPxManual.g:1669:3: rule__Table__ClassNameAssignment_2_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Table__ClassNameAssignment_2_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getClassNameAssignment_2_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group_2_1__1__Impl"


    // $ANTLR start "rule__Row__Group__0"
    // InternalPxManual.g:1678:1: rule__Row__Group__0 : rule__Row__Group__0__Impl rule__Row__Group__1 ;
    public final void rule__Row__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1682:1: ( rule__Row__Group__0__Impl rule__Row__Group__1 )
            // InternalPxManual.g:1683:2: rule__Row__Group__0__Impl rule__Row__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__Row__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Row__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__0"


    // $ANTLR start "rule__Row__Group__0__Impl"
    // InternalPxManual.g:1690:1: rule__Row__Group__0__Impl : ( () ) ;
    public final void rule__Row__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1694:1: ( ( () ) )
            // InternalPxManual.g:1695:1: ( () )
            {
            // InternalPxManual.g:1695:1: ( () )
            // InternalPxManual.g:1696:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getRowAction_0()); 
            }
            // InternalPxManual.g:1697:2: ()
            // InternalPxManual.g:1697:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getRowAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__0__Impl"


    // $ANTLR start "rule__Row__Group__1"
    // InternalPxManual.g:1705:1: rule__Row__Group__1 : rule__Row__Group__1__Impl rule__Row__Group__2 ;
    public final void rule__Row__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1709:1: ( rule__Row__Group__1__Impl rule__Row__Group__2 )
            // InternalPxManual.g:1710:2: rule__Row__Group__1__Impl rule__Row__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Row__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Row__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__1"


    // $ANTLR start "rule__Row__Group__1__Impl"
    // InternalPxManual.g:1717:1: rule__Row__Group__1__Impl : ( 'row' ) ;
    public final void rule__Row__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1721:1: ( ( 'row' ) )
            // InternalPxManual.g:1722:1: ( 'row' )
            {
            // InternalPxManual.g:1722:1: ( 'row' )
            // InternalPxManual.g:1723:2: 'row'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getRowKeyword_1()); 
            }
            match(input,28,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getRowKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__1__Impl"


    // $ANTLR start "rule__Row__Group__2"
    // InternalPxManual.g:1732:1: rule__Row__Group__2 : rule__Row__Group__2__Impl rule__Row__Group__3 ;
    public final void rule__Row__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1736:1: ( rule__Row__Group__2__Impl rule__Row__Group__3 )
            // InternalPxManual.g:1737:2: rule__Row__Group__2__Impl rule__Row__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Row__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Row__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__2"


    // $ANTLR start "rule__Row__Group__2__Impl"
    // InternalPxManual.g:1744:1: rule__Row__Group__2__Impl : ( ( rule__Row__UnorderedGroup_2 ) ) ;
    public final void rule__Row__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1748:1: ( ( ( rule__Row__UnorderedGroup_2 ) ) )
            // InternalPxManual.g:1749:1: ( ( rule__Row__UnorderedGroup_2 ) )
            {
            // InternalPxManual.g:1749:1: ( ( rule__Row__UnorderedGroup_2 ) )
            // InternalPxManual.g:1750:2: ( rule__Row__UnorderedGroup_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getUnorderedGroup_2()); 
            }
            // InternalPxManual.g:1751:2: ( rule__Row__UnorderedGroup_2 )
            // InternalPxManual.g:1751:3: rule__Row__UnorderedGroup_2
            {
            pushFollow(FOLLOW_2);
            rule__Row__UnorderedGroup_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getUnorderedGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__2__Impl"


    // $ANTLR start "rule__Row__Group__3"
    // InternalPxManual.g:1759:1: rule__Row__Group__3 : rule__Row__Group__3__Impl rule__Row__Group__4 ;
    public final void rule__Row__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1763:1: ( rule__Row__Group__3__Impl rule__Row__Group__4 )
            // InternalPxManual.g:1764:2: rule__Row__Group__3__Impl rule__Row__Group__4
            {
            pushFollow(FOLLOW_20);
            rule__Row__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Row__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__3"


    // $ANTLR start "rule__Row__Group__3__Impl"
    // InternalPxManual.g:1771:1: rule__Row__Group__3__Impl : ( '{' ) ;
    public final void rule__Row__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1775:1: ( ( '{' ) )
            // InternalPxManual.g:1776:1: ( '{' )
            {
            // InternalPxManual.g:1776:1: ( '{' )
            // InternalPxManual.g:1777:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,18,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__3__Impl"


    // $ANTLR start "rule__Row__Group__4"
    // InternalPxManual.g:1786:1: rule__Row__Group__4 : rule__Row__Group__4__Impl rule__Row__Group__5 ;
    public final void rule__Row__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1790:1: ( rule__Row__Group__4__Impl rule__Row__Group__5 )
            // InternalPxManual.g:1791:2: rule__Row__Group__4__Impl rule__Row__Group__5
            {
            pushFollow(FOLLOW_20);
            rule__Row__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Row__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__4"


    // $ANTLR start "rule__Row__Group__4__Impl"
    // InternalPxManual.g:1798:1: rule__Row__Group__4__Impl : ( ( rule__Row__CellsAssignment_4 )* ) ;
    public final void rule__Row__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1802:1: ( ( ( rule__Row__CellsAssignment_4 )* ) )
            // InternalPxManual.g:1803:1: ( ( rule__Row__CellsAssignment_4 )* )
            {
            // InternalPxManual.g:1803:1: ( ( rule__Row__CellsAssignment_4 )* )
            // InternalPxManual.g:1804:2: ( rule__Row__CellsAssignment_4 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getCellsAssignment_4()); 
            }
            // InternalPxManual.g:1805:2: ( rule__Row__CellsAssignment_4 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==29) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalPxManual.g:1805:3: rule__Row__CellsAssignment_4
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__Row__CellsAssignment_4();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getCellsAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__4__Impl"


    // $ANTLR start "rule__Row__Group__5"
    // InternalPxManual.g:1813:1: rule__Row__Group__5 : rule__Row__Group__5__Impl ;
    public final void rule__Row__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1817:1: ( rule__Row__Group__5__Impl )
            // InternalPxManual.g:1818:2: rule__Row__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Row__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__5"


    // $ANTLR start "rule__Row__Group__5__Impl"
    // InternalPxManual.g:1824:1: rule__Row__Group__5__Impl : ( '}' ) ;
    public final void rule__Row__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1828:1: ( ( '}' ) )
            // InternalPxManual.g:1829:1: ( '}' )
            {
            // InternalPxManual.g:1829:1: ( '}' )
            // InternalPxManual.g:1830:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getRightCurlyBracketKeyword_5()); 
            }
            match(input,19,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getRightCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__5__Impl"


    // $ANTLR start "rule__Row__Group_2_0__0"
    // InternalPxManual.g:1840:1: rule__Row__Group_2_0__0 : rule__Row__Group_2_0__0__Impl rule__Row__Group_2_0__1 ;
    public final void rule__Row__Group_2_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1844:1: ( rule__Row__Group_2_0__0__Impl rule__Row__Group_2_0__1 )
            // InternalPxManual.g:1845:2: rule__Row__Group_2_0__0__Impl rule__Row__Group_2_0__1
            {
            pushFollow(FOLLOW_6);
            rule__Row__Group_2_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Row__Group_2_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group_2_0__0"


    // $ANTLR start "rule__Row__Group_2_0__0__Impl"
    // InternalPxManual.g:1852:1: rule__Row__Group_2_0__0__Impl : ( 'style:' ) ;
    public final void rule__Row__Group_2_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1856:1: ( ( 'style:' ) )
            // InternalPxManual.g:1857:1: ( 'style:' )
            {
            // InternalPxManual.g:1857:1: ( 'style:' )
            // InternalPxManual.g:1858:2: 'style:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getStyleKeyword_2_0_0()); 
            }
            match(input,26,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getStyleKeyword_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group_2_0__0__Impl"


    // $ANTLR start "rule__Row__Group_2_0__1"
    // InternalPxManual.g:1867:1: rule__Row__Group_2_0__1 : rule__Row__Group_2_0__1__Impl ;
    public final void rule__Row__Group_2_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1871:1: ( rule__Row__Group_2_0__1__Impl )
            // InternalPxManual.g:1872:2: rule__Row__Group_2_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Row__Group_2_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group_2_0__1"


    // $ANTLR start "rule__Row__Group_2_0__1__Impl"
    // InternalPxManual.g:1878:1: rule__Row__Group_2_0__1__Impl : ( ( rule__Row__StyleAssignment_2_0_1 ) ) ;
    public final void rule__Row__Group_2_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1882:1: ( ( ( rule__Row__StyleAssignment_2_0_1 ) ) )
            // InternalPxManual.g:1883:1: ( ( rule__Row__StyleAssignment_2_0_1 ) )
            {
            // InternalPxManual.g:1883:1: ( ( rule__Row__StyleAssignment_2_0_1 ) )
            // InternalPxManual.g:1884:2: ( rule__Row__StyleAssignment_2_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getStyleAssignment_2_0_1()); 
            }
            // InternalPxManual.g:1885:2: ( rule__Row__StyleAssignment_2_0_1 )
            // InternalPxManual.g:1885:3: rule__Row__StyleAssignment_2_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Row__StyleAssignment_2_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getStyleAssignment_2_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group_2_0__1__Impl"


    // $ANTLR start "rule__Row__Group_2_1__0"
    // InternalPxManual.g:1894:1: rule__Row__Group_2_1__0 : rule__Row__Group_2_1__0__Impl rule__Row__Group_2_1__1 ;
    public final void rule__Row__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1898:1: ( rule__Row__Group_2_1__0__Impl rule__Row__Group_2_1__1 )
            // InternalPxManual.g:1899:2: rule__Row__Group_2_1__0__Impl rule__Row__Group_2_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Row__Group_2_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Row__Group_2_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group_2_1__0"


    // $ANTLR start "rule__Row__Group_2_1__0__Impl"
    // InternalPxManual.g:1906:1: rule__Row__Group_2_1__0__Impl : ( 'className:' ) ;
    public final void rule__Row__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1910:1: ( ( 'className:' ) )
            // InternalPxManual.g:1911:1: ( 'className:' )
            {
            // InternalPxManual.g:1911:1: ( 'className:' )
            // InternalPxManual.g:1912:2: 'className:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getClassNameKeyword_2_1_0()); 
            }
            match(input,27,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getClassNameKeyword_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group_2_1__0__Impl"


    // $ANTLR start "rule__Row__Group_2_1__1"
    // InternalPxManual.g:1921:1: rule__Row__Group_2_1__1 : rule__Row__Group_2_1__1__Impl ;
    public final void rule__Row__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1925:1: ( rule__Row__Group_2_1__1__Impl )
            // InternalPxManual.g:1926:2: rule__Row__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Row__Group_2_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group_2_1__1"


    // $ANTLR start "rule__Row__Group_2_1__1__Impl"
    // InternalPxManual.g:1932:1: rule__Row__Group_2_1__1__Impl : ( ( rule__Row__ClassNameAssignment_2_1_1 ) ) ;
    public final void rule__Row__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1936:1: ( ( ( rule__Row__ClassNameAssignment_2_1_1 ) ) )
            // InternalPxManual.g:1937:1: ( ( rule__Row__ClassNameAssignment_2_1_1 ) )
            {
            // InternalPxManual.g:1937:1: ( ( rule__Row__ClassNameAssignment_2_1_1 ) )
            // InternalPxManual.g:1938:2: ( rule__Row__ClassNameAssignment_2_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getClassNameAssignment_2_1_1()); 
            }
            // InternalPxManual.g:1939:2: ( rule__Row__ClassNameAssignment_2_1_1 )
            // InternalPxManual.g:1939:3: rule__Row__ClassNameAssignment_2_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Row__ClassNameAssignment_2_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getClassNameAssignment_2_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group_2_1__1__Impl"


    // $ANTLR start "rule__Cell__Group__0"
    // InternalPxManual.g:1948:1: rule__Cell__Group__0 : rule__Cell__Group__0__Impl rule__Cell__Group__1 ;
    public final void rule__Cell__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1952:1: ( rule__Cell__Group__0__Impl rule__Cell__Group__1 )
            // InternalPxManual.g:1953:2: rule__Cell__Group__0__Impl rule__Cell__Group__1
            {
            pushFollow(FOLLOW_22);
            rule__Cell__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Cell__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group__0"


    // $ANTLR start "rule__Cell__Group__0__Impl"
    // InternalPxManual.g:1960:1: rule__Cell__Group__0__Impl : ( 'cell' ) ;
    public final void rule__Cell__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1964:1: ( ( 'cell' ) )
            // InternalPxManual.g:1965:1: ( 'cell' )
            {
            // InternalPxManual.g:1965:1: ( 'cell' )
            // InternalPxManual.g:1966:2: 'cell'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getCellKeyword_0()); 
            }
            match(input,29,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getCellKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group__0__Impl"


    // $ANTLR start "rule__Cell__Group__1"
    // InternalPxManual.g:1975:1: rule__Cell__Group__1 : rule__Cell__Group__1__Impl rule__Cell__Group__2 ;
    public final void rule__Cell__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1979:1: ( rule__Cell__Group__1__Impl rule__Cell__Group__2 )
            // InternalPxManual.g:1980:2: rule__Cell__Group__1__Impl rule__Cell__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Cell__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Cell__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group__1"


    // $ANTLR start "rule__Cell__Group__1__Impl"
    // InternalPxManual.g:1987:1: rule__Cell__Group__1__Impl : ( ( rule__Cell__UnorderedGroup_1 ) ) ;
    public final void rule__Cell__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:1991:1: ( ( ( rule__Cell__UnorderedGroup_1 ) ) )
            // InternalPxManual.g:1992:1: ( ( rule__Cell__UnorderedGroup_1 ) )
            {
            // InternalPxManual.g:1992:1: ( ( rule__Cell__UnorderedGroup_1 ) )
            // InternalPxManual.g:1993:2: ( rule__Cell__UnorderedGroup_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getUnorderedGroup_1()); 
            }
            // InternalPxManual.g:1994:2: ( rule__Cell__UnorderedGroup_1 )
            // InternalPxManual.g:1994:3: rule__Cell__UnorderedGroup_1
            {
            pushFollow(FOLLOW_2);
            rule__Cell__UnorderedGroup_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getUnorderedGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group__1__Impl"


    // $ANTLR start "rule__Cell__Group__2"
    // InternalPxManual.g:2002:1: rule__Cell__Group__2 : rule__Cell__Group__2__Impl ;
    public final void rule__Cell__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2006:1: ( rule__Cell__Group__2__Impl )
            // InternalPxManual.g:2007:2: rule__Cell__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cell__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group__2"


    // $ANTLR start "rule__Cell__Group__2__Impl"
    // InternalPxManual.g:2013:1: rule__Cell__Group__2__Impl : ( ( rule__Cell__BodyAssignment_2 ) ) ;
    public final void rule__Cell__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2017:1: ( ( ( rule__Cell__BodyAssignment_2 ) ) )
            // InternalPxManual.g:2018:1: ( ( rule__Cell__BodyAssignment_2 ) )
            {
            // InternalPxManual.g:2018:1: ( ( rule__Cell__BodyAssignment_2 ) )
            // InternalPxManual.g:2019:2: ( rule__Cell__BodyAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getBodyAssignment_2()); 
            }
            // InternalPxManual.g:2020:2: ( rule__Cell__BodyAssignment_2 )
            // InternalPxManual.g:2020:3: rule__Cell__BodyAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Cell__BodyAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getBodyAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group__2__Impl"


    // $ANTLR start "rule__Cell__Group_1_0__0"
    // InternalPxManual.g:2029:1: rule__Cell__Group_1_0__0 : rule__Cell__Group_1_0__0__Impl rule__Cell__Group_1_0__1 ;
    public final void rule__Cell__Group_1_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2033:1: ( rule__Cell__Group_1_0__0__Impl rule__Cell__Group_1_0__1 )
            // InternalPxManual.g:2034:2: rule__Cell__Group_1_0__0__Impl rule__Cell__Group_1_0__1
            {
            pushFollow(FOLLOW_6);
            rule__Cell__Group_1_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Cell__Group_1_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_0__0"


    // $ANTLR start "rule__Cell__Group_1_0__0__Impl"
    // InternalPxManual.g:2041:1: rule__Cell__Group_1_0__0__Impl : ( 'style:' ) ;
    public final void rule__Cell__Group_1_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2045:1: ( ( 'style:' ) )
            // InternalPxManual.g:2046:1: ( 'style:' )
            {
            // InternalPxManual.g:2046:1: ( 'style:' )
            // InternalPxManual.g:2047:2: 'style:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getStyleKeyword_1_0_0()); 
            }
            match(input,26,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getStyleKeyword_1_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_0__0__Impl"


    // $ANTLR start "rule__Cell__Group_1_0__1"
    // InternalPxManual.g:2056:1: rule__Cell__Group_1_0__1 : rule__Cell__Group_1_0__1__Impl ;
    public final void rule__Cell__Group_1_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2060:1: ( rule__Cell__Group_1_0__1__Impl )
            // InternalPxManual.g:2061:2: rule__Cell__Group_1_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cell__Group_1_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_0__1"


    // $ANTLR start "rule__Cell__Group_1_0__1__Impl"
    // InternalPxManual.g:2067:1: rule__Cell__Group_1_0__1__Impl : ( ( rule__Cell__StyleAssignment_1_0_1 ) ) ;
    public final void rule__Cell__Group_1_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2071:1: ( ( ( rule__Cell__StyleAssignment_1_0_1 ) ) )
            // InternalPxManual.g:2072:1: ( ( rule__Cell__StyleAssignment_1_0_1 ) )
            {
            // InternalPxManual.g:2072:1: ( ( rule__Cell__StyleAssignment_1_0_1 ) )
            // InternalPxManual.g:2073:2: ( rule__Cell__StyleAssignment_1_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getStyleAssignment_1_0_1()); 
            }
            // InternalPxManual.g:2074:2: ( rule__Cell__StyleAssignment_1_0_1 )
            // InternalPxManual.g:2074:3: rule__Cell__StyleAssignment_1_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Cell__StyleAssignment_1_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getStyleAssignment_1_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_0__1__Impl"


    // $ANTLR start "rule__Cell__Group_1_1__0"
    // InternalPxManual.g:2083:1: rule__Cell__Group_1_1__0 : rule__Cell__Group_1_1__0__Impl rule__Cell__Group_1_1__1 ;
    public final void rule__Cell__Group_1_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2087:1: ( rule__Cell__Group_1_1__0__Impl rule__Cell__Group_1_1__1 )
            // InternalPxManual.g:2088:2: rule__Cell__Group_1_1__0__Impl rule__Cell__Group_1_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Cell__Group_1_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Cell__Group_1_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_1__0"


    // $ANTLR start "rule__Cell__Group_1_1__0__Impl"
    // InternalPxManual.g:2095:1: rule__Cell__Group_1_1__0__Impl : ( 'className:' ) ;
    public final void rule__Cell__Group_1_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2099:1: ( ( 'className:' ) )
            // InternalPxManual.g:2100:1: ( 'className:' )
            {
            // InternalPxManual.g:2100:1: ( 'className:' )
            // InternalPxManual.g:2101:2: 'className:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getClassNameKeyword_1_1_0()); 
            }
            match(input,27,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getClassNameKeyword_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_1__0__Impl"


    // $ANTLR start "rule__Cell__Group_1_1__1"
    // InternalPxManual.g:2110:1: rule__Cell__Group_1_1__1 : rule__Cell__Group_1_1__1__Impl ;
    public final void rule__Cell__Group_1_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2114:1: ( rule__Cell__Group_1_1__1__Impl )
            // InternalPxManual.g:2115:2: rule__Cell__Group_1_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cell__Group_1_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_1__1"


    // $ANTLR start "rule__Cell__Group_1_1__1__Impl"
    // InternalPxManual.g:2121:1: rule__Cell__Group_1_1__1__Impl : ( ( rule__Cell__ClassNameAssignment_1_1_1 ) ) ;
    public final void rule__Cell__Group_1_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2125:1: ( ( ( rule__Cell__ClassNameAssignment_1_1_1 ) ) )
            // InternalPxManual.g:2126:1: ( ( rule__Cell__ClassNameAssignment_1_1_1 ) )
            {
            // InternalPxManual.g:2126:1: ( ( rule__Cell__ClassNameAssignment_1_1_1 ) )
            // InternalPxManual.g:2127:2: ( rule__Cell__ClassNameAssignment_1_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getClassNameAssignment_1_1_1()); 
            }
            // InternalPxManual.g:2128:2: ( rule__Cell__ClassNameAssignment_1_1_1 )
            // InternalPxManual.g:2128:3: rule__Cell__ClassNameAssignment_1_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Cell__ClassNameAssignment_1_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getClassNameAssignment_1_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_1__1__Impl"


    // $ANTLR start "rule__Cell__Group_1_2__0"
    // InternalPxManual.g:2137:1: rule__Cell__Group_1_2__0 : rule__Cell__Group_1_2__0__Impl rule__Cell__Group_1_2__1 ;
    public final void rule__Cell__Group_1_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2141:1: ( rule__Cell__Group_1_2__0__Impl rule__Cell__Group_1_2__1 )
            // InternalPxManual.g:2142:2: rule__Cell__Group_1_2__0__Impl rule__Cell__Group_1_2__1
            {
            pushFollow(FOLLOW_7);
            rule__Cell__Group_1_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Cell__Group_1_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_2__0"


    // $ANTLR start "rule__Cell__Group_1_2__0__Impl"
    // InternalPxManual.g:2149:1: rule__Cell__Group_1_2__0__Impl : ( 'span:' ) ;
    public final void rule__Cell__Group_1_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2153:1: ( ( 'span:' ) )
            // InternalPxManual.g:2154:1: ( 'span:' )
            {
            // InternalPxManual.g:2154:1: ( 'span:' )
            // InternalPxManual.g:2155:2: 'span:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getSpanKeyword_1_2_0()); 
            }
            match(input,30,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getSpanKeyword_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_2__0__Impl"


    // $ANTLR start "rule__Cell__Group_1_2__1"
    // InternalPxManual.g:2164:1: rule__Cell__Group_1_2__1 : rule__Cell__Group_1_2__1__Impl ;
    public final void rule__Cell__Group_1_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2168:1: ( rule__Cell__Group_1_2__1__Impl )
            // InternalPxManual.g:2169:2: rule__Cell__Group_1_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cell__Group_1_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_2__1"


    // $ANTLR start "rule__Cell__Group_1_2__1__Impl"
    // InternalPxManual.g:2175:1: rule__Cell__Group_1_2__1__Impl : ( ( rule__Cell__SpanAssignment_1_2_1 ) ) ;
    public final void rule__Cell__Group_1_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2179:1: ( ( ( rule__Cell__SpanAssignment_1_2_1 ) ) )
            // InternalPxManual.g:2180:1: ( ( rule__Cell__SpanAssignment_1_2_1 ) )
            {
            // InternalPxManual.g:2180:1: ( ( rule__Cell__SpanAssignment_1_2_1 ) )
            // InternalPxManual.g:2181:2: ( rule__Cell__SpanAssignment_1_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getSpanAssignment_1_2_1()); 
            }
            // InternalPxManual.g:2182:2: ( rule__Cell__SpanAssignment_1_2_1 )
            // InternalPxManual.g:2182:3: rule__Cell__SpanAssignment_1_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Cell__SpanAssignment_1_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getSpanAssignment_1_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_2__1__Impl"


    // $ANTLR start "rule__Cell__Group_1_3__0"
    // InternalPxManual.g:2191:1: rule__Cell__Group_1_3__0 : rule__Cell__Group_1_3__0__Impl rule__Cell__Group_1_3__1 ;
    public final void rule__Cell__Group_1_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2195:1: ( rule__Cell__Group_1_3__0__Impl rule__Cell__Group_1_3__1 )
            // InternalPxManual.g:2196:2: rule__Cell__Group_1_3__0__Impl rule__Cell__Group_1_3__1
            {
            pushFollow(FOLLOW_6);
            rule__Cell__Group_1_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Cell__Group_1_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_3__0"


    // $ANTLR start "rule__Cell__Group_1_3__0__Impl"
    // InternalPxManual.g:2203:1: rule__Cell__Group_1_3__0__Impl : ( 'width:' ) ;
    public final void rule__Cell__Group_1_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2207:1: ( ( 'width:' ) )
            // InternalPxManual.g:2208:1: ( 'width:' )
            {
            // InternalPxManual.g:2208:1: ( 'width:' )
            // InternalPxManual.g:2209:2: 'width:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getWidthKeyword_1_3_0()); 
            }
            match(input,31,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getWidthKeyword_1_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_3__0__Impl"


    // $ANTLR start "rule__Cell__Group_1_3__1"
    // InternalPxManual.g:2218:1: rule__Cell__Group_1_3__1 : rule__Cell__Group_1_3__1__Impl ;
    public final void rule__Cell__Group_1_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2222:1: ( rule__Cell__Group_1_3__1__Impl )
            // InternalPxManual.g:2223:2: rule__Cell__Group_1_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cell__Group_1_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_3__1"


    // $ANTLR start "rule__Cell__Group_1_3__1__Impl"
    // InternalPxManual.g:2229:1: rule__Cell__Group_1_3__1__Impl : ( ( rule__Cell__WidthAssignment_1_3_1 ) ) ;
    public final void rule__Cell__Group_1_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2233:1: ( ( ( rule__Cell__WidthAssignment_1_3_1 ) ) )
            // InternalPxManual.g:2234:1: ( ( rule__Cell__WidthAssignment_1_3_1 ) )
            {
            // InternalPxManual.g:2234:1: ( ( rule__Cell__WidthAssignment_1_3_1 ) )
            // InternalPxManual.g:2235:2: ( rule__Cell__WidthAssignment_1_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getWidthAssignment_1_3_1()); 
            }
            // InternalPxManual.g:2236:2: ( rule__Cell__WidthAssignment_1_3_1 )
            // InternalPxManual.g:2236:3: rule__Cell__WidthAssignment_1_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Cell__WidthAssignment_1_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getWidthAssignment_1_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_3__1__Impl"


    // $ANTLR start "rule__Cell__Group_1_4__0"
    // InternalPxManual.g:2245:1: rule__Cell__Group_1_4__0 : rule__Cell__Group_1_4__0__Impl rule__Cell__Group_1_4__1 ;
    public final void rule__Cell__Group_1_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2249:1: ( rule__Cell__Group_1_4__0__Impl rule__Cell__Group_1_4__1 )
            // InternalPxManual.g:2250:2: rule__Cell__Group_1_4__0__Impl rule__Cell__Group_1_4__1
            {
            pushFollow(FOLLOW_23);
            rule__Cell__Group_1_4__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Cell__Group_1_4__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_4__0"


    // $ANTLR start "rule__Cell__Group_1_4__0__Impl"
    // InternalPxManual.g:2257:1: rule__Cell__Group_1_4__0__Impl : ( 'vAlign:' ) ;
    public final void rule__Cell__Group_1_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2261:1: ( ( 'vAlign:' ) )
            // InternalPxManual.g:2262:1: ( 'vAlign:' )
            {
            // InternalPxManual.g:2262:1: ( 'vAlign:' )
            // InternalPxManual.g:2263:2: 'vAlign:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getVAlignKeyword_1_4_0()); 
            }
            match(input,32,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getVAlignKeyword_1_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_4__0__Impl"


    // $ANTLR start "rule__Cell__Group_1_4__1"
    // InternalPxManual.g:2272:1: rule__Cell__Group_1_4__1 : rule__Cell__Group_1_4__1__Impl ;
    public final void rule__Cell__Group_1_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2276:1: ( rule__Cell__Group_1_4__1__Impl )
            // InternalPxManual.g:2277:2: rule__Cell__Group_1_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cell__Group_1_4__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_4__1"


    // $ANTLR start "rule__Cell__Group_1_4__1__Impl"
    // InternalPxManual.g:2283:1: rule__Cell__Group_1_4__1__Impl : ( ( rule__Cell__VAlignAssignment_1_4_1 ) ) ;
    public final void rule__Cell__Group_1_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2287:1: ( ( ( rule__Cell__VAlignAssignment_1_4_1 ) ) )
            // InternalPxManual.g:2288:1: ( ( rule__Cell__VAlignAssignment_1_4_1 ) )
            {
            // InternalPxManual.g:2288:1: ( ( rule__Cell__VAlignAssignment_1_4_1 ) )
            // InternalPxManual.g:2289:2: ( rule__Cell__VAlignAssignment_1_4_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getVAlignAssignment_1_4_1()); 
            }
            // InternalPxManual.g:2290:2: ( rule__Cell__VAlignAssignment_1_4_1 )
            // InternalPxManual.g:2290:3: rule__Cell__VAlignAssignment_1_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Cell__VAlignAssignment_1_4_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getVAlignAssignment_1_4_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__Group_1_4__1__Impl"


    // $ANTLR start "rule__Image__Group__0"
    // InternalPxManual.g:2299:1: rule__Image__Group__0 : rule__Image__Group__0__Impl rule__Image__Group__1 ;
    public final void rule__Image__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2303:1: ( rule__Image__Group__0__Impl rule__Image__Group__1 )
            // InternalPxManual.g:2304:2: rule__Image__Group__0__Impl rule__Image__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__Image__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Image__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__0"


    // $ANTLR start "rule__Image__Group__0__Impl"
    // InternalPxManual.g:2311:1: rule__Image__Group__0__Impl : ( 'image' ) ;
    public final void rule__Image__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2315:1: ( ( 'image' ) )
            // InternalPxManual.g:2316:1: ( 'image' )
            {
            // InternalPxManual.g:2316:1: ( 'image' )
            // InternalPxManual.g:2317:2: 'image'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getImageKeyword_0()); 
            }
            match(input,33,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getImageKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__0__Impl"


    // $ANTLR start "rule__Image__Group__1"
    // InternalPxManual.g:2326:1: rule__Image__Group__1 : rule__Image__Group__1__Impl rule__Image__Group__2 ;
    public final void rule__Image__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2330:1: ( rule__Image__Group__1__Impl rule__Image__Group__2 )
            // InternalPxManual.g:2331:2: rule__Image__Group__1__Impl rule__Image__Group__2
            {
            pushFollow(FOLLOW_24);
            rule__Image__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Image__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__1"


    // $ANTLR start "rule__Image__Group__1__Impl"
    // InternalPxManual.g:2338:1: rule__Image__Group__1__Impl : ( ( rule__Image__NameAssignment_1 )? ) ;
    public final void rule__Image__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2342:1: ( ( ( rule__Image__NameAssignment_1 )? ) )
            // InternalPxManual.g:2343:1: ( ( rule__Image__NameAssignment_1 )? )
            {
            // InternalPxManual.g:2343:1: ( ( rule__Image__NameAssignment_1 )? )
            // InternalPxManual.g:2344:2: ( rule__Image__NameAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getNameAssignment_1()); 
            }
            // InternalPxManual.g:2345:2: ( rule__Image__NameAssignment_1 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalPxManual.g:2345:3: rule__Image__NameAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Image__NameAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__1__Impl"


    // $ANTLR start "rule__Image__Group__2"
    // InternalPxManual.g:2353:1: rule__Image__Group__2 : rule__Image__Group__2__Impl rule__Image__Group__3 ;
    public final void rule__Image__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2357:1: ( rule__Image__Group__2__Impl rule__Image__Group__3 )
            // InternalPxManual.g:2358:2: rule__Image__Group__2__Impl rule__Image__Group__3
            {
            pushFollow(FOLLOW_25);
            rule__Image__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Image__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__2"


    // $ANTLR start "rule__Image__Group__2__Impl"
    // InternalPxManual.g:2365:1: rule__Image__Group__2__Impl : ( ( rule__Image__PathAssignment_2 ) ) ;
    public final void rule__Image__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2369:1: ( ( ( rule__Image__PathAssignment_2 ) ) )
            // InternalPxManual.g:2370:1: ( ( rule__Image__PathAssignment_2 ) )
            {
            // InternalPxManual.g:2370:1: ( ( rule__Image__PathAssignment_2 ) )
            // InternalPxManual.g:2371:2: ( rule__Image__PathAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getPathAssignment_2()); 
            }
            // InternalPxManual.g:2372:2: ( rule__Image__PathAssignment_2 )
            // InternalPxManual.g:2372:3: rule__Image__PathAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Image__PathAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getPathAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__2__Impl"


    // $ANTLR start "rule__Image__Group__3"
    // InternalPxManual.g:2380:1: rule__Image__Group__3 : rule__Image__Group__3__Impl ;
    public final void rule__Image__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2384:1: ( rule__Image__Group__3__Impl )
            // InternalPxManual.g:2385:2: rule__Image__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Image__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__3"


    // $ANTLR start "rule__Image__Group__3__Impl"
    // InternalPxManual.g:2391:1: rule__Image__Group__3__Impl : ( ( rule__Image__UnorderedGroup_3 ) ) ;
    public final void rule__Image__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2395:1: ( ( ( rule__Image__UnorderedGroup_3 ) ) )
            // InternalPxManual.g:2396:1: ( ( rule__Image__UnorderedGroup_3 ) )
            {
            // InternalPxManual.g:2396:1: ( ( rule__Image__UnorderedGroup_3 ) )
            // InternalPxManual.g:2397:2: ( rule__Image__UnorderedGroup_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getUnorderedGroup_3()); 
            }
            // InternalPxManual.g:2398:2: ( rule__Image__UnorderedGroup_3 )
            // InternalPxManual.g:2398:3: rule__Image__UnorderedGroup_3
            {
            pushFollow(FOLLOW_2);
            rule__Image__UnorderedGroup_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getUnorderedGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__3__Impl"


    // $ANTLR start "rule__Image__Group_3_0__0"
    // InternalPxManual.g:2407:1: rule__Image__Group_3_0__0 : rule__Image__Group_3_0__0__Impl rule__Image__Group_3_0__1 ;
    public final void rule__Image__Group_3_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2411:1: ( rule__Image__Group_3_0__0__Impl rule__Image__Group_3_0__1 )
            // InternalPxManual.g:2412:2: rule__Image__Group_3_0__0__Impl rule__Image__Group_3_0__1
            {
            pushFollow(FOLLOW_6);
            rule__Image__Group_3_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Image__Group_3_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_0__0"


    // $ANTLR start "rule__Image__Group_3_0__0__Impl"
    // InternalPxManual.g:2419:1: rule__Image__Group_3_0__0__Impl : ( 'style:' ) ;
    public final void rule__Image__Group_3_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2423:1: ( ( 'style:' ) )
            // InternalPxManual.g:2424:1: ( 'style:' )
            {
            // InternalPxManual.g:2424:1: ( 'style:' )
            // InternalPxManual.g:2425:2: 'style:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getStyleKeyword_3_0_0()); 
            }
            match(input,26,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getStyleKeyword_3_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_0__0__Impl"


    // $ANTLR start "rule__Image__Group_3_0__1"
    // InternalPxManual.g:2434:1: rule__Image__Group_3_0__1 : rule__Image__Group_3_0__1__Impl ;
    public final void rule__Image__Group_3_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2438:1: ( rule__Image__Group_3_0__1__Impl )
            // InternalPxManual.g:2439:2: rule__Image__Group_3_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Image__Group_3_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_0__1"


    // $ANTLR start "rule__Image__Group_3_0__1__Impl"
    // InternalPxManual.g:2445:1: rule__Image__Group_3_0__1__Impl : ( ( rule__Image__StyleAssignment_3_0_1 ) ) ;
    public final void rule__Image__Group_3_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2449:1: ( ( ( rule__Image__StyleAssignment_3_0_1 ) ) )
            // InternalPxManual.g:2450:1: ( ( rule__Image__StyleAssignment_3_0_1 ) )
            {
            // InternalPxManual.g:2450:1: ( ( rule__Image__StyleAssignment_3_0_1 ) )
            // InternalPxManual.g:2451:2: ( rule__Image__StyleAssignment_3_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getStyleAssignment_3_0_1()); 
            }
            // InternalPxManual.g:2452:2: ( rule__Image__StyleAssignment_3_0_1 )
            // InternalPxManual.g:2452:3: rule__Image__StyleAssignment_3_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Image__StyleAssignment_3_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getStyleAssignment_3_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_0__1__Impl"


    // $ANTLR start "rule__Image__Group_3_1__0"
    // InternalPxManual.g:2461:1: rule__Image__Group_3_1__0 : rule__Image__Group_3_1__0__Impl rule__Image__Group_3_1__1 ;
    public final void rule__Image__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2465:1: ( rule__Image__Group_3_1__0__Impl rule__Image__Group_3_1__1 )
            // InternalPxManual.g:2466:2: rule__Image__Group_3_1__0__Impl rule__Image__Group_3_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Image__Group_3_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Image__Group_3_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_1__0"


    // $ANTLR start "rule__Image__Group_3_1__0__Impl"
    // InternalPxManual.g:2473:1: rule__Image__Group_3_1__0__Impl : ( 'className:' ) ;
    public final void rule__Image__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2477:1: ( ( 'className:' ) )
            // InternalPxManual.g:2478:1: ( 'className:' )
            {
            // InternalPxManual.g:2478:1: ( 'className:' )
            // InternalPxManual.g:2479:2: 'className:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getClassNameKeyword_3_1_0()); 
            }
            match(input,27,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getClassNameKeyword_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_1__0__Impl"


    // $ANTLR start "rule__Image__Group_3_1__1"
    // InternalPxManual.g:2488:1: rule__Image__Group_3_1__1 : rule__Image__Group_3_1__1__Impl ;
    public final void rule__Image__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2492:1: ( rule__Image__Group_3_1__1__Impl )
            // InternalPxManual.g:2493:2: rule__Image__Group_3_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Image__Group_3_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_1__1"


    // $ANTLR start "rule__Image__Group_3_1__1__Impl"
    // InternalPxManual.g:2499:1: rule__Image__Group_3_1__1__Impl : ( ( rule__Image__ClassNameAssignment_3_1_1 ) ) ;
    public final void rule__Image__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2503:1: ( ( ( rule__Image__ClassNameAssignment_3_1_1 ) ) )
            // InternalPxManual.g:2504:1: ( ( rule__Image__ClassNameAssignment_3_1_1 ) )
            {
            // InternalPxManual.g:2504:1: ( ( rule__Image__ClassNameAssignment_3_1_1 ) )
            // InternalPxManual.g:2505:2: ( rule__Image__ClassNameAssignment_3_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getClassNameAssignment_3_1_1()); 
            }
            // InternalPxManual.g:2506:2: ( rule__Image__ClassNameAssignment_3_1_1 )
            // InternalPxManual.g:2506:3: rule__Image__ClassNameAssignment_3_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Image__ClassNameAssignment_3_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getClassNameAssignment_3_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_1__1__Impl"


    // $ANTLR start "rule__Image__Group_3_2__0"
    // InternalPxManual.g:2515:1: rule__Image__Group_3_2__0 : rule__Image__Group_3_2__0__Impl rule__Image__Group_3_2__1 ;
    public final void rule__Image__Group_3_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2519:1: ( rule__Image__Group_3_2__0__Impl rule__Image__Group_3_2__1 )
            // InternalPxManual.g:2520:2: rule__Image__Group_3_2__0__Impl rule__Image__Group_3_2__1
            {
            pushFollow(FOLLOW_7);
            rule__Image__Group_3_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Image__Group_3_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_2__0"


    // $ANTLR start "rule__Image__Group_3_2__0__Impl"
    // InternalPxManual.g:2527:1: rule__Image__Group_3_2__0__Impl : ( 'width:' ) ;
    public final void rule__Image__Group_3_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2531:1: ( ( 'width:' ) )
            // InternalPxManual.g:2532:1: ( 'width:' )
            {
            // InternalPxManual.g:2532:1: ( 'width:' )
            // InternalPxManual.g:2533:2: 'width:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getWidthKeyword_3_2_0()); 
            }
            match(input,31,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getWidthKeyword_3_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_2__0__Impl"


    // $ANTLR start "rule__Image__Group_3_2__1"
    // InternalPxManual.g:2542:1: rule__Image__Group_3_2__1 : rule__Image__Group_3_2__1__Impl ;
    public final void rule__Image__Group_3_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2546:1: ( rule__Image__Group_3_2__1__Impl )
            // InternalPxManual.g:2547:2: rule__Image__Group_3_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Image__Group_3_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_2__1"


    // $ANTLR start "rule__Image__Group_3_2__1__Impl"
    // InternalPxManual.g:2553:1: rule__Image__Group_3_2__1__Impl : ( ( rule__Image__WidthAssignment_3_2_1 ) ) ;
    public final void rule__Image__Group_3_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2557:1: ( ( ( rule__Image__WidthAssignment_3_2_1 ) ) )
            // InternalPxManual.g:2558:1: ( ( rule__Image__WidthAssignment_3_2_1 ) )
            {
            // InternalPxManual.g:2558:1: ( ( rule__Image__WidthAssignment_3_2_1 ) )
            // InternalPxManual.g:2559:2: ( rule__Image__WidthAssignment_3_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getWidthAssignment_3_2_1()); 
            }
            // InternalPxManual.g:2560:2: ( rule__Image__WidthAssignment_3_2_1 )
            // InternalPxManual.g:2560:3: rule__Image__WidthAssignment_3_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Image__WidthAssignment_3_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getWidthAssignment_3_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group_3_2__1__Impl"


    // $ANTLR start "rule__Section__Group__0"
    // InternalPxManual.g:2569:1: rule__Section__Group__0 : rule__Section__Group__0__Impl rule__Section__Group__1 ;
    public final void rule__Section__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2573:1: ( rule__Section__Group__0__Impl rule__Section__Group__1 )
            // InternalPxManual.g:2574:2: rule__Section__Group__0__Impl rule__Section__Group__1
            {
            pushFollow(FOLLOW_26);
            rule__Section__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Section__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__0"


    // $ANTLR start "rule__Section__Group__0__Impl"
    // InternalPxManual.g:2581:1: rule__Section__Group__0__Impl : ( 'section' ) ;
    public final void rule__Section__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2585:1: ( ( 'section' ) )
            // InternalPxManual.g:2586:1: ( 'section' )
            {
            // InternalPxManual.g:2586:1: ( 'section' )
            // InternalPxManual.g:2587:2: 'section'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSectionAccess().getSectionKeyword_0()); 
            }
            match(input,34,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSectionAccess().getSectionKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__0__Impl"


    // $ANTLR start "rule__Section__Group__1"
    // InternalPxManual.g:2596:1: rule__Section__Group__1 : rule__Section__Group__1__Impl rule__Section__Group__2 ;
    public final void rule__Section__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2600:1: ( rule__Section__Group__1__Impl rule__Section__Group__2 )
            // InternalPxManual.g:2601:2: rule__Section__Group__1__Impl rule__Section__Group__2
            {
            pushFollow(FOLLOW_26);
            rule__Section__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Section__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__1"


    // $ANTLR start "rule__Section__Group__1__Impl"
    // InternalPxManual.g:2608:1: rule__Section__Group__1__Impl : ( ( rule__Section__NameAssignment_1 )? ) ;
    public final void rule__Section__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2612:1: ( ( ( rule__Section__NameAssignment_1 )? ) )
            // InternalPxManual.g:2613:1: ( ( rule__Section__NameAssignment_1 )? )
            {
            // InternalPxManual.g:2613:1: ( ( rule__Section__NameAssignment_1 )? )
            // InternalPxManual.g:2614:2: ( rule__Section__NameAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSectionAccess().getNameAssignment_1()); 
            }
            // InternalPxManual.g:2615:2: ( rule__Section__NameAssignment_1 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_ID) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalPxManual.g:2615:3: rule__Section__NameAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Section__NameAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSectionAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__1__Impl"


    // $ANTLR start "rule__Section__Group__2"
    // InternalPxManual.g:2623:1: rule__Section__Group__2 : rule__Section__Group__2__Impl rule__Section__Group__3 ;
    public final void rule__Section__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2627:1: ( rule__Section__Group__2__Impl rule__Section__Group__3 )
            // InternalPxManual.g:2628:2: rule__Section__Group__2__Impl rule__Section__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Section__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Section__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__2"


    // $ANTLR start "rule__Section__Group__2__Impl"
    // InternalPxManual.g:2635:1: rule__Section__Group__2__Impl : ( ( rule__Section__TitleAssignment_2 ) ) ;
    public final void rule__Section__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2639:1: ( ( ( rule__Section__TitleAssignment_2 ) ) )
            // InternalPxManual.g:2640:1: ( ( rule__Section__TitleAssignment_2 ) )
            {
            // InternalPxManual.g:2640:1: ( ( rule__Section__TitleAssignment_2 ) )
            // InternalPxManual.g:2641:2: ( rule__Section__TitleAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSectionAccess().getTitleAssignment_2()); 
            }
            // InternalPxManual.g:2642:2: ( rule__Section__TitleAssignment_2 )
            // InternalPxManual.g:2642:3: rule__Section__TitleAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Section__TitleAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSectionAccess().getTitleAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__2__Impl"


    // $ANTLR start "rule__Section__Group__3"
    // InternalPxManual.g:2650:1: rule__Section__Group__3 : rule__Section__Group__3__Impl rule__Section__Group__4 ;
    public final void rule__Section__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2654:1: ( rule__Section__Group__3__Impl rule__Section__Group__4 )
            // InternalPxManual.g:2655:2: rule__Section__Group__3__Impl rule__Section__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Section__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Section__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__3"


    // $ANTLR start "rule__Section__Group__3__Impl"
    // InternalPxManual.g:2662:1: rule__Section__Group__3__Impl : ( '{' ) ;
    public final void rule__Section__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2666:1: ( ( '{' ) )
            // InternalPxManual.g:2667:1: ( '{' )
            {
            // InternalPxManual.g:2667:1: ( '{' )
            // InternalPxManual.g:2668:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSectionAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,18,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSectionAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__3__Impl"


    // $ANTLR start "rule__Section__Group__4"
    // InternalPxManual.g:2677:1: rule__Section__Group__4 : rule__Section__Group__4__Impl rule__Section__Group__5 ;
    public final void rule__Section__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2681:1: ( rule__Section__Group__4__Impl rule__Section__Group__5 )
            // InternalPxManual.g:2682:2: rule__Section__Group__4__Impl rule__Section__Group__5
            {
            pushFollow(FOLLOW_8);
            rule__Section__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Section__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__4"


    // $ANTLR start "rule__Section__Group__4__Impl"
    // InternalPxManual.g:2689:1: rule__Section__Group__4__Impl : ( ( rule__Section__ElementsAssignment_4 )* ) ;
    public final void rule__Section__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2693:1: ( ( ( rule__Section__ElementsAssignment_4 )* ) )
            // InternalPxManual.g:2694:1: ( ( rule__Section__ElementsAssignment_4 )* )
            {
            // InternalPxManual.g:2694:1: ( ( rule__Section__ElementsAssignment_4 )* )
            // InternalPxManual.g:2695:2: ( rule__Section__ElementsAssignment_4 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSectionAccess().getElementsAssignment_4()); 
            }
            // InternalPxManual.g:2696:2: ( rule__Section__ElementsAssignment_4 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_STRING||LA11_0==20||LA11_0==22||(LA11_0>=24 && LA11_0<=25)||(LA11_0>=33 && LA11_0<=36)||(LA11_0>=38 && LA11_0<=41)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalPxManual.g:2696:3: rule__Section__ElementsAssignment_4
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Section__ElementsAssignment_4();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSectionAccess().getElementsAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__4__Impl"


    // $ANTLR start "rule__Section__Group__5"
    // InternalPxManual.g:2704:1: rule__Section__Group__5 : rule__Section__Group__5__Impl ;
    public final void rule__Section__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2708:1: ( rule__Section__Group__5__Impl )
            // InternalPxManual.g:2709:2: rule__Section__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Section__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__5"


    // $ANTLR start "rule__Section__Group__5__Impl"
    // InternalPxManual.g:2715:1: rule__Section__Group__5__Impl : ( '}' ) ;
    public final void rule__Section__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2719:1: ( ( '}' ) )
            // InternalPxManual.g:2720:1: ( '}' )
            {
            // InternalPxManual.g:2720:1: ( '}' )
            // InternalPxManual.g:2721:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSectionAccess().getRightCurlyBracketKeyword_5()); 
            }
            match(input,19,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSectionAccess().getRightCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__Group__5__Impl"


    // $ANTLR start "rule__Page__Group__0"
    // InternalPxManual.g:2731:1: rule__Page__Group__0 : rule__Page__Group__0__Impl rule__Page__Group__1 ;
    public final void rule__Page__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2735:1: ( rule__Page__Group__0__Impl rule__Page__Group__1 )
            // InternalPxManual.g:2736:2: rule__Page__Group__0__Impl rule__Page__Group__1
            {
            pushFollow(FOLLOW_26);
            rule__Page__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Page__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__0"


    // $ANTLR start "rule__Page__Group__0__Impl"
    // InternalPxManual.g:2743:1: rule__Page__Group__0__Impl : ( 'page' ) ;
    public final void rule__Page__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2747:1: ( ( 'page' ) )
            // InternalPxManual.g:2748:1: ( 'page' )
            {
            // InternalPxManual.g:2748:1: ( 'page' )
            // InternalPxManual.g:2749:2: 'page'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPageAccess().getPageKeyword_0()); 
            }
            match(input,35,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPageAccess().getPageKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__0__Impl"


    // $ANTLR start "rule__Page__Group__1"
    // InternalPxManual.g:2758:1: rule__Page__Group__1 : rule__Page__Group__1__Impl rule__Page__Group__2 ;
    public final void rule__Page__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2762:1: ( rule__Page__Group__1__Impl rule__Page__Group__2 )
            // InternalPxManual.g:2763:2: rule__Page__Group__1__Impl rule__Page__Group__2
            {
            pushFollow(FOLLOW_26);
            rule__Page__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Page__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__1"


    // $ANTLR start "rule__Page__Group__1__Impl"
    // InternalPxManual.g:2770:1: rule__Page__Group__1__Impl : ( ( rule__Page__NameAssignment_1 )? ) ;
    public final void rule__Page__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2774:1: ( ( ( rule__Page__NameAssignment_1 )? ) )
            // InternalPxManual.g:2775:1: ( ( rule__Page__NameAssignment_1 )? )
            {
            // InternalPxManual.g:2775:1: ( ( rule__Page__NameAssignment_1 )? )
            // InternalPxManual.g:2776:2: ( rule__Page__NameAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPageAccess().getNameAssignment_1()); 
            }
            // InternalPxManual.g:2777:2: ( rule__Page__NameAssignment_1 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_ID) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalPxManual.g:2777:3: rule__Page__NameAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Page__NameAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPageAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__1__Impl"


    // $ANTLR start "rule__Page__Group__2"
    // InternalPxManual.g:2785:1: rule__Page__Group__2 : rule__Page__Group__2__Impl rule__Page__Group__3 ;
    public final void rule__Page__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2789:1: ( rule__Page__Group__2__Impl rule__Page__Group__3 )
            // InternalPxManual.g:2790:2: rule__Page__Group__2__Impl rule__Page__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Page__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Page__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__2"


    // $ANTLR start "rule__Page__Group__2__Impl"
    // InternalPxManual.g:2797:1: rule__Page__Group__2__Impl : ( ( rule__Page__TitleAssignment_2 ) ) ;
    public final void rule__Page__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2801:1: ( ( ( rule__Page__TitleAssignment_2 ) ) )
            // InternalPxManual.g:2802:1: ( ( rule__Page__TitleAssignment_2 ) )
            {
            // InternalPxManual.g:2802:1: ( ( rule__Page__TitleAssignment_2 ) )
            // InternalPxManual.g:2803:2: ( rule__Page__TitleAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPageAccess().getTitleAssignment_2()); 
            }
            // InternalPxManual.g:2804:2: ( rule__Page__TitleAssignment_2 )
            // InternalPxManual.g:2804:3: rule__Page__TitleAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Page__TitleAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPageAccess().getTitleAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__2__Impl"


    // $ANTLR start "rule__Page__Group__3"
    // InternalPxManual.g:2812:1: rule__Page__Group__3 : rule__Page__Group__3__Impl rule__Page__Group__4 ;
    public final void rule__Page__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2816:1: ( rule__Page__Group__3__Impl rule__Page__Group__4 )
            // InternalPxManual.g:2817:2: rule__Page__Group__3__Impl rule__Page__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Page__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Page__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__3"


    // $ANTLR start "rule__Page__Group__3__Impl"
    // InternalPxManual.g:2824:1: rule__Page__Group__3__Impl : ( '{' ) ;
    public final void rule__Page__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2828:1: ( ( '{' ) )
            // InternalPxManual.g:2829:1: ( '{' )
            {
            // InternalPxManual.g:2829:1: ( '{' )
            // InternalPxManual.g:2830:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPageAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,18,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPageAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__3__Impl"


    // $ANTLR start "rule__Page__Group__4"
    // InternalPxManual.g:2839:1: rule__Page__Group__4 : rule__Page__Group__4__Impl rule__Page__Group__5 ;
    public final void rule__Page__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2843:1: ( rule__Page__Group__4__Impl rule__Page__Group__5 )
            // InternalPxManual.g:2844:2: rule__Page__Group__4__Impl rule__Page__Group__5
            {
            pushFollow(FOLLOW_8);
            rule__Page__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Page__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__4"


    // $ANTLR start "rule__Page__Group__4__Impl"
    // InternalPxManual.g:2851:1: rule__Page__Group__4__Impl : ( ( rule__Page__ElementsAssignment_4 )* ) ;
    public final void rule__Page__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2855:1: ( ( ( rule__Page__ElementsAssignment_4 )* ) )
            // InternalPxManual.g:2856:1: ( ( rule__Page__ElementsAssignment_4 )* )
            {
            // InternalPxManual.g:2856:1: ( ( rule__Page__ElementsAssignment_4 )* )
            // InternalPxManual.g:2857:2: ( rule__Page__ElementsAssignment_4 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPageAccess().getElementsAssignment_4()); 
            }
            // InternalPxManual.g:2858:2: ( rule__Page__ElementsAssignment_4 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==RULE_STRING||LA13_0==20||LA13_0==22||(LA13_0>=24 && LA13_0<=25)||(LA13_0>=33 && LA13_0<=36)||(LA13_0>=38 && LA13_0<=41)) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalPxManual.g:2858:3: rule__Page__ElementsAssignment_4
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Page__ElementsAssignment_4();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPageAccess().getElementsAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__4__Impl"


    // $ANTLR start "rule__Page__Group__5"
    // InternalPxManual.g:2866:1: rule__Page__Group__5 : rule__Page__Group__5__Impl ;
    public final void rule__Page__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2870:1: ( rule__Page__Group__5__Impl )
            // InternalPxManual.g:2871:2: rule__Page__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Page__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__5"


    // $ANTLR start "rule__Page__Group__5__Impl"
    // InternalPxManual.g:2877:1: rule__Page__Group__5__Impl : ( '}' ) ;
    public final void rule__Page__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2881:1: ( ( '}' ) )
            // InternalPxManual.g:2882:1: ( '}' )
            {
            // InternalPxManual.g:2882:1: ( '}' )
            // InternalPxManual.g:2883:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPageAccess().getRightCurlyBracketKeyword_5()); 
            }
            match(input,19,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPageAccess().getRightCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__Group__5__Impl"


    // $ANTLR start "rule__Example__Group__0"
    // InternalPxManual.g:2893:1: rule__Example__Group__0 : rule__Example__Group__0__Impl rule__Example__Group__1 ;
    public final void rule__Example__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2897:1: ( rule__Example__Group__0__Impl rule__Example__Group__1 )
            // InternalPxManual.g:2898:2: rule__Example__Group__0__Impl rule__Example__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__Example__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Example__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__Group__0"


    // $ANTLR start "rule__Example__Group__0__Impl"
    // InternalPxManual.g:2905:1: rule__Example__Group__0__Impl : ( 'example' ) ;
    public final void rule__Example__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2909:1: ( ( 'example' ) )
            // InternalPxManual.g:2910:1: ( 'example' )
            {
            // InternalPxManual.g:2910:1: ( 'example' )
            // InternalPxManual.g:2911:2: 'example'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExampleAccess().getExampleKeyword_0()); 
            }
            match(input,36,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExampleAccess().getExampleKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__Group__0__Impl"


    // $ANTLR start "rule__Example__Group__1"
    // InternalPxManual.g:2920:1: rule__Example__Group__1 : rule__Example__Group__1__Impl rule__Example__Group__2 ;
    public final void rule__Example__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2924:1: ( rule__Example__Group__1__Impl rule__Example__Group__2 )
            // InternalPxManual.g:2925:2: rule__Example__Group__1__Impl rule__Example__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Example__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Example__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__Group__1"


    // $ANTLR start "rule__Example__Group__1__Impl"
    // InternalPxManual.g:2932:1: rule__Example__Group__1__Impl : ( ( rule__Example__UnorderedGroup_1 ) ) ;
    public final void rule__Example__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2936:1: ( ( ( rule__Example__UnorderedGroup_1 ) ) )
            // InternalPxManual.g:2937:1: ( ( rule__Example__UnorderedGroup_1 ) )
            {
            // InternalPxManual.g:2937:1: ( ( rule__Example__UnorderedGroup_1 ) )
            // InternalPxManual.g:2938:2: ( rule__Example__UnorderedGroup_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExampleAccess().getUnorderedGroup_1()); 
            }
            // InternalPxManual.g:2939:2: ( rule__Example__UnorderedGroup_1 )
            // InternalPxManual.g:2939:3: rule__Example__UnorderedGroup_1
            {
            pushFollow(FOLLOW_2);
            rule__Example__UnorderedGroup_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExampleAccess().getUnorderedGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__Group__1__Impl"


    // $ANTLR start "rule__Example__Group__2"
    // InternalPxManual.g:2947:1: rule__Example__Group__2 : rule__Example__Group__2__Impl ;
    public final void rule__Example__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2951:1: ( rule__Example__Group__2__Impl )
            // InternalPxManual.g:2952:2: rule__Example__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Example__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__Group__2"


    // $ANTLR start "rule__Example__Group__2__Impl"
    // InternalPxManual.g:2958:1: rule__Example__Group__2__Impl : ( ( rule__Example__BodyAssignment_2 ) ) ;
    public final void rule__Example__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2962:1: ( ( ( rule__Example__BodyAssignment_2 ) ) )
            // InternalPxManual.g:2963:1: ( ( rule__Example__BodyAssignment_2 ) )
            {
            // InternalPxManual.g:2963:1: ( ( rule__Example__BodyAssignment_2 ) )
            // InternalPxManual.g:2964:2: ( rule__Example__BodyAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExampleAccess().getBodyAssignment_2()); 
            }
            // InternalPxManual.g:2965:2: ( rule__Example__BodyAssignment_2 )
            // InternalPxManual.g:2965:3: rule__Example__BodyAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Example__BodyAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExampleAccess().getBodyAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__Group__2__Impl"


    // $ANTLR start "rule__Example__Group_1_1__0"
    // InternalPxManual.g:2974:1: rule__Example__Group_1_1__0 : rule__Example__Group_1_1__0__Impl rule__Example__Group_1_1__1 ;
    public final void rule__Example__Group_1_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2978:1: ( rule__Example__Group_1_1__0__Impl rule__Example__Group_1_1__1 )
            // InternalPxManual.g:2979:2: rule__Example__Group_1_1__0__Impl rule__Example__Group_1_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Example__Group_1_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Example__Group_1_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__Group_1_1__0"


    // $ANTLR start "rule__Example__Group_1_1__0__Impl"
    // InternalPxManual.g:2986:1: rule__Example__Group_1_1__0__Impl : ( 'screenshot:' ) ;
    public final void rule__Example__Group_1_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:2990:1: ( ( 'screenshot:' ) )
            // InternalPxManual.g:2991:1: ( 'screenshot:' )
            {
            // InternalPxManual.g:2991:1: ( 'screenshot:' )
            // InternalPxManual.g:2992:2: 'screenshot:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExampleAccess().getScreenshotKeyword_1_1_0()); 
            }
            match(input,37,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExampleAccess().getScreenshotKeyword_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__Group_1_1__0__Impl"


    // $ANTLR start "rule__Example__Group_1_1__1"
    // InternalPxManual.g:3001:1: rule__Example__Group_1_1__1 : rule__Example__Group_1_1__1__Impl ;
    public final void rule__Example__Group_1_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3005:1: ( rule__Example__Group_1_1__1__Impl )
            // InternalPxManual.g:3006:2: rule__Example__Group_1_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Example__Group_1_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__Group_1_1__1"


    // $ANTLR start "rule__Example__Group_1_1__1__Impl"
    // InternalPxManual.g:3012:1: rule__Example__Group_1_1__1__Impl : ( ( rule__Example__ScreenshotAssignment_1_1_1 ) ) ;
    public final void rule__Example__Group_1_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3016:1: ( ( ( rule__Example__ScreenshotAssignment_1_1_1 ) ) )
            // InternalPxManual.g:3017:1: ( ( rule__Example__ScreenshotAssignment_1_1_1 ) )
            {
            // InternalPxManual.g:3017:1: ( ( rule__Example__ScreenshotAssignment_1_1_1 ) )
            // InternalPxManual.g:3018:2: ( rule__Example__ScreenshotAssignment_1_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExampleAccess().getScreenshotAssignment_1_1_1()); 
            }
            // InternalPxManual.g:3019:2: ( rule__Example__ScreenshotAssignment_1_1_1 )
            // InternalPxManual.g:3019:3: rule__Example__ScreenshotAssignment_1_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Example__ScreenshotAssignment_1_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExampleAccess().getScreenshotAssignment_1_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__Group_1_1__1__Impl"


    // $ANTLR start "rule__Usage__Group__0"
    // InternalPxManual.g:3028:1: rule__Usage__Group__0 : rule__Usage__Group__0__Impl rule__Usage__Group__1 ;
    public final void rule__Usage__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3032:1: ( rule__Usage__Group__0__Impl rule__Usage__Group__1 )
            // InternalPxManual.g:3033:2: rule__Usage__Group__0__Impl rule__Usage__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__Usage__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Usage__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__Group__0"


    // $ANTLR start "rule__Usage__Group__0__Impl"
    // InternalPxManual.g:3040:1: rule__Usage__Group__0__Impl : ( 'usage' ) ;
    public final void rule__Usage__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3044:1: ( ( 'usage' ) )
            // InternalPxManual.g:3045:1: ( 'usage' )
            {
            // InternalPxManual.g:3045:1: ( 'usage' )
            // InternalPxManual.g:3046:2: 'usage'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUsageAccess().getUsageKeyword_0()); 
            }
            match(input,38,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUsageAccess().getUsageKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__Group__0__Impl"


    // $ANTLR start "rule__Usage__Group__1"
    // InternalPxManual.g:3055:1: rule__Usage__Group__1 : rule__Usage__Group__1__Impl rule__Usage__Group__2 ;
    public final void rule__Usage__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3059:1: ( rule__Usage__Group__1__Impl rule__Usage__Group__2 )
            // InternalPxManual.g:3060:2: rule__Usage__Group__1__Impl rule__Usage__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Usage__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Usage__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__Group__1"


    // $ANTLR start "rule__Usage__Group__1__Impl"
    // InternalPxManual.g:3067:1: rule__Usage__Group__1__Impl : ( ( rule__Usage__UnorderedGroup_1 ) ) ;
    public final void rule__Usage__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3071:1: ( ( ( rule__Usage__UnorderedGroup_1 ) ) )
            // InternalPxManual.g:3072:1: ( ( rule__Usage__UnorderedGroup_1 ) )
            {
            // InternalPxManual.g:3072:1: ( ( rule__Usage__UnorderedGroup_1 ) )
            // InternalPxManual.g:3073:2: ( rule__Usage__UnorderedGroup_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUsageAccess().getUnorderedGroup_1()); 
            }
            // InternalPxManual.g:3074:2: ( rule__Usage__UnorderedGroup_1 )
            // InternalPxManual.g:3074:3: rule__Usage__UnorderedGroup_1
            {
            pushFollow(FOLLOW_2);
            rule__Usage__UnorderedGroup_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUsageAccess().getUnorderedGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__Group__1__Impl"


    // $ANTLR start "rule__Usage__Group__2"
    // InternalPxManual.g:3082:1: rule__Usage__Group__2 : rule__Usage__Group__2__Impl ;
    public final void rule__Usage__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3086:1: ( rule__Usage__Group__2__Impl )
            // InternalPxManual.g:3087:2: rule__Usage__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Usage__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__Group__2"


    // $ANTLR start "rule__Usage__Group__2__Impl"
    // InternalPxManual.g:3093:1: rule__Usage__Group__2__Impl : ( ( rule__Usage__BodyAssignment_2 ) ) ;
    public final void rule__Usage__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3097:1: ( ( ( rule__Usage__BodyAssignment_2 ) ) )
            // InternalPxManual.g:3098:1: ( ( rule__Usage__BodyAssignment_2 ) )
            {
            // InternalPxManual.g:3098:1: ( ( rule__Usage__BodyAssignment_2 ) )
            // InternalPxManual.g:3099:2: ( rule__Usage__BodyAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUsageAccess().getBodyAssignment_2()); 
            }
            // InternalPxManual.g:3100:2: ( rule__Usage__BodyAssignment_2 )
            // InternalPxManual.g:3100:3: rule__Usage__BodyAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Usage__BodyAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUsageAccess().getBodyAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__Group__2__Impl"


    // $ANTLR start "rule__Usage__Group_1_1__0"
    // InternalPxManual.g:3109:1: rule__Usage__Group_1_1__0 : rule__Usage__Group_1_1__0__Impl rule__Usage__Group_1_1__1 ;
    public final void rule__Usage__Group_1_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3113:1: ( rule__Usage__Group_1_1__0__Impl rule__Usage__Group_1_1__1 )
            // InternalPxManual.g:3114:2: rule__Usage__Group_1_1__0__Impl rule__Usage__Group_1_1__1
            {
            pushFollow(FOLLOW_6);
            rule__Usage__Group_1_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Usage__Group_1_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__Group_1_1__0"


    // $ANTLR start "rule__Usage__Group_1_1__0__Impl"
    // InternalPxManual.g:3121:1: rule__Usage__Group_1_1__0__Impl : ( 'screenshot:' ) ;
    public final void rule__Usage__Group_1_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3125:1: ( ( 'screenshot:' ) )
            // InternalPxManual.g:3126:1: ( 'screenshot:' )
            {
            // InternalPxManual.g:3126:1: ( 'screenshot:' )
            // InternalPxManual.g:3127:2: 'screenshot:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUsageAccess().getScreenshotKeyword_1_1_0()); 
            }
            match(input,37,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUsageAccess().getScreenshotKeyword_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__Group_1_1__0__Impl"


    // $ANTLR start "rule__Usage__Group_1_1__1"
    // InternalPxManual.g:3136:1: rule__Usage__Group_1_1__1 : rule__Usage__Group_1_1__1__Impl ;
    public final void rule__Usage__Group_1_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3140:1: ( rule__Usage__Group_1_1__1__Impl )
            // InternalPxManual.g:3141:2: rule__Usage__Group_1_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Usage__Group_1_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__Group_1_1__1"


    // $ANTLR start "rule__Usage__Group_1_1__1__Impl"
    // InternalPxManual.g:3147:1: rule__Usage__Group_1_1__1__Impl : ( ( rule__Usage__ScreenshotAssignment_1_1_1 ) ) ;
    public final void rule__Usage__Group_1_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3151:1: ( ( ( rule__Usage__ScreenshotAssignment_1_1_1 ) ) )
            // InternalPxManual.g:3152:1: ( ( rule__Usage__ScreenshotAssignment_1_1_1 ) )
            {
            // InternalPxManual.g:3152:1: ( ( rule__Usage__ScreenshotAssignment_1_1_1 ) )
            // InternalPxManual.g:3153:2: ( rule__Usage__ScreenshotAssignment_1_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUsageAccess().getScreenshotAssignment_1_1_1()); 
            }
            // InternalPxManual.g:3154:2: ( rule__Usage__ScreenshotAssignment_1_1_1 )
            // InternalPxManual.g:3154:3: rule__Usage__ScreenshotAssignment_1_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Usage__ScreenshotAssignment_1_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUsageAccess().getScreenshotAssignment_1_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__Group_1_1__1__Impl"


    // $ANTLR start "rule__Text__Group__0"
    // InternalPxManual.g:3163:1: rule__Text__Group__0 : rule__Text__Group__0__Impl rule__Text__Group__1 ;
    public final void rule__Text__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3167:1: ( rule__Text__Group__0__Impl rule__Text__Group__1 )
            // InternalPxManual.g:3168:2: rule__Text__Group__0__Impl rule__Text__Group__1
            {
            pushFollow(FOLLOW_28);
            rule__Text__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Text__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group__0"


    // $ANTLR start "rule__Text__Group__0__Impl"
    // InternalPxManual.g:3175:1: rule__Text__Group__0__Impl : ( ( rule__Text__Group_0__0 )? ) ;
    public final void rule__Text__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3179:1: ( ( ( rule__Text__Group_0__0 )? ) )
            // InternalPxManual.g:3180:1: ( ( rule__Text__Group_0__0 )? )
            {
            // InternalPxManual.g:3180:1: ( ( rule__Text__Group_0__0 )? )
            // InternalPxManual.g:3181:2: ( rule__Text__Group_0__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextAccess().getGroup_0()); 
            }
            // InternalPxManual.g:3182:2: ( rule__Text__Group_0__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==39) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalPxManual.g:3182:3: rule__Text__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Text__Group_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextAccess().getGroup_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group__0__Impl"


    // $ANTLR start "rule__Text__Group__1"
    // InternalPxManual.g:3190:1: rule__Text__Group__1 : rule__Text__Group__1__Impl rule__Text__Group__2 ;
    public final void rule__Text__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3194:1: ( rule__Text__Group__1__Impl rule__Text__Group__2 )
            // InternalPxManual.g:3195:2: rule__Text__Group__1__Impl rule__Text__Group__2
            {
            pushFollow(FOLLOW_28);
            rule__Text__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Text__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group__1"


    // $ANTLR start "rule__Text__Group__1__Impl"
    // InternalPxManual.g:3202:1: rule__Text__Group__1__Impl : ( ( rule__Text__Group_1__0 )? ) ;
    public final void rule__Text__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3206:1: ( ( ( rule__Text__Group_1__0 )? ) )
            // InternalPxManual.g:3207:1: ( ( rule__Text__Group_1__0 )? )
            {
            // InternalPxManual.g:3207:1: ( ( rule__Text__Group_1__0 )? )
            // InternalPxManual.g:3208:2: ( rule__Text__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextAccess().getGroup_1()); 
            }
            // InternalPxManual.g:3209:2: ( rule__Text__Group_1__0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==40) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalPxManual.g:3209:3: rule__Text__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Text__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group__1__Impl"


    // $ANTLR start "rule__Text__Group__2"
    // InternalPxManual.g:3217:1: rule__Text__Group__2 : rule__Text__Group__2__Impl ;
    public final void rule__Text__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3221:1: ( rule__Text__Group__2__Impl )
            // InternalPxManual.g:3222:2: rule__Text__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Text__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group__2"


    // $ANTLR start "rule__Text__Group__2__Impl"
    // InternalPxManual.g:3228:1: rule__Text__Group__2__Impl : ( ( rule__Text__BodyAssignment_2 ) ) ;
    public final void rule__Text__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3232:1: ( ( ( rule__Text__BodyAssignment_2 ) ) )
            // InternalPxManual.g:3233:1: ( ( rule__Text__BodyAssignment_2 ) )
            {
            // InternalPxManual.g:3233:1: ( ( rule__Text__BodyAssignment_2 ) )
            // InternalPxManual.g:3234:2: ( rule__Text__BodyAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextAccess().getBodyAssignment_2()); 
            }
            // InternalPxManual.g:3235:2: ( rule__Text__BodyAssignment_2 )
            // InternalPxManual.g:3235:3: rule__Text__BodyAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Text__BodyAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextAccess().getBodyAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group__2__Impl"


    // $ANTLR start "rule__Text__Group_0__0"
    // InternalPxManual.g:3244:1: rule__Text__Group_0__0 : rule__Text__Group_0__0__Impl rule__Text__Group_0__1 ;
    public final void rule__Text__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3248:1: ( rule__Text__Group_0__0__Impl rule__Text__Group_0__1 )
            // InternalPxManual.g:3249:2: rule__Text__Group_0__0__Impl rule__Text__Group_0__1
            {
            pushFollow(FOLLOW_29);
            rule__Text__Group_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Text__Group_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group_0__0"


    // $ANTLR start "rule__Text__Group_0__0__Impl"
    // InternalPxManual.g:3256:1: rule__Text__Group_0__0__Impl : ( '@' ) ;
    public final void rule__Text__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3260:1: ( ( '@' ) )
            // InternalPxManual.g:3261:1: ( '@' )
            {
            // InternalPxManual.g:3261:1: ( '@' )
            // InternalPxManual.g:3262:2: '@'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextAccess().getCommercialAtKeyword_0_0()); 
            }
            match(input,39,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextAccess().getCommercialAtKeyword_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group_0__0__Impl"


    // $ANTLR start "rule__Text__Group_0__1"
    // InternalPxManual.g:3271:1: rule__Text__Group_0__1 : rule__Text__Group_0__1__Impl ;
    public final void rule__Text__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3275:1: ( rule__Text__Group_0__1__Impl )
            // InternalPxManual.g:3276:2: rule__Text__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Text__Group_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group_0__1"


    // $ANTLR start "rule__Text__Group_0__1__Impl"
    // InternalPxManual.g:3282:1: rule__Text__Group_0__1__Impl : ( ( rule__Text__LanguageAssignment_0_1 ) ) ;
    public final void rule__Text__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3286:1: ( ( ( rule__Text__LanguageAssignment_0_1 ) ) )
            // InternalPxManual.g:3287:1: ( ( rule__Text__LanguageAssignment_0_1 ) )
            {
            // InternalPxManual.g:3287:1: ( ( rule__Text__LanguageAssignment_0_1 ) )
            // InternalPxManual.g:3288:2: ( rule__Text__LanguageAssignment_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextAccess().getLanguageAssignment_0_1()); 
            }
            // InternalPxManual.g:3289:2: ( rule__Text__LanguageAssignment_0_1 )
            // InternalPxManual.g:3289:3: rule__Text__LanguageAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Text__LanguageAssignment_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextAccess().getLanguageAssignment_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group_0__1__Impl"


    // $ANTLR start "rule__Text__Group_1__0"
    // InternalPxManual.g:3298:1: rule__Text__Group_1__0 : rule__Text__Group_1__0__Impl rule__Text__Group_1__1 ;
    public final void rule__Text__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3302:1: ( rule__Text__Group_1__0__Impl rule__Text__Group_1__1 )
            // InternalPxManual.g:3303:2: rule__Text__Group_1__0__Impl rule__Text__Group_1__1
            {
            pushFollow(FOLLOW_29);
            rule__Text__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Text__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group_1__0"


    // $ANTLR start "rule__Text__Group_1__0__Impl"
    // InternalPxManual.g:3310:1: rule__Text__Group_1__0__Impl : ( 'class:' ) ;
    public final void rule__Text__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3314:1: ( ( 'class:' ) )
            // InternalPxManual.g:3315:1: ( 'class:' )
            {
            // InternalPxManual.g:3315:1: ( 'class:' )
            // InternalPxManual.g:3316:2: 'class:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextAccess().getClassKeyword_1_0()); 
            }
            match(input,40,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextAccess().getClassKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group_1__0__Impl"


    // $ANTLR start "rule__Text__Group_1__1"
    // InternalPxManual.g:3325:1: rule__Text__Group_1__1 : rule__Text__Group_1__1__Impl ;
    public final void rule__Text__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3329:1: ( rule__Text__Group_1__1__Impl )
            // InternalPxManual.g:3330:2: rule__Text__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Text__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group_1__1"


    // $ANTLR start "rule__Text__Group_1__1__Impl"
    // InternalPxManual.g:3336:1: rule__Text__Group_1__1__Impl : ( ( rule__Text__ClassNameAssignment_1_1 ) ) ;
    public final void rule__Text__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3340:1: ( ( ( rule__Text__ClassNameAssignment_1_1 ) ) )
            // InternalPxManual.g:3341:1: ( ( rule__Text__ClassNameAssignment_1_1 ) )
            {
            // InternalPxManual.g:3341:1: ( ( rule__Text__ClassNameAssignment_1_1 ) )
            // InternalPxManual.g:3342:2: ( rule__Text__ClassNameAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextAccess().getClassNameAssignment_1_1()); 
            }
            // InternalPxManual.g:3343:2: ( rule__Text__ClassNameAssignment_1_1 )
            // InternalPxManual.g:3343:3: rule__Text__ClassNameAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Text__ClassNameAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextAccess().getClassNameAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__Group_1__1__Impl"


    // $ANTLR start "rule__Keyword__Group__0"
    // InternalPxManual.g:3352:1: rule__Keyword__Group__0 : rule__Keyword__Group__0__Impl rule__Keyword__Group__1 ;
    public final void rule__Keyword__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3356:1: ( rule__Keyword__Group__0__Impl rule__Keyword__Group__1 )
            // InternalPxManual.g:3357:2: rule__Keyword__Group__0__Impl rule__Keyword__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__Keyword__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Keyword__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__0"


    // $ANTLR start "rule__Keyword__Group__0__Impl"
    // InternalPxManual.g:3364:1: rule__Keyword__Group__0__Impl : ( 'keyword' ) ;
    public final void rule__Keyword__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3368:1: ( ( 'keyword' ) )
            // InternalPxManual.g:3369:1: ( 'keyword' )
            {
            // InternalPxManual.g:3369:1: ( 'keyword' )
            // InternalPxManual.g:3370:2: 'keyword'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getKeywordKeyword_0()); 
            }
            match(input,41,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getKeywordKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__0__Impl"


    // $ANTLR start "rule__Keyword__Group__1"
    // InternalPxManual.g:3379:1: rule__Keyword__Group__1 : rule__Keyword__Group__1__Impl rule__Keyword__Group__2 ;
    public final void rule__Keyword__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3383:1: ( rule__Keyword__Group__1__Impl rule__Keyword__Group__2 )
            // InternalPxManual.g:3384:2: rule__Keyword__Group__1__Impl rule__Keyword__Group__2
            {
            pushFollow(FOLLOW_24);
            rule__Keyword__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Keyword__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__1"


    // $ANTLR start "rule__Keyword__Group__1__Impl"
    // InternalPxManual.g:3391:1: rule__Keyword__Group__1__Impl : ( ( rule__Keyword__NameAssignment_1 )? ) ;
    public final void rule__Keyword__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3395:1: ( ( ( rule__Keyword__NameAssignment_1 )? ) )
            // InternalPxManual.g:3396:1: ( ( rule__Keyword__NameAssignment_1 )? )
            {
            // InternalPxManual.g:3396:1: ( ( rule__Keyword__NameAssignment_1 )? )
            // InternalPxManual.g:3397:2: ( rule__Keyword__NameAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getNameAssignment_1()); 
            }
            // InternalPxManual.g:3398:2: ( rule__Keyword__NameAssignment_1 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_ID) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalPxManual.g:3398:3: rule__Keyword__NameAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Keyword__NameAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__1__Impl"


    // $ANTLR start "rule__Keyword__Group__2"
    // InternalPxManual.g:3406:1: rule__Keyword__Group__2 : rule__Keyword__Group__2__Impl rule__Keyword__Group__3 ;
    public final void rule__Keyword__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3410:1: ( rule__Keyword__Group__2__Impl rule__Keyword__Group__3 )
            // InternalPxManual.g:3411:2: rule__Keyword__Group__2__Impl rule__Keyword__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Keyword__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Keyword__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__2"


    // $ANTLR start "rule__Keyword__Group__2__Impl"
    // InternalPxManual.g:3418:1: rule__Keyword__Group__2__Impl : ( ( rule__Keyword__KeywordNameAssignment_2 ) ) ;
    public final void rule__Keyword__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3422:1: ( ( ( rule__Keyword__KeywordNameAssignment_2 ) ) )
            // InternalPxManual.g:3423:1: ( ( rule__Keyword__KeywordNameAssignment_2 ) )
            {
            // InternalPxManual.g:3423:1: ( ( rule__Keyword__KeywordNameAssignment_2 ) )
            // InternalPxManual.g:3424:2: ( rule__Keyword__KeywordNameAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getKeywordNameAssignment_2()); 
            }
            // InternalPxManual.g:3425:2: ( rule__Keyword__KeywordNameAssignment_2 )
            // InternalPxManual.g:3425:3: rule__Keyword__KeywordNameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Keyword__KeywordNameAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getKeywordNameAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__2__Impl"


    // $ANTLR start "rule__Keyword__Group__3"
    // InternalPxManual.g:3433:1: rule__Keyword__Group__3 : rule__Keyword__Group__3__Impl rule__Keyword__Group__4 ;
    public final void rule__Keyword__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3437:1: ( rule__Keyword__Group__3__Impl rule__Keyword__Group__4 )
            // InternalPxManual.g:3438:2: rule__Keyword__Group__3__Impl rule__Keyword__Group__4
            {
            pushFollow(FOLLOW_30);
            rule__Keyword__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Keyword__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__3"


    // $ANTLR start "rule__Keyword__Group__3__Impl"
    // InternalPxManual.g:3445:1: rule__Keyword__Group__3__Impl : ( '{' ) ;
    public final void rule__Keyword__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3449:1: ( ( '{' ) )
            // InternalPxManual.g:3450:1: ( '{' )
            {
            // InternalPxManual.g:3450:1: ( '{' )
            // InternalPxManual.g:3451:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,18,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__3__Impl"


    // $ANTLR start "rule__Keyword__Group__4"
    // InternalPxManual.g:3460:1: rule__Keyword__Group__4 : rule__Keyword__Group__4__Impl rule__Keyword__Group__5 ;
    public final void rule__Keyword__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3464:1: ( rule__Keyword__Group__4__Impl rule__Keyword__Group__5 )
            // InternalPxManual.g:3465:2: rule__Keyword__Group__4__Impl rule__Keyword__Group__5
            {
            pushFollow(FOLLOW_31);
            rule__Keyword__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Keyword__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__4"


    // $ANTLR start "rule__Keyword__Group__4__Impl"
    // InternalPxManual.g:3472:1: rule__Keyword__Group__4__Impl : ( ( rule__Keyword__UnorderedGroup_4 ) ) ;
    public final void rule__Keyword__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3476:1: ( ( ( rule__Keyword__UnorderedGroup_4 ) ) )
            // InternalPxManual.g:3477:1: ( ( rule__Keyword__UnorderedGroup_4 ) )
            {
            // InternalPxManual.g:3477:1: ( ( rule__Keyword__UnorderedGroup_4 ) )
            // InternalPxManual.g:3478:2: ( rule__Keyword__UnorderedGroup_4 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getUnorderedGroup_4()); 
            }
            // InternalPxManual.g:3479:2: ( rule__Keyword__UnorderedGroup_4 )
            // InternalPxManual.g:3479:3: rule__Keyword__UnorderedGroup_4
            {
            pushFollow(FOLLOW_2);
            rule__Keyword__UnorderedGroup_4();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getUnorderedGroup_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__4__Impl"


    // $ANTLR start "rule__Keyword__Group__5"
    // InternalPxManual.g:3487:1: rule__Keyword__Group__5 : rule__Keyword__Group__5__Impl ;
    public final void rule__Keyword__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3491:1: ( rule__Keyword__Group__5__Impl )
            // InternalPxManual.g:3492:2: rule__Keyword__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Keyword__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__5"


    // $ANTLR start "rule__Keyword__Group__5__Impl"
    // InternalPxManual.g:3498:1: rule__Keyword__Group__5__Impl : ( '}' ) ;
    public final void rule__Keyword__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3502:1: ( ( '}' ) )
            // InternalPxManual.g:3503:1: ( '}' )
            {
            // InternalPxManual.g:3503:1: ( '}' )
            // InternalPxManual.g:3504:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getRightCurlyBracketKeyword_5()); 
            }
            match(input,19,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getRightCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group__5__Impl"


    // $ANTLR start "rule__Keyword__Group_4_0__0"
    // InternalPxManual.g:3514:1: rule__Keyword__Group_4_0__0 : rule__Keyword__Group_4_0__0__Impl rule__Keyword__Group_4_0__1 ;
    public final void rule__Keyword__Group_4_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3518:1: ( rule__Keyword__Group_4_0__0__Impl rule__Keyword__Group_4_0__1 )
            // InternalPxManual.g:3519:2: rule__Keyword__Group_4_0__0__Impl rule__Keyword__Group_4_0__1
            {
            pushFollow(FOLLOW_5);
            rule__Keyword__Group_4_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Keyword__Group_4_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_0__0"


    // $ANTLR start "rule__Keyword__Group_4_0__0__Impl"
    // InternalPxManual.g:3526:1: rule__Keyword__Group_4_0__0__Impl : ( 'doc:' ) ;
    public final void rule__Keyword__Group_4_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3530:1: ( ( 'doc:' ) )
            // InternalPxManual.g:3531:1: ( 'doc:' )
            {
            // InternalPxManual.g:3531:1: ( 'doc:' )
            // InternalPxManual.g:3532:2: 'doc:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getDocKeyword_4_0_0()); 
            }
            match(input,42,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getDocKeyword_4_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_0__0__Impl"


    // $ANTLR start "rule__Keyword__Group_4_0__1"
    // InternalPxManual.g:3541:1: rule__Keyword__Group_4_0__1 : rule__Keyword__Group_4_0__1__Impl ;
    public final void rule__Keyword__Group_4_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3545:1: ( rule__Keyword__Group_4_0__1__Impl )
            // InternalPxManual.g:3546:2: rule__Keyword__Group_4_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Keyword__Group_4_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_0__1"


    // $ANTLR start "rule__Keyword__Group_4_0__1__Impl"
    // InternalPxManual.g:3552:1: rule__Keyword__Group_4_0__1__Impl : ( ( rule__Keyword__DocumentationAssignment_4_0_1 ) ) ;
    public final void rule__Keyword__Group_4_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3556:1: ( ( ( rule__Keyword__DocumentationAssignment_4_0_1 ) ) )
            // InternalPxManual.g:3557:1: ( ( rule__Keyword__DocumentationAssignment_4_0_1 ) )
            {
            // InternalPxManual.g:3557:1: ( ( rule__Keyword__DocumentationAssignment_4_0_1 ) )
            // InternalPxManual.g:3558:2: ( rule__Keyword__DocumentationAssignment_4_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getDocumentationAssignment_4_0_1()); 
            }
            // InternalPxManual.g:3559:2: ( rule__Keyword__DocumentationAssignment_4_0_1 )
            // InternalPxManual.g:3559:3: rule__Keyword__DocumentationAssignment_4_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Keyword__DocumentationAssignment_4_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getDocumentationAssignment_4_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_0__1__Impl"


    // $ANTLR start "rule__Keyword__Group_4_1__0"
    // InternalPxManual.g:3568:1: rule__Keyword__Group_4_1__0 : rule__Keyword__Group_4_1__0__Impl rule__Keyword__Group_4_1__1 ;
    public final void rule__Keyword__Group_4_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3572:1: ( rule__Keyword__Group_4_1__0__Impl rule__Keyword__Group_4_1__1 )
            // InternalPxManual.g:3573:2: rule__Keyword__Group_4_1__0__Impl rule__Keyword__Group_4_1__1
            {
            pushFollow(FOLLOW_28);
            rule__Keyword__Group_4_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Keyword__Group_4_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_1__0"


    // $ANTLR start "rule__Keyword__Group_4_1__0__Impl"
    // InternalPxManual.g:3580:1: rule__Keyword__Group_4_1__0__Impl : ( 'prettyTitle:' ) ;
    public final void rule__Keyword__Group_4_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3584:1: ( ( 'prettyTitle:' ) )
            // InternalPxManual.g:3585:1: ( 'prettyTitle:' )
            {
            // InternalPxManual.g:3585:1: ( 'prettyTitle:' )
            // InternalPxManual.g:3586:2: 'prettyTitle:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getPrettyTitleKeyword_4_1_0()); 
            }
            match(input,43,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getPrettyTitleKeyword_4_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_1__0__Impl"


    // $ANTLR start "rule__Keyword__Group_4_1__1"
    // InternalPxManual.g:3595:1: rule__Keyword__Group_4_1__1 : rule__Keyword__Group_4_1__1__Impl ;
    public final void rule__Keyword__Group_4_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3599:1: ( rule__Keyword__Group_4_1__1__Impl )
            // InternalPxManual.g:3600:2: rule__Keyword__Group_4_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Keyword__Group_4_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_1__1"


    // $ANTLR start "rule__Keyword__Group_4_1__1__Impl"
    // InternalPxManual.g:3606:1: rule__Keyword__Group_4_1__1__Impl : ( ( rule__Keyword__PrettyTitleAssignment_4_1_1 ) ) ;
    public final void rule__Keyword__Group_4_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3610:1: ( ( ( rule__Keyword__PrettyTitleAssignment_4_1_1 ) ) )
            // InternalPxManual.g:3611:1: ( ( rule__Keyword__PrettyTitleAssignment_4_1_1 ) )
            {
            // InternalPxManual.g:3611:1: ( ( rule__Keyword__PrettyTitleAssignment_4_1_1 ) )
            // InternalPxManual.g:3612:2: ( rule__Keyword__PrettyTitleAssignment_4_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getPrettyTitleAssignment_4_1_1()); 
            }
            // InternalPxManual.g:3613:2: ( rule__Keyword__PrettyTitleAssignment_4_1_1 )
            // InternalPxManual.g:3613:3: rule__Keyword__PrettyTitleAssignment_4_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Keyword__PrettyTitleAssignment_4_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getPrettyTitleAssignment_4_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_1__1__Impl"


    // $ANTLR start "rule__Keyword__Group_4_4__0"
    // InternalPxManual.g:3622:1: rule__Keyword__Group_4_4__0 : rule__Keyword__Group_4_4__0__Impl rule__Keyword__Group_4_4__1 ;
    public final void rule__Keyword__Group_4_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3626:1: ( rule__Keyword__Group_4_4__0__Impl rule__Keyword__Group_4_4__1 )
            // InternalPxManual.g:3627:2: rule__Keyword__Group_4_4__0__Impl rule__Keyword__Group_4_4__1
            {
            pushFollow(FOLLOW_5);
            rule__Keyword__Group_4_4__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Keyword__Group_4_4__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_4__0"


    // $ANTLR start "rule__Keyword__Group_4_4__0__Impl"
    // InternalPxManual.g:3634:1: rule__Keyword__Group_4_4__0__Impl : ( 'conclusion:' ) ;
    public final void rule__Keyword__Group_4_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3638:1: ( ( 'conclusion:' ) )
            // InternalPxManual.g:3639:1: ( 'conclusion:' )
            {
            // InternalPxManual.g:3639:1: ( 'conclusion:' )
            // InternalPxManual.g:3640:2: 'conclusion:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getConclusionKeyword_4_4_0()); 
            }
            match(input,44,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getConclusionKeyword_4_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_4__0__Impl"


    // $ANTLR start "rule__Keyword__Group_4_4__1"
    // InternalPxManual.g:3649:1: rule__Keyword__Group_4_4__1 : rule__Keyword__Group_4_4__1__Impl ;
    public final void rule__Keyword__Group_4_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3653:1: ( rule__Keyword__Group_4_4__1__Impl )
            // InternalPxManual.g:3654:2: rule__Keyword__Group_4_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Keyword__Group_4_4__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_4__1"


    // $ANTLR start "rule__Keyword__Group_4_4__1__Impl"
    // InternalPxManual.g:3660:1: rule__Keyword__Group_4_4__1__Impl : ( ( rule__Keyword__ConclusionAssignment_4_4_1 ) ) ;
    public final void rule__Keyword__Group_4_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3664:1: ( ( ( rule__Keyword__ConclusionAssignment_4_4_1 ) ) )
            // InternalPxManual.g:3665:1: ( ( rule__Keyword__ConclusionAssignment_4_4_1 ) )
            {
            // InternalPxManual.g:3665:1: ( ( rule__Keyword__ConclusionAssignment_4_4_1 ) )
            // InternalPxManual.g:3666:2: ( rule__Keyword__ConclusionAssignment_4_4_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getConclusionAssignment_4_4_1()); 
            }
            // InternalPxManual.g:3667:2: ( rule__Keyword__ConclusionAssignment_4_4_1 )
            // InternalPxManual.g:3667:3: rule__Keyword__ConclusionAssignment_4_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Keyword__ConclusionAssignment_4_4_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getConclusionAssignment_4_4_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__Group_4_4__1__Impl"


    // $ANTLR start "rule__KeywordAttribute__Group__0"
    // InternalPxManual.g:3676:1: rule__KeywordAttribute__Group__0 : rule__KeywordAttribute__Group__0__Impl rule__KeywordAttribute__Group__1 ;
    public final void rule__KeywordAttribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3680:1: ( rule__KeywordAttribute__Group__0__Impl rule__KeywordAttribute__Group__1 )
            // InternalPxManual.g:3681:2: rule__KeywordAttribute__Group__0__Impl rule__KeywordAttribute__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__KeywordAttribute__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group__0"


    // $ANTLR start "rule__KeywordAttribute__Group__0__Impl"
    // InternalPxManual.g:3688:1: rule__KeywordAttribute__Group__0__Impl : ( 'attribute' ) ;
    public final void rule__KeywordAttribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3692:1: ( ( 'attribute' ) )
            // InternalPxManual.g:3693:1: ( 'attribute' )
            {
            // InternalPxManual.g:3693:1: ( 'attribute' )
            // InternalPxManual.g:3694:2: 'attribute'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getAttributeKeyword_0()); 
            }
            match(input,45,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getAttributeKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group__0__Impl"


    // $ANTLR start "rule__KeywordAttribute__Group__1"
    // InternalPxManual.g:3703:1: rule__KeywordAttribute__Group__1 : rule__KeywordAttribute__Group__1__Impl rule__KeywordAttribute__Group__2 ;
    public final void rule__KeywordAttribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3707:1: ( rule__KeywordAttribute__Group__1__Impl rule__KeywordAttribute__Group__2 )
            // InternalPxManual.g:3708:2: rule__KeywordAttribute__Group__1__Impl rule__KeywordAttribute__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__KeywordAttribute__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group__1"


    // $ANTLR start "rule__KeywordAttribute__Group__1__Impl"
    // InternalPxManual.g:3715:1: rule__KeywordAttribute__Group__1__Impl : ( ( rule__KeywordAttribute__AttNameAssignment_1 ) ) ;
    public final void rule__KeywordAttribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3719:1: ( ( ( rule__KeywordAttribute__AttNameAssignment_1 ) ) )
            // InternalPxManual.g:3720:1: ( ( rule__KeywordAttribute__AttNameAssignment_1 ) )
            {
            // InternalPxManual.g:3720:1: ( ( rule__KeywordAttribute__AttNameAssignment_1 ) )
            // InternalPxManual.g:3721:2: ( rule__KeywordAttribute__AttNameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getAttNameAssignment_1()); 
            }
            // InternalPxManual.g:3722:2: ( rule__KeywordAttribute__AttNameAssignment_1 )
            // InternalPxManual.g:3722:3: rule__KeywordAttribute__AttNameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__AttNameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getAttNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group__1__Impl"


    // $ANTLR start "rule__KeywordAttribute__Group__2"
    // InternalPxManual.g:3730:1: rule__KeywordAttribute__Group__2 : rule__KeywordAttribute__Group__2__Impl rule__KeywordAttribute__Group__3 ;
    public final void rule__KeywordAttribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3734:1: ( rule__KeywordAttribute__Group__2__Impl rule__KeywordAttribute__Group__3 )
            // InternalPxManual.g:3735:2: rule__KeywordAttribute__Group__2__Impl rule__KeywordAttribute__Group__3
            {
            pushFollow(FOLLOW_32);
            rule__KeywordAttribute__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group__2"


    // $ANTLR start "rule__KeywordAttribute__Group__2__Impl"
    // InternalPxManual.g:3742:1: rule__KeywordAttribute__Group__2__Impl : ( '{' ) ;
    public final void rule__KeywordAttribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3746:1: ( ( '{' ) )
            // InternalPxManual.g:3747:1: ( '{' )
            {
            // InternalPxManual.g:3747:1: ( '{' )
            // InternalPxManual.g:3748:2: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getLeftCurlyBracketKeyword_2()); 
            }
            match(input,18,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getLeftCurlyBracketKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group__2__Impl"


    // $ANTLR start "rule__KeywordAttribute__Group__3"
    // InternalPxManual.g:3757:1: rule__KeywordAttribute__Group__3 : rule__KeywordAttribute__Group__3__Impl rule__KeywordAttribute__Group__4 ;
    public final void rule__KeywordAttribute__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3761:1: ( rule__KeywordAttribute__Group__3__Impl rule__KeywordAttribute__Group__4 )
            // InternalPxManual.g:3762:2: rule__KeywordAttribute__Group__3__Impl rule__KeywordAttribute__Group__4
            {
            pushFollow(FOLLOW_31);
            rule__KeywordAttribute__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group__3"


    // $ANTLR start "rule__KeywordAttribute__Group__3__Impl"
    // InternalPxManual.g:3769:1: rule__KeywordAttribute__Group__3__Impl : ( ( rule__KeywordAttribute__UnorderedGroup_3 ) ) ;
    public final void rule__KeywordAttribute__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3773:1: ( ( ( rule__KeywordAttribute__UnorderedGroup_3 ) ) )
            // InternalPxManual.g:3774:1: ( ( rule__KeywordAttribute__UnorderedGroup_3 ) )
            {
            // InternalPxManual.g:3774:1: ( ( rule__KeywordAttribute__UnorderedGroup_3 ) )
            // InternalPxManual.g:3775:2: ( rule__KeywordAttribute__UnorderedGroup_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3()); 
            }
            // InternalPxManual.g:3776:2: ( rule__KeywordAttribute__UnorderedGroup_3 )
            // InternalPxManual.g:3776:3: rule__KeywordAttribute__UnorderedGroup_3
            {
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__UnorderedGroup_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group__3__Impl"


    // $ANTLR start "rule__KeywordAttribute__Group__4"
    // InternalPxManual.g:3784:1: rule__KeywordAttribute__Group__4 : rule__KeywordAttribute__Group__4__Impl ;
    public final void rule__KeywordAttribute__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3788:1: ( rule__KeywordAttribute__Group__4__Impl )
            // InternalPxManual.g:3789:2: rule__KeywordAttribute__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group__4"


    // $ANTLR start "rule__KeywordAttribute__Group__4__Impl"
    // InternalPxManual.g:3795:1: rule__KeywordAttribute__Group__4__Impl : ( '}' ) ;
    public final void rule__KeywordAttribute__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3799:1: ( ( '}' ) )
            // InternalPxManual.g:3800:1: ( '}' )
            {
            // InternalPxManual.g:3800:1: ( '}' )
            // InternalPxManual.g:3801:2: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getRightCurlyBracketKeyword_4()); 
            }
            match(input,19,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getRightCurlyBracketKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group__4__Impl"


    // $ANTLR start "rule__KeywordAttribute__Group_3_0__0"
    // InternalPxManual.g:3811:1: rule__KeywordAttribute__Group_3_0__0 : rule__KeywordAttribute__Group_3_0__0__Impl rule__KeywordAttribute__Group_3_0__1 ;
    public final void rule__KeywordAttribute__Group_3_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3815:1: ( rule__KeywordAttribute__Group_3_0__0__Impl rule__KeywordAttribute__Group_3_0__1 )
            // InternalPxManual.g:3816:2: rule__KeywordAttribute__Group_3_0__0__Impl rule__KeywordAttribute__Group_3_0__1
            {
            pushFollow(FOLLOW_5);
            rule__KeywordAttribute__Group_3_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__Group_3_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group_3_0__0"


    // $ANTLR start "rule__KeywordAttribute__Group_3_0__0__Impl"
    // InternalPxManual.g:3823:1: rule__KeywordAttribute__Group_3_0__0__Impl : ( 'doc:' ) ;
    public final void rule__KeywordAttribute__Group_3_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3827:1: ( ( 'doc:' ) )
            // InternalPxManual.g:3828:1: ( 'doc:' )
            {
            // InternalPxManual.g:3828:1: ( 'doc:' )
            // InternalPxManual.g:3829:2: 'doc:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getDocKeyword_3_0_0()); 
            }
            match(input,42,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getDocKeyword_3_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group_3_0__0__Impl"


    // $ANTLR start "rule__KeywordAttribute__Group_3_0__1"
    // InternalPxManual.g:3838:1: rule__KeywordAttribute__Group_3_0__1 : rule__KeywordAttribute__Group_3_0__1__Impl ;
    public final void rule__KeywordAttribute__Group_3_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3842:1: ( rule__KeywordAttribute__Group_3_0__1__Impl )
            // InternalPxManual.g:3843:2: rule__KeywordAttribute__Group_3_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__Group_3_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group_3_0__1"


    // $ANTLR start "rule__KeywordAttribute__Group_3_0__1__Impl"
    // InternalPxManual.g:3849:1: rule__KeywordAttribute__Group_3_0__1__Impl : ( ( rule__KeywordAttribute__DocumentationAssignment_3_0_1 ) ) ;
    public final void rule__KeywordAttribute__Group_3_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3853:1: ( ( ( rule__KeywordAttribute__DocumentationAssignment_3_0_1 ) ) )
            // InternalPxManual.g:3854:1: ( ( rule__KeywordAttribute__DocumentationAssignment_3_0_1 ) )
            {
            // InternalPxManual.g:3854:1: ( ( rule__KeywordAttribute__DocumentationAssignment_3_0_1 ) )
            // InternalPxManual.g:3855:2: ( rule__KeywordAttribute__DocumentationAssignment_3_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getDocumentationAssignment_3_0_1()); 
            }
            // InternalPxManual.g:3856:2: ( rule__KeywordAttribute__DocumentationAssignment_3_0_1 )
            // InternalPxManual.g:3856:3: rule__KeywordAttribute__DocumentationAssignment_3_0_1
            {
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__DocumentationAssignment_3_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getDocumentationAssignment_3_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group_3_0__1__Impl"


    // $ANTLR start "rule__KeywordAttribute__Group_3_3__0"
    // InternalPxManual.g:3865:1: rule__KeywordAttribute__Group_3_3__0 : rule__KeywordAttribute__Group_3_3__0__Impl rule__KeywordAttribute__Group_3_3__1 ;
    public final void rule__KeywordAttribute__Group_3_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3869:1: ( rule__KeywordAttribute__Group_3_3__0__Impl rule__KeywordAttribute__Group_3_3__1 )
            // InternalPxManual.g:3870:2: rule__KeywordAttribute__Group_3_3__0__Impl rule__KeywordAttribute__Group_3_3__1
            {
            pushFollow(FOLLOW_5);
            rule__KeywordAttribute__Group_3_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__Group_3_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group_3_3__0"


    // $ANTLR start "rule__KeywordAttribute__Group_3_3__0__Impl"
    // InternalPxManual.g:3877:1: rule__KeywordAttribute__Group_3_3__0__Impl : ( 'possibleValues:' ) ;
    public final void rule__KeywordAttribute__Group_3_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3881:1: ( ( 'possibleValues:' ) )
            // InternalPxManual.g:3882:1: ( 'possibleValues:' )
            {
            // InternalPxManual.g:3882:1: ( 'possibleValues:' )
            // InternalPxManual.g:3883:2: 'possibleValues:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getPossibleValuesKeyword_3_3_0()); 
            }
            match(input,46,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getPossibleValuesKeyword_3_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group_3_3__0__Impl"


    // $ANTLR start "rule__KeywordAttribute__Group_3_3__1"
    // InternalPxManual.g:3892:1: rule__KeywordAttribute__Group_3_3__1 : rule__KeywordAttribute__Group_3_3__1__Impl ;
    public final void rule__KeywordAttribute__Group_3_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3896:1: ( rule__KeywordAttribute__Group_3_3__1__Impl )
            // InternalPxManual.g:3897:2: rule__KeywordAttribute__Group_3_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__Group_3_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group_3_3__1"


    // $ANTLR start "rule__KeywordAttribute__Group_3_3__1__Impl"
    // InternalPxManual.g:3903:1: rule__KeywordAttribute__Group_3_3__1__Impl : ( ( rule__KeywordAttribute__PossibleValuesAssignment_3_3_1 ) ) ;
    public final void rule__KeywordAttribute__Group_3_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3907:1: ( ( ( rule__KeywordAttribute__PossibleValuesAssignment_3_3_1 ) ) )
            // InternalPxManual.g:3908:1: ( ( rule__KeywordAttribute__PossibleValuesAssignment_3_3_1 ) )
            {
            // InternalPxManual.g:3908:1: ( ( rule__KeywordAttribute__PossibleValuesAssignment_3_3_1 ) )
            // InternalPxManual.g:3909:2: ( rule__KeywordAttribute__PossibleValuesAssignment_3_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getPossibleValuesAssignment_3_3_1()); 
            }
            // InternalPxManual.g:3910:2: ( rule__KeywordAttribute__PossibleValuesAssignment_3_3_1 )
            // InternalPxManual.g:3910:3: rule__KeywordAttribute__PossibleValuesAssignment_3_3_1
            {
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__PossibleValuesAssignment_3_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getPossibleValuesAssignment_3_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__Group_3_3__1__Impl"


    // $ANTLR start "rule__Document__UnorderedGroup_2"
    // InternalPxManual.g:3919:1: rule__Document__UnorderedGroup_2 : ( rule__Document__UnorderedGroup_2__0 )? ;
    public final void rule__Document__UnorderedGroup_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getDocumentAccess().getUnorderedGroup_2());
        	
        try {
            // InternalPxManual.g:3924:1: ( ( rule__Document__UnorderedGroup_2__0 )? )
            // InternalPxManual.g:3925:2: ( rule__Document__UnorderedGroup_2__0 )?
            {
            // InternalPxManual.g:3925:2: ( rule__Document__UnorderedGroup_2__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( ( LA17_0 == RULE_STRING || LA17_0 >= 39 && LA17_0 <= 40 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 0) ) {
                alt17=1;
            }
            else if ( LA17_0 == 16 && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 1) ) {
                alt17=1;
            }
            else if ( LA17_0 == 17 && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 2) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalPxManual.g:3925:2: rule__Document__UnorderedGroup_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Document__UnorderedGroup_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getDocumentAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__UnorderedGroup_2"


    // $ANTLR start "rule__Document__UnorderedGroup_2__Impl"
    // InternalPxManual.g:3933:1: rule__Document__UnorderedGroup_2__Impl : ( ({...}? => ( ( ( rule__Document__TitleAssignment_2_0 ) ) ) ) | ({...}? => ( ( ( rule__Document__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Document__Group_2_2__0 ) ) ) ) ) ;
    public final void rule__Document__UnorderedGroup_2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
        	
        try {
            // InternalPxManual.g:3938:1: ( ( ({...}? => ( ( ( rule__Document__TitleAssignment_2_0 ) ) ) ) | ({...}? => ( ( ( rule__Document__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Document__Group_2_2__0 ) ) ) ) ) )
            // InternalPxManual.g:3939:3: ( ({...}? => ( ( ( rule__Document__TitleAssignment_2_0 ) ) ) ) | ({...}? => ( ( ( rule__Document__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Document__Group_2_2__0 ) ) ) ) )
            {
            // InternalPxManual.g:3939:3: ( ({...}? => ( ( ( rule__Document__TitleAssignment_2_0 ) ) ) ) | ({...}? => ( ( ( rule__Document__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Document__Group_2_2__0 ) ) ) ) )
            int alt18=3;
            int LA18_0 = input.LA(1);

            if ( ( LA18_0 == RULE_STRING || LA18_0 >= 39 && LA18_0 <= 40 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 0) ) {
                alt18=1;
            }
            else if ( LA18_0 == 16 && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 1) ) {
                alt18=2;
            }
            else if ( LA18_0 == 17 && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 2) ) {
                alt18=3;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // InternalPxManual.g:3940:3: ({...}? => ( ( ( rule__Document__TitleAssignment_2_0 ) ) ) )
                    {
                    // InternalPxManual.g:3940:3: ({...}? => ( ( ( rule__Document__TitleAssignment_2_0 ) ) ) )
                    // InternalPxManual.g:3941:4: {...}? => ( ( ( rule__Document__TitleAssignment_2_0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 0) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Document__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 0)");
                    }
                    // InternalPxManual.g:3941:104: ( ( ( rule__Document__TitleAssignment_2_0 ) ) )
                    // InternalPxManual.g:3942:5: ( ( rule__Document__TitleAssignment_2_0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 0);
                    selected = true;
                    // InternalPxManual.g:3948:5: ( ( rule__Document__TitleAssignment_2_0 ) )
                    // InternalPxManual.g:3949:6: ( rule__Document__TitleAssignment_2_0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getDocumentAccess().getTitleAssignment_2_0()); 
                    }
                    // InternalPxManual.g:3950:6: ( rule__Document__TitleAssignment_2_0 )
                    // InternalPxManual.g:3950:7: rule__Document__TitleAssignment_2_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Document__TitleAssignment_2_0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getDocumentAccess().getTitleAssignment_2_0()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:3955:3: ({...}? => ( ( ( rule__Document__Group_2_1__0 ) ) ) )
                    {
                    // InternalPxManual.g:3955:3: ({...}? => ( ( ( rule__Document__Group_2_1__0 ) ) ) )
                    // InternalPxManual.g:3956:4: {...}? => ( ( ( rule__Document__Group_2_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 1) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Document__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 1)");
                    }
                    // InternalPxManual.g:3956:104: ( ( ( rule__Document__Group_2_1__0 ) ) )
                    // InternalPxManual.g:3957:5: ( ( rule__Document__Group_2_1__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 1);
                    selected = true;
                    // InternalPxManual.g:3963:5: ( ( rule__Document__Group_2_1__0 ) )
                    // InternalPxManual.g:3964:6: ( rule__Document__Group_2_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getDocumentAccess().getGroup_2_1()); 
                    }
                    // InternalPxManual.g:3965:6: ( rule__Document__Group_2_1__0 )
                    // InternalPxManual.g:3965:7: rule__Document__Group_2_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Document__Group_2_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getDocumentAccess().getGroup_2_1()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPxManual.g:3970:3: ({...}? => ( ( ( rule__Document__Group_2_2__0 ) ) ) )
                    {
                    // InternalPxManual.g:3970:3: ({...}? => ( ( ( rule__Document__Group_2_2__0 ) ) ) )
                    // InternalPxManual.g:3971:4: {...}? => ( ( ( rule__Document__Group_2_2__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 2) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Document__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 2)");
                    }
                    // InternalPxManual.g:3971:104: ( ( ( rule__Document__Group_2_2__0 ) ) )
                    // InternalPxManual.g:3972:5: ( ( rule__Document__Group_2_2__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 2);
                    selected = true;
                    // InternalPxManual.g:3978:5: ( ( rule__Document__Group_2_2__0 ) )
                    // InternalPxManual.g:3979:6: ( rule__Document__Group_2_2__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getDocumentAccess().getGroup_2_2()); 
                    }
                    // InternalPxManual.g:3980:6: ( rule__Document__Group_2_2__0 )
                    // InternalPxManual.g:3980:7: rule__Document__Group_2_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Document__Group_2_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getDocumentAccess().getGroup_2_2()); 
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDocumentAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__UnorderedGroup_2__Impl"


    // $ANTLR start "rule__Document__UnorderedGroup_2__0"
    // InternalPxManual.g:3993:1: rule__Document__UnorderedGroup_2__0 : rule__Document__UnorderedGroup_2__Impl ( rule__Document__UnorderedGroup_2__1 )? ;
    public final void rule__Document__UnorderedGroup_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:3997:1: ( rule__Document__UnorderedGroup_2__Impl ( rule__Document__UnorderedGroup_2__1 )? )
            // InternalPxManual.g:3998:2: rule__Document__UnorderedGroup_2__Impl ( rule__Document__UnorderedGroup_2__1 )?
            {
            pushFollow(FOLLOW_33);
            rule__Document__UnorderedGroup_2__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:3999:2: ( rule__Document__UnorderedGroup_2__1 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( ( LA19_0 == RULE_STRING || LA19_0 >= 39 && LA19_0 <= 40 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 0) ) {
                alt19=1;
            }
            else if ( LA19_0 == 16 && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 1) ) {
                alt19=1;
            }
            else if ( LA19_0 == 17 && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 2) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalPxManual.g:3999:2: rule__Document__UnorderedGroup_2__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Document__UnorderedGroup_2__1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__UnorderedGroup_2__0"


    // $ANTLR start "rule__Document__UnorderedGroup_2__1"
    // InternalPxManual.g:4005:1: rule__Document__UnorderedGroup_2__1 : rule__Document__UnorderedGroup_2__Impl ( rule__Document__UnorderedGroup_2__2 )? ;
    public final void rule__Document__UnorderedGroup_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4009:1: ( rule__Document__UnorderedGroup_2__Impl ( rule__Document__UnorderedGroup_2__2 )? )
            // InternalPxManual.g:4010:2: rule__Document__UnorderedGroup_2__Impl ( rule__Document__UnorderedGroup_2__2 )?
            {
            pushFollow(FOLLOW_33);
            rule__Document__UnorderedGroup_2__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4011:2: ( rule__Document__UnorderedGroup_2__2 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( ( LA20_0 == RULE_STRING || LA20_0 >= 39 && LA20_0 <= 40 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 0) ) {
                alt20=1;
            }
            else if ( LA20_0 == 16 && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 1) ) {
                alt20=1;
            }
            else if ( LA20_0 == 17 && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 2) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalPxManual.g:4011:2: rule__Document__UnorderedGroup_2__2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Document__UnorderedGroup_2__2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__UnorderedGroup_2__1"


    // $ANTLR start "rule__Document__UnorderedGroup_2__2"
    // InternalPxManual.g:4017:1: rule__Document__UnorderedGroup_2__2 : rule__Document__UnorderedGroup_2__Impl ;
    public final void rule__Document__UnorderedGroup_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4021:1: ( rule__Document__UnorderedGroup_2__Impl )
            // InternalPxManual.g:4022:2: rule__Document__UnorderedGroup_2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Document__UnorderedGroup_2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__UnorderedGroup_2__2"


    // $ANTLR start "rule__Toc__UnorderedGroup_2"
    // InternalPxManual.g:4029:1: rule__Toc__UnorderedGroup_2 : ( rule__Toc__UnorderedGroup_2__0 )? ;
    public final void rule__Toc__UnorderedGroup_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getTocAccess().getUnorderedGroup_2());
        	
        try {
            // InternalPxManual.g:4034:1: ( ( rule__Toc__UnorderedGroup_2__0 )? )
            // InternalPxManual.g:4035:2: ( rule__Toc__UnorderedGroup_2__0 )?
            {
            // InternalPxManual.g:4035:2: ( rule__Toc__UnorderedGroup_2__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==39) ) {
                int LA21_1 = input.LA(2);

                if ( (LA21_1==RULE_ID) ) {
                    int LA21_6 = input.LA(3);

                    if ( (LA21_6==40) ) {
                        int LA21_2 = input.LA(4);

                        if ( (LA21_2==RULE_ID) ) {
                            int LA21_7 = input.LA(5);

                            if ( (LA21_7==RULE_STRING) ) {
                                int LA21_3 = input.LA(6);

                                if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                                    alt21=1;
                                }
                            }
                        }
                    }
                    else if ( (LA21_6==RULE_STRING) ) {
                        int LA21_3 = input.LA(4);

                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                            alt21=1;
                        }
                    }
                }
            }
            else if ( (LA21_0==40) ) {
                int LA21_2 = input.LA(2);

                if ( (LA21_2==RULE_ID) ) {
                    int LA21_7 = input.LA(3);

                    if ( (LA21_7==RULE_STRING) ) {
                        int LA21_3 = input.LA(4);

                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                            alt21=1;
                        }
                    }
                }
            }
            else if ( (LA21_0==RULE_STRING) ) {
                int LA21_3 = input.LA(2);

                if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                    alt21=1;
                }
            }
            else if ( LA21_0 == 21 && getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 1) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalPxManual.g:4035:2: rule__Toc__UnorderedGroup_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Toc__UnorderedGroup_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getTocAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__UnorderedGroup_2"


    // $ANTLR start "rule__Toc__UnorderedGroup_2__Impl"
    // InternalPxManual.g:4043:1: rule__Toc__UnorderedGroup_2__Impl : ( ({...}? => ( ( ( rule__Toc__TitleAssignment_2_0 ) ) ) ) | ({...}? => ( ( ( rule__Toc__Group_2_1__0 ) ) ) ) ) ;
    public final void rule__Toc__UnorderedGroup_2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
        	
        try {
            // InternalPxManual.g:4048:1: ( ( ({...}? => ( ( ( rule__Toc__TitleAssignment_2_0 ) ) ) ) | ({...}? => ( ( ( rule__Toc__Group_2_1__0 ) ) ) ) ) )
            // InternalPxManual.g:4049:3: ( ({...}? => ( ( ( rule__Toc__TitleAssignment_2_0 ) ) ) ) | ({...}? => ( ( ( rule__Toc__Group_2_1__0 ) ) ) ) )
            {
            // InternalPxManual.g:4049:3: ( ({...}? => ( ( ( rule__Toc__TitleAssignment_2_0 ) ) ) ) | ({...}? => ( ( ( rule__Toc__Group_2_1__0 ) ) ) ) )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( ( LA22_0 == RULE_STRING || LA22_0 >= 39 && LA22_0 <= 40 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                alt22=1;
            }
            else if ( LA22_0 == 21 && getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 1) ) {
                alt22=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // InternalPxManual.g:4050:3: ({...}? => ( ( ( rule__Toc__TitleAssignment_2_0 ) ) ) )
                    {
                    // InternalPxManual.g:4050:3: ({...}? => ( ( ( rule__Toc__TitleAssignment_2_0 ) ) ) )
                    // InternalPxManual.g:4051:4: {...}? => ( ( ( rule__Toc__TitleAssignment_2_0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Toc__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0)");
                    }
                    // InternalPxManual.g:4051:99: ( ( ( rule__Toc__TitleAssignment_2_0 ) ) )
                    // InternalPxManual.g:4052:5: ( ( rule__Toc__TitleAssignment_2_0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0);
                    selected = true;
                    // InternalPxManual.g:4058:5: ( ( rule__Toc__TitleAssignment_2_0 ) )
                    // InternalPxManual.g:4059:6: ( rule__Toc__TitleAssignment_2_0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTocAccess().getTitleAssignment_2_0()); 
                    }
                    // InternalPxManual.g:4060:6: ( rule__Toc__TitleAssignment_2_0 )
                    // InternalPxManual.g:4060:7: rule__Toc__TitleAssignment_2_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Toc__TitleAssignment_2_0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTocAccess().getTitleAssignment_2_0()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:4065:3: ({...}? => ( ( ( rule__Toc__Group_2_1__0 ) ) ) )
                    {
                    // InternalPxManual.g:4065:3: ({...}? => ( ( ( rule__Toc__Group_2_1__0 ) ) ) )
                    // InternalPxManual.g:4066:4: {...}? => ( ( ( rule__Toc__Group_2_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 1) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Toc__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 1)");
                    }
                    // InternalPxManual.g:4066:99: ( ( ( rule__Toc__Group_2_1__0 ) ) )
                    // InternalPxManual.g:4067:5: ( ( rule__Toc__Group_2_1__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getTocAccess().getUnorderedGroup_2(), 1);
                    selected = true;
                    // InternalPxManual.g:4073:5: ( ( rule__Toc__Group_2_1__0 ) )
                    // InternalPxManual.g:4074:6: ( rule__Toc__Group_2_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTocAccess().getGroup_2_1()); 
                    }
                    // InternalPxManual.g:4075:6: ( rule__Toc__Group_2_1__0 )
                    // InternalPxManual.g:4075:7: rule__Toc__Group_2_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Toc__Group_2_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTocAccess().getGroup_2_1()); 
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getTocAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__UnorderedGroup_2__Impl"


    // $ANTLR start "rule__Toc__UnorderedGroup_2__0"
    // InternalPxManual.g:4088:1: rule__Toc__UnorderedGroup_2__0 : rule__Toc__UnorderedGroup_2__Impl ( rule__Toc__UnorderedGroup_2__1 )? ;
    public final void rule__Toc__UnorderedGroup_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4092:1: ( rule__Toc__UnorderedGroup_2__Impl ( rule__Toc__UnorderedGroup_2__1 )? )
            // InternalPxManual.g:4093:2: rule__Toc__UnorderedGroup_2__Impl ( rule__Toc__UnorderedGroup_2__1 )?
            {
            pushFollow(FOLLOW_34);
            rule__Toc__UnorderedGroup_2__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4094:2: ( rule__Toc__UnorderedGroup_2__1 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==39) ) {
                int LA23_1 = input.LA(2);

                if ( (LA23_1==RULE_ID) ) {
                    int LA23_6 = input.LA(3);

                    if ( (LA23_6==40) ) {
                        int LA23_2 = input.LA(4);

                        if ( (LA23_2==RULE_ID) ) {
                            int LA23_7 = input.LA(5);

                            if ( (LA23_7==RULE_STRING) ) {
                                int LA23_3 = input.LA(6);

                                if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                                    alt23=1;
                                }
                            }
                        }
                    }
                    else if ( (LA23_6==RULE_STRING) ) {
                        int LA23_3 = input.LA(4);

                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                            alt23=1;
                        }
                    }
                }
            }
            else if ( (LA23_0==40) ) {
                int LA23_2 = input.LA(2);

                if ( (LA23_2==RULE_ID) ) {
                    int LA23_7 = input.LA(3);

                    if ( (LA23_7==RULE_STRING) ) {
                        int LA23_3 = input.LA(4);

                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                            alt23=1;
                        }
                    }
                }
            }
            else if ( (LA23_0==RULE_STRING) ) {
                int LA23_3 = input.LA(2);

                if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                    alt23=1;
                }
            }
            else if ( LA23_0 == 21 && getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 1) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalPxManual.g:4094:2: rule__Toc__UnorderedGroup_2__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Toc__UnorderedGroup_2__1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__UnorderedGroup_2__0"


    // $ANTLR start "rule__Toc__UnorderedGroup_2__1"
    // InternalPxManual.g:4100:1: rule__Toc__UnorderedGroup_2__1 : rule__Toc__UnorderedGroup_2__Impl ;
    public final void rule__Toc__UnorderedGroup_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4104:1: ( rule__Toc__UnorderedGroup_2__Impl )
            // InternalPxManual.g:4105:2: rule__Toc__UnorderedGroup_2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Toc__UnorderedGroup_2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__UnorderedGroup_2__1"


    // $ANTLR start "rule__Table__UnorderedGroup_2"
    // InternalPxManual.g:4112:1: rule__Table__UnorderedGroup_2 : ( rule__Table__UnorderedGroup_2__0 )? ;
    public final void rule__Table__UnorderedGroup_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getTableAccess().getUnorderedGroup_2());
        	
        try {
            // InternalPxManual.g:4117:1: ( ( rule__Table__UnorderedGroup_2__0 )? )
            // InternalPxManual.g:4118:2: ( rule__Table__UnorderedGroup_2__0 )?
            {
            // InternalPxManual.g:4118:2: ( rule__Table__UnorderedGroup_2__0 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( LA24_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 0) ) {
                alt24=1;
            }
            else if ( LA24_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 1) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalPxManual.g:4118:2: rule__Table__UnorderedGroup_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Table__UnorderedGroup_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getTableAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__UnorderedGroup_2"


    // $ANTLR start "rule__Table__UnorderedGroup_2__Impl"
    // InternalPxManual.g:4126:1: rule__Table__UnorderedGroup_2__Impl : ( ({...}? => ( ( ( rule__Table__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Table__Group_2_1__0 ) ) ) ) ) ;
    public final void rule__Table__UnorderedGroup_2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
        	
        try {
            // InternalPxManual.g:4131:1: ( ( ({...}? => ( ( ( rule__Table__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Table__Group_2_1__0 ) ) ) ) ) )
            // InternalPxManual.g:4132:3: ( ({...}? => ( ( ( rule__Table__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Table__Group_2_1__0 ) ) ) ) )
            {
            // InternalPxManual.g:4132:3: ( ({...}? => ( ( ( rule__Table__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Table__Group_2_1__0 ) ) ) ) )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( LA25_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 0) ) {
                alt25=1;
            }
            else if ( LA25_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 1) ) {
                alt25=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }
            switch (alt25) {
                case 1 :
                    // InternalPxManual.g:4133:3: ({...}? => ( ( ( rule__Table__Group_2_0__0 ) ) ) )
                    {
                    // InternalPxManual.g:4133:3: ({...}? => ( ( ( rule__Table__Group_2_0__0 ) ) ) )
                    // InternalPxManual.g:4134:4: {...}? => ( ( ( rule__Table__Group_2_0__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 0) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Table__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 0)");
                    }
                    // InternalPxManual.g:4134:101: ( ( ( rule__Table__Group_2_0__0 ) ) )
                    // InternalPxManual.g:4135:5: ( ( rule__Table__Group_2_0__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getTableAccess().getUnorderedGroup_2(), 0);
                    selected = true;
                    // InternalPxManual.g:4141:5: ( ( rule__Table__Group_2_0__0 ) )
                    // InternalPxManual.g:4142:6: ( rule__Table__Group_2_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTableAccess().getGroup_2_0()); 
                    }
                    // InternalPxManual.g:4143:6: ( rule__Table__Group_2_0__0 )
                    // InternalPxManual.g:4143:7: rule__Table__Group_2_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Table__Group_2_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTableAccess().getGroup_2_0()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:4148:3: ({...}? => ( ( ( rule__Table__Group_2_1__0 ) ) ) )
                    {
                    // InternalPxManual.g:4148:3: ({...}? => ( ( ( rule__Table__Group_2_1__0 ) ) ) )
                    // InternalPxManual.g:4149:4: {...}? => ( ( ( rule__Table__Group_2_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 1) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Table__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 1)");
                    }
                    // InternalPxManual.g:4149:101: ( ( ( rule__Table__Group_2_1__0 ) ) )
                    // InternalPxManual.g:4150:5: ( ( rule__Table__Group_2_1__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getTableAccess().getUnorderedGroup_2(), 1);
                    selected = true;
                    // InternalPxManual.g:4156:5: ( ( rule__Table__Group_2_1__0 ) )
                    // InternalPxManual.g:4157:6: ( rule__Table__Group_2_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTableAccess().getGroup_2_1()); 
                    }
                    // InternalPxManual.g:4158:6: ( rule__Table__Group_2_1__0 )
                    // InternalPxManual.g:4158:7: rule__Table__Group_2_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Table__Group_2_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTableAccess().getGroup_2_1()); 
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getTableAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__UnorderedGroup_2__Impl"


    // $ANTLR start "rule__Table__UnorderedGroup_2__0"
    // InternalPxManual.g:4171:1: rule__Table__UnorderedGroup_2__0 : rule__Table__UnorderedGroup_2__Impl ( rule__Table__UnorderedGroup_2__1 )? ;
    public final void rule__Table__UnorderedGroup_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4175:1: ( rule__Table__UnorderedGroup_2__Impl ( rule__Table__UnorderedGroup_2__1 )? )
            // InternalPxManual.g:4176:2: rule__Table__UnorderedGroup_2__Impl ( rule__Table__UnorderedGroup_2__1 )?
            {
            pushFollow(FOLLOW_35);
            rule__Table__UnorderedGroup_2__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4177:2: ( rule__Table__UnorderedGroup_2__1 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( LA26_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 0) ) {
                alt26=1;
            }
            else if ( LA26_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 1) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalPxManual.g:4177:2: rule__Table__UnorderedGroup_2__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Table__UnorderedGroup_2__1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__UnorderedGroup_2__0"


    // $ANTLR start "rule__Table__UnorderedGroup_2__1"
    // InternalPxManual.g:4183:1: rule__Table__UnorderedGroup_2__1 : rule__Table__UnorderedGroup_2__Impl ;
    public final void rule__Table__UnorderedGroup_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4187:1: ( rule__Table__UnorderedGroup_2__Impl )
            // InternalPxManual.g:4188:2: rule__Table__UnorderedGroup_2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Table__UnorderedGroup_2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__UnorderedGroup_2__1"


    // $ANTLR start "rule__Row__UnorderedGroup_2"
    // InternalPxManual.g:4195:1: rule__Row__UnorderedGroup_2 : ( rule__Row__UnorderedGroup_2__0 )? ;
    public final void rule__Row__UnorderedGroup_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getRowAccess().getUnorderedGroup_2());
        	
        try {
            // InternalPxManual.g:4200:1: ( ( rule__Row__UnorderedGroup_2__0 )? )
            // InternalPxManual.g:4201:2: ( rule__Row__UnorderedGroup_2__0 )?
            {
            // InternalPxManual.g:4201:2: ( rule__Row__UnorderedGroup_2__0 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( LA27_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 0) ) {
                alt27=1;
            }
            else if ( LA27_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 1) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalPxManual.g:4201:2: rule__Row__UnorderedGroup_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Row__UnorderedGroup_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getRowAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__UnorderedGroup_2"


    // $ANTLR start "rule__Row__UnorderedGroup_2__Impl"
    // InternalPxManual.g:4209:1: rule__Row__UnorderedGroup_2__Impl : ( ({...}? => ( ( ( rule__Row__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Row__Group_2_1__0 ) ) ) ) ) ;
    public final void rule__Row__UnorderedGroup_2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
        	
        try {
            // InternalPxManual.g:4214:1: ( ( ({...}? => ( ( ( rule__Row__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Row__Group_2_1__0 ) ) ) ) ) )
            // InternalPxManual.g:4215:3: ( ({...}? => ( ( ( rule__Row__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Row__Group_2_1__0 ) ) ) ) )
            {
            // InternalPxManual.g:4215:3: ( ({...}? => ( ( ( rule__Row__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Row__Group_2_1__0 ) ) ) ) )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( LA28_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 0) ) {
                alt28=1;
            }
            else if ( LA28_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 1) ) {
                alt28=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // InternalPxManual.g:4216:3: ({...}? => ( ( ( rule__Row__Group_2_0__0 ) ) ) )
                    {
                    // InternalPxManual.g:4216:3: ({...}? => ( ( ( rule__Row__Group_2_0__0 ) ) ) )
                    // InternalPxManual.g:4217:4: {...}? => ( ( ( rule__Row__Group_2_0__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 0) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Row__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 0)");
                    }
                    // InternalPxManual.g:4217:99: ( ( ( rule__Row__Group_2_0__0 ) ) )
                    // InternalPxManual.g:4218:5: ( ( rule__Row__Group_2_0__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getRowAccess().getUnorderedGroup_2(), 0);
                    selected = true;
                    // InternalPxManual.g:4224:5: ( ( rule__Row__Group_2_0__0 ) )
                    // InternalPxManual.g:4225:6: ( rule__Row__Group_2_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRowAccess().getGroup_2_0()); 
                    }
                    // InternalPxManual.g:4226:6: ( rule__Row__Group_2_0__0 )
                    // InternalPxManual.g:4226:7: rule__Row__Group_2_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Row__Group_2_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRowAccess().getGroup_2_0()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:4231:3: ({...}? => ( ( ( rule__Row__Group_2_1__0 ) ) ) )
                    {
                    // InternalPxManual.g:4231:3: ({...}? => ( ( ( rule__Row__Group_2_1__0 ) ) ) )
                    // InternalPxManual.g:4232:4: {...}? => ( ( ( rule__Row__Group_2_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 1) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Row__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 1)");
                    }
                    // InternalPxManual.g:4232:99: ( ( ( rule__Row__Group_2_1__0 ) ) )
                    // InternalPxManual.g:4233:5: ( ( rule__Row__Group_2_1__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getRowAccess().getUnorderedGroup_2(), 1);
                    selected = true;
                    // InternalPxManual.g:4239:5: ( ( rule__Row__Group_2_1__0 ) )
                    // InternalPxManual.g:4240:6: ( rule__Row__Group_2_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRowAccess().getGroup_2_1()); 
                    }
                    // InternalPxManual.g:4241:6: ( rule__Row__Group_2_1__0 )
                    // InternalPxManual.g:4241:7: rule__Row__Group_2_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Row__Group_2_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRowAccess().getGroup_2_1()); 
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getRowAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__UnorderedGroup_2__Impl"


    // $ANTLR start "rule__Row__UnorderedGroup_2__0"
    // InternalPxManual.g:4254:1: rule__Row__UnorderedGroup_2__0 : rule__Row__UnorderedGroup_2__Impl ( rule__Row__UnorderedGroup_2__1 )? ;
    public final void rule__Row__UnorderedGroup_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4258:1: ( rule__Row__UnorderedGroup_2__Impl ( rule__Row__UnorderedGroup_2__1 )? )
            // InternalPxManual.g:4259:2: rule__Row__UnorderedGroup_2__Impl ( rule__Row__UnorderedGroup_2__1 )?
            {
            pushFollow(FOLLOW_35);
            rule__Row__UnorderedGroup_2__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4260:2: ( rule__Row__UnorderedGroup_2__1 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( LA29_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 0) ) {
                alt29=1;
            }
            else if ( LA29_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 1) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalPxManual.g:4260:2: rule__Row__UnorderedGroup_2__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Row__UnorderedGroup_2__1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__UnorderedGroup_2__0"


    // $ANTLR start "rule__Row__UnorderedGroup_2__1"
    // InternalPxManual.g:4266:1: rule__Row__UnorderedGroup_2__1 : rule__Row__UnorderedGroup_2__Impl ;
    public final void rule__Row__UnorderedGroup_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4270:1: ( rule__Row__UnorderedGroup_2__Impl )
            // InternalPxManual.g:4271:2: rule__Row__UnorderedGroup_2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Row__UnorderedGroup_2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__UnorderedGroup_2__1"


    // $ANTLR start "rule__Cell__UnorderedGroup_1"
    // InternalPxManual.g:4278:1: rule__Cell__UnorderedGroup_1 : ( rule__Cell__UnorderedGroup_1__0 )? ;
    public final void rule__Cell__UnorderedGroup_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getCellAccess().getUnorderedGroup_1());
        	
        try {
            // InternalPxManual.g:4283:1: ( ( rule__Cell__UnorderedGroup_1__0 )? )
            // InternalPxManual.g:4284:2: ( rule__Cell__UnorderedGroup_1__0 )?
            {
            // InternalPxManual.g:4284:2: ( rule__Cell__UnorderedGroup_1__0 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( LA30_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0) ) {
                alt30=1;
            }
            else if ( LA30_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1) ) {
                alt30=1;
            }
            else if ( LA30_0 == 30 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2) ) {
                alt30=1;
            }
            else if ( LA30_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3) ) {
                alt30=1;
            }
            else if ( LA30_0 == 32 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalPxManual.g:4284:2: rule__Cell__UnorderedGroup_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cell__UnorderedGroup_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getCellAccess().getUnorderedGroup_1());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__UnorderedGroup_1"


    // $ANTLR start "rule__Cell__UnorderedGroup_1__Impl"
    // InternalPxManual.g:4292:1: rule__Cell__UnorderedGroup_1__Impl : ( ({...}? => ( ( ( rule__Cell__Group_1_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_2__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_3__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_4__0 ) ) ) ) ) ;
    public final void rule__Cell__UnorderedGroup_1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
        	
        try {
            // InternalPxManual.g:4297:1: ( ( ({...}? => ( ( ( rule__Cell__Group_1_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_2__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_3__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_4__0 ) ) ) ) ) )
            // InternalPxManual.g:4298:3: ( ({...}? => ( ( ( rule__Cell__Group_1_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_2__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_3__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_4__0 ) ) ) ) )
            {
            // InternalPxManual.g:4298:3: ( ({...}? => ( ( ( rule__Cell__Group_1_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_2__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_3__0 ) ) ) ) | ({...}? => ( ( ( rule__Cell__Group_1_4__0 ) ) ) ) )
            int alt31=5;
            int LA31_0 = input.LA(1);

            if ( LA31_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0) ) {
                alt31=1;
            }
            else if ( LA31_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1) ) {
                alt31=2;
            }
            else if ( LA31_0 == 30 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2) ) {
                alt31=3;
            }
            else if ( LA31_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3) ) {
                alt31=4;
            }
            else if ( LA31_0 == 32 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4) ) {
                alt31=5;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // InternalPxManual.g:4299:3: ({...}? => ( ( ( rule__Cell__Group_1_0__0 ) ) ) )
                    {
                    // InternalPxManual.g:4299:3: ({...}? => ( ( ( rule__Cell__Group_1_0__0 ) ) ) )
                    // InternalPxManual.g:4300:4: {...}? => ( ( ( rule__Cell__Group_1_0__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Cell__UnorderedGroup_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0)");
                    }
                    // InternalPxManual.g:4300:100: ( ( ( rule__Cell__Group_1_0__0 ) ) )
                    // InternalPxManual.g:4301:5: ( ( rule__Cell__Group_1_0__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0);
                    selected = true;
                    // InternalPxManual.g:4307:5: ( ( rule__Cell__Group_1_0__0 ) )
                    // InternalPxManual.g:4308:6: ( rule__Cell__Group_1_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCellAccess().getGroup_1_0()); 
                    }
                    // InternalPxManual.g:4309:6: ( rule__Cell__Group_1_0__0 )
                    // InternalPxManual.g:4309:7: rule__Cell__Group_1_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cell__Group_1_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCellAccess().getGroup_1_0()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:4314:3: ({...}? => ( ( ( rule__Cell__Group_1_1__0 ) ) ) )
                    {
                    // InternalPxManual.g:4314:3: ({...}? => ( ( ( rule__Cell__Group_1_1__0 ) ) ) )
                    // InternalPxManual.g:4315:4: {...}? => ( ( ( rule__Cell__Group_1_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Cell__UnorderedGroup_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1)");
                    }
                    // InternalPxManual.g:4315:100: ( ( ( rule__Cell__Group_1_1__0 ) ) )
                    // InternalPxManual.g:4316:5: ( ( rule__Cell__Group_1_1__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1);
                    selected = true;
                    // InternalPxManual.g:4322:5: ( ( rule__Cell__Group_1_1__0 ) )
                    // InternalPxManual.g:4323:6: ( rule__Cell__Group_1_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCellAccess().getGroup_1_1()); 
                    }
                    // InternalPxManual.g:4324:6: ( rule__Cell__Group_1_1__0 )
                    // InternalPxManual.g:4324:7: rule__Cell__Group_1_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cell__Group_1_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCellAccess().getGroup_1_1()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPxManual.g:4329:3: ({...}? => ( ( ( rule__Cell__Group_1_2__0 ) ) ) )
                    {
                    // InternalPxManual.g:4329:3: ({...}? => ( ( ( rule__Cell__Group_1_2__0 ) ) ) )
                    // InternalPxManual.g:4330:4: {...}? => ( ( ( rule__Cell__Group_1_2__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Cell__UnorderedGroup_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2)");
                    }
                    // InternalPxManual.g:4330:100: ( ( ( rule__Cell__Group_1_2__0 ) ) )
                    // InternalPxManual.g:4331:5: ( ( rule__Cell__Group_1_2__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2);
                    selected = true;
                    // InternalPxManual.g:4337:5: ( ( rule__Cell__Group_1_2__0 ) )
                    // InternalPxManual.g:4338:6: ( rule__Cell__Group_1_2__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCellAccess().getGroup_1_2()); 
                    }
                    // InternalPxManual.g:4339:6: ( rule__Cell__Group_1_2__0 )
                    // InternalPxManual.g:4339:7: rule__Cell__Group_1_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cell__Group_1_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCellAccess().getGroup_1_2()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalPxManual.g:4344:3: ({...}? => ( ( ( rule__Cell__Group_1_3__0 ) ) ) )
                    {
                    // InternalPxManual.g:4344:3: ({...}? => ( ( ( rule__Cell__Group_1_3__0 ) ) ) )
                    // InternalPxManual.g:4345:4: {...}? => ( ( ( rule__Cell__Group_1_3__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Cell__UnorderedGroup_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3)");
                    }
                    // InternalPxManual.g:4345:100: ( ( ( rule__Cell__Group_1_3__0 ) ) )
                    // InternalPxManual.g:4346:5: ( ( rule__Cell__Group_1_3__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3);
                    selected = true;
                    // InternalPxManual.g:4352:5: ( ( rule__Cell__Group_1_3__0 ) )
                    // InternalPxManual.g:4353:6: ( rule__Cell__Group_1_3__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCellAccess().getGroup_1_3()); 
                    }
                    // InternalPxManual.g:4354:6: ( rule__Cell__Group_1_3__0 )
                    // InternalPxManual.g:4354:7: rule__Cell__Group_1_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cell__Group_1_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCellAccess().getGroup_1_3()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalPxManual.g:4359:3: ({...}? => ( ( ( rule__Cell__Group_1_4__0 ) ) ) )
                    {
                    // InternalPxManual.g:4359:3: ({...}? => ( ( ( rule__Cell__Group_1_4__0 ) ) ) )
                    // InternalPxManual.g:4360:4: {...}? => ( ( ( rule__Cell__Group_1_4__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Cell__UnorderedGroup_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4)");
                    }
                    // InternalPxManual.g:4360:100: ( ( ( rule__Cell__Group_1_4__0 ) ) )
                    // InternalPxManual.g:4361:5: ( ( rule__Cell__Group_1_4__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4);
                    selected = true;
                    // InternalPxManual.g:4367:5: ( ( rule__Cell__Group_1_4__0 ) )
                    // InternalPxManual.g:4368:6: ( rule__Cell__Group_1_4__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCellAccess().getGroup_1_4()); 
                    }
                    // InternalPxManual.g:4369:6: ( rule__Cell__Group_1_4__0 )
                    // InternalPxManual.g:4369:7: rule__Cell__Group_1_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cell__Group_1_4__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCellAccess().getGroup_1_4()); 
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getCellAccess().getUnorderedGroup_1());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__UnorderedGroup_1__Impl"


    // $ANTLR start "rule__Cell__UnorderedGroup_1__0"
    // InternalPxManual.g:4382:1: rule__Cell__UnorderedGroup_1__0 : rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__1 )? ;
    public final void rule__Cell__UnorderedGroup_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4386:1: ( rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__1 )? )
            // InternalPxManual.g:4387:2: rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__1 )?
            {
            pushFollow(FOLLOW_36);
            rule__Cell__UnorderedGroup_1__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4388:2: ( rule__Cell__UnorderedGroup_1__1 )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( LA32_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0) ) {
                alt32=1;
            }
            else if ( LA32_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1) ) {
                alt32=1;
            }
            else if ( LA32_0 == 30 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2) ) {
                alt32=1;
            }
            else if ( LA32_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3) ) {
                alt32=1;
            }
            else if ( LA32_0 == 32 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalPxManual.g:4388:2: rule__Cell__UnorderedGroup_1__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cell__UnorderedGroup_1__1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__UnorderedGroup_1__0"


    // $ANTLR start "rule__Cell__UnorderedGroup_1__1"
    // InternalPxManual.g:4394:1: rule__Cell__UnorderedGroup_1__1 : rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__2 )? ;
    public final void rule__Cell__UnorderedGroup_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4398:1: ( rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__2 )? )
            // InternalPxManual.g:4399:2: rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__2 )?
            {
            pushFollow(FOLLOW_36);
            rule__Cell__UnorderedGroup_1__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4400:2: ( rule__Cell__UnorderedGroup_1__2 )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( LA33_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0) ) {
                alt33=1;
            }
            else if ( LA33_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1) ) {
                alt33=1;
            }
            else if ( LA33_0 == 30 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2) ) {
                alt33=1;
            }
            else if ( LA33_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3) ) {
                alt33=1;
            }
            else if ( LA33_0 == 32 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalPxManual.g:4400:2: rule__Cell__UnorderedGroup_1__2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cell__UnorderedGroup_1__2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__UnorderedGroup_1__1"


    // $ANTLR start "rule__Cell__UnorderedGroup_1__2"
    // InternalPxManual.g:4406:1: rule__Cell__UnorderedGroup_1__2 : rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__3 )? ;
    public final void rule__Cell__UnorderedGroup_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4410:1: ( rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__3 )? )
            // InternalPxManual.g:4411:2: rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__3 )?
            {
            pushFollow(FOLLOW_36);
            rule__Cell__UnorderedGroup_1__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4412:2: ( rule__Cell__UnorderedGroup_1__3 )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( LA34_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0) ) {
                alt34=1;
            }
            else if ( LA34_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1) ) {
                alt34=1;
            }
            else if ( LA34_0 == 30 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2) ) {
                alt34=1;
            }
            else if ( LA34_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3) ) {
                alt34=1;
            }
            else if ( LA34_0 == 32 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalPxManual.g:4412:2: rule__Cell__UnorderedGroup_1__3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cell__UnorderedGroup_1__3();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__UnorderedGroup_1__2"


    // $ANTLR start "rule__Cell__UnorderedGroup_1__3"
    // InternalPxManual.g:4418:1: rule__Cell__UnorderedGroup_1__3 : rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__4 )? ;
    public final void rule__Cell__UnorderedGroup_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4422:1: ( rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__4 )? )
            // InternalPxManual.g:4423:2: rule__Cell__UnorderedGroup_1__Impl ( rule__Cell__UnorderedGroup_1__4 )?
            {
            pushFollow(FOLLOW_36);
            rule__Cell__UnorderedGroup_1__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4424:2: ( rule__Cell__UnorderedGroup_1__4 )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( LA35_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0) ) {
                alt35=1;
            }
            else if ( LA35_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1) ) {
                alt35=1;
            }
            else if ( LA35_0 == 30 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2) ) {
                alt35=1;
            }
            else if ( LA35_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3) ) {
                alt35=1;
            }
            else if ( LA35_0 == 32 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalPxManual.g:4424:2: rule__Cell__UnorderedGroup_1__4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cell__UnorderedGroup_1__4();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__UnorderedGroup_1__3"


    // $ANTLR start "rule__Cell__UnorderedGroup_1__4"
    // InternalPxManual.g:4430:1: rule__Cell__UnorderedGroup_1__4 : rule__Cell__UnorderedGroup_1__Impl ;
    public final void rule__Cell__UnorderedGroup_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4434:1: ( rule__Cell__UnorderedGroup_1__Impl )
            // InternalPxManual.g:4435:2: rule__Cell__UnorderedGroup_1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cell__UnorderedGroup_1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__UnorderedGroup_1__4"


    // $ANTLR start "rule__Image__UnorderedGroup_3"
    // InternalPxManual.g:4442:1: rule__Image__UnorderedGroup_3 : ( rule__Image__UnorderedGroup_3__0 )? ;
    public final void rule__Image__UnorderedGroup_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getImageAccess().getUnorderedGroup_3());
        	
        try {
            // InternalPxManual.g:4447:1: ( ( rule__Image__UnorderedGroup_3__0 )? )
            // InternalPxManual.g:4448:2: ( rule__Image__UnorderedGroup_3__0 )?
            {
            // InternalPxManual.g:4448:2: ( rule__Image__UnorderedGroup_3__0 )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( LA36_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0) ) {
                alt36=1;
            }
            else if ( LA36_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1) ) {
                alt36=1;
            }
            else if ( LA36_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalPxManual.g:4448:2: rule__Image__UnorderedGroup_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Image__UnorderedGroup_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getImageAccess().getUnorderedGroup_3());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__UnorderedGroup_3"


    // $ANTLR start "rule__Image__UnorderedGroup_3__Impl"
    // InternalPxManual.g:4456:1: rule__Image__UnorderedGroup_3__Impl : ( ({...}? => ( ( ( rule__Image__Group_3_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Image__Group_3_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Image__Group_3_2__0 ) ) ) ) ) ;
    public final void rule__Image__UnorderedGroup_3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
        	
        try {
            // InternalPxManual.g:4461:1: ( ( ({...}? => ( ( ( rule__Image__Group_3_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Image__Group_3_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Image__Group_3_2__0 ) ) ) ) ) )
            // InternalPxManual.g:4462:3: ( ({...}? => ( ( ( rule__Image__Group_3_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Image__Group_3_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Image__Group_3_2__0 ) ) ) ) )
            {
            // InternalPxManual.g:4462:3: ( ({...}? => ( ( ( rule__Image__Group_3_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Image__Group_3_1__0 ) ) ) ) | ({...}? => ( ( ( rule__Image__Group_3_2__0 ) ) ) ) )
            int alt37=3;
            int LA37_0 = input.LA(1);

            if ( LA37_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0) ) {
                alt37=1;
            }
            else if ( LA37_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1) ) {
                alt37=2;
            }
            else if ( LA37_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2) ) {
                alt37=3;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;
            }
            switch (alt37) {
                case 1 :
                    // InternalPxManual.g:4463:3: ({...}? => ( ( ( rule__Image__Group_3_0__0 ) ) ) )
                    {
                    // InternalPxManual.g:4463:3: ({...}? => ( ( ( rule__Image__Group_3_0__0 ) ) ) )
                    // InternalPxManual.g:4464:4: {...}? => ( ( ( rule__Image__Group_3_0__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Image__UnorderedGroup_3__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0)");
                    }
                    // InternalPxManual.g:4464:101: ( ( ( rule__Image__Group_3_0__0 ) ) )
                    // InternalPxManual.g:4465:5: ( ( rule__Image__Group_3_0__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0);
                    selected = true;
                    // InternalPxManual.g:4471:5: ( ( rule__Image__Group_3_0__0 ) )
                    // InternalPxManual.g:4472:6: ( rule__Image__Group_3_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getImageAccess().getGroup_3_0()); 
                    }
                    // InternalPxManual.g:4473:6: ( rule__Image__Group_3_0__0 )
                    // InternalPxManual.g:4473:7: rule__Image__Group_3_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Image__Group_3_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getImageAccess().getGroup_3_0()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:4478:3: ({...}? => ( ( ( rule__Image__Group_3_1__0 ) ) ) )
                    {
                    // InternalPxManual.g:4478:3: ({...}? => ( ( ( rule__Image__Group_3_1__0 ) ) ) )
                    // InternalPxManual.g:4479:4: {...}? => ( ( ( rule__Image__Group_3_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Image__UnorderedGroup_3__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1)");
                    }
                    // InternalPxManual.g:4479:101: ( ( ( rule__Image__Group_3_1__0 ) ) )
                    // InternalPxManual.g:4480:5: ( ( rule__Image__Group_3_1__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1);
                    selected = true;
                    // InternalPxManual.g:4486:5: ( ( rule__Image__Group_3_1__0 ) )
                    // InternalPxManual.g:4487:6: ( rule__Image__Group_3_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getImageAccess().getGroup_3_1()); 
                    }
                    // InternalPxManual.g:4488:6: ( rule__Image__Group_3_1__0 )
                    // InternalPxManual.g:4488:7: rule__Image__Group_3_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Image__Group_3_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getImageAccess().getGroup_3_1()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPxManual.g:4493:3: ({...}? => ( ( ( rule__Image__Group_3_2__0 ) ) ) )
                    {
                    // InternalPxManual.g:4493:3: ({...}? => ( ( ( rule__Image__Group_3_2__0 ) ) ) )
                    // InternalPxManual.g:4494:4: {...}? => ( ( ( rule__Image__Group_3_2__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Image__UnorderedGroup_3__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2)");
                    }
                    // InternalPxManual.g:4494:101: ( ( ( rule__Image__Group_3_2__0 ) ) )
                    // InternalPxManual.g:4495:5: ( ( rule__Image__Group_3_2__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2);
                    selected = true;
                    // InternalPxManual.g:4501:5: ( ( rule__Image__Group_3_2__0 ) )
                    // InternalPxManual.g:4502:6: ( rule__Image__Group_3_2__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getImageAccess().getGroup_3_2()); 
                    }
                    // InternalPxManual.g:4503:6: ( rule__Image__Group_3_2__0 )
                    // InternalPxManual.g:4503:7: rule__Image__Group_3_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Image__Group_3_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getImageAccess().getGroup_3_2()); 
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getImageAccess().getUnorderedGroup_3());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__UnorderedGroup_3__Impl"


    // $ANTLR start "rule__Image__UnorderedGroup_3__0"
    // InternalPxManual.g:4516:1: rule__Image__UnorderedGroup_3__0 : rule__Image__UnorderedGroup_3__Impl ( rule__Image__UnorderedGroup_3__1 )? ;
    public final void rule__Image__UnorderedGroup_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4520:1: ( rule__Image__UnorderedGroup_3__Impl ( rule__Image__UnorderedGroup_3__1 )? )
            // InternalPxManual.g:4521:2: rule__Image__UnorderedGroup_3__Impl ( rule__Image__UnorderedGroup_3__1 )?
            {
            pushFollow(FOLLOW_37);
            rule__Image__UnorderedGroup_3__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4522:2: ( rule__Image__UnorderedGroup_3__1 )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( LA38_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0) ) {
                alt38=1;
            }
            else if ( LA38_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1) ) {
                alt38=1;
            }
            else if ( LA38_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalPxManual.g:4522:2: rule__Image__UnorderedGroup_3__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Image__UnorderedGroup_3__1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__UnorderedGroup_3__0"


    // $ANTLR start "rule__Image__UnorderedGroup_3__1"
    // InternalPxManual.g:4528:1: rule__Image__UnorderedGroup_3__1 : rule__Image__UnorderedGroup_3__Impl ( rule__Image__UnorderedGroup_3__2 )? ;
    public final void rule__Image__UnorderedGroup_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4532:1: ( rule__Image__UnorderedGroup_3__Impl ( rule__Image__UnorderedGroup_3__2 )? )
            // InternalPxManual.g:4533:2: rule__Image__UnorderedGroup_3__Impl ( rule__Image__UnorderedGroup_3__2 )?
            {
            pushFollow(FOLLOW_37);
            rule__Image__UnorderedGroup_3__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4534:2: ( rule__Image__UnorderedGroup_3__2 )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( LA39_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0) ) {
                alt39=1;
            }
            else if ( LA39_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1) ) {
                alt39=1;
            }
            else if ( LA39_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalPxManual.g:4534:2: rule__Image__UnorderedGroup_3__2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Image__UnorderedGroup_3__2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__UnorderedGroup_3__1"


    // $ANTLR start "rule__Image__UnorderedGroup_3__2"
    // InternalPxManual.g:4540:1: rule__Image__UnorderedGroup_3__2 : rule__Image__UnorderedGroup_3__Impl ;
    public final void rule__Image__UnorderedGroup_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4544:1: ( rule__Image__UnorderedGroup_3__Impl )
            // InternalPxManual.g:4545:2: rule__Image__UnorderedGroup_3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Image__UnorderedGroup_3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__UnorderedGroup_3__2"


    // $ANTLR start "rule__Example__UnorderedGroup_1"
    // InternalPxManual.g:4552:1: rule__Example__UnorderedGroup_1 : ( rule__Example__UnorderedGroup_1__0 )? ;
    public final void rule__Example__UnorderedGroup_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getExampleAccess().getUnorderedGroup_1());
        	
        try {
            // InternalPxManual.g:4557:1: ( ( rule__Example__UnorderedGroup_1__0 )? )
            // InternalPxManual.g:4558:2: ( rule__Example__UnorderedGroup_1__0 )?
            {
            // InternalPxManual.g:4558:2: ( rule__Example__UnorderedGroup_1__0 )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( ( LA40_0 == RULE_STRING || LA40_0 >= 39 && LA40_0 <= 40 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 0) ) {
                alt40=1;
            }
            else if ( LA40_0 == 37 && getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 1) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalPxManual.g:4558:2: rule__Example__UnorderedGroup_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Example__UnorderedGroup_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getExampleAccess().getUnorderedGroup_1());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__UnorderedGroup_1"


    // $ANTLR start "rule__Example__UnorderedGroup_1__Impl"
    // InternalPxManual.g:4566:1: rule__Example__UnorderedGroup_1__Impl : ( ({...}? => ( ( ( rule__Example__TitleAssignment_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Example__Group_1_1__0 ) ) ) ) ) ;
    public final void rule__Example__UnorderedGroup_1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
        	
        try {
            // InternalPxManual.g:4571:1: ( ( ({...}? => ( ( ( rule__Example__TitleAssignment_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Example__Group_1_1__0 ) ) ) ) ) )
            // InternalPxManual.g:4572:3: ( ({...}? => ( ( ( rule__Example__TitleAssignment_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Example__Group_1_1__0 ) ) ) ) )
            {
            // InternalPxManual.g:4572:3: ( ({...}? => ( ( ( rule__Example__TitleAssignment_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Example__Group_1_1__0 ) ) ) ) )
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( ( LA41_0 == RULE_STRING || LA41_0 >= 39 && LA41_0 <= 40 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 0) ) {
                alt41=1;
            }
            else if ( LA41_0 == 37 && getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 1) ) {
                alt41=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 41, 0, input);

                throw nvae;
            }
            switch (alt41) {
                case 1 :
                    // InternalPxManual.g:4573:3: ({...}? => ( ( ( rule__Example__TitleAssignment_1_0 ) ) ) )
                    {
                    // InternalPxManual.g:4573:3: ({...}? => ( ( ( rule__Example__TitleAssignment_1_0 ) ) ) )
                    // InternalPxManual.g:4574:4: {...}? => ( ( ( rule__Example__TitleAssignment_1_0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 0) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Example__UnorderedGroup_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 0)");
                    }
                    // InternalPxManual.g:4574:103: ( ( ( rule__Example__TitleAssignment_1_0 ) ) )
                    // InternalPxManual.g:4575:5: ( ( rule__Example__TitleAssignment_1_0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 0);
                    selected = true;
                    // InternalPxManual.g:4581:5: ( ( rule__Example__TitleAssignment_1_0 ) )
                    // InternalPxManual.g:4582:6: ( rule__Example__TitleAssignment_1_0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExampleAccess().getTitleAssignment_1_0()); 
                    }
                    // InternalPxManual.g:4583:6: ( rule__Example__TitleAssignment_1_0 )
                    // InternalPxManual.g:4583:7: rule__Example__TitleAssignment_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Example__TitleAssignment_1_0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExampleAccess().getTitleAssignment_1_0()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:4588:3: ({...}? => ( ( ( rule__Example__Group_1_1__0 ) ) ) )
                    {
                    // InternalPxManual.g:4588:3: ({...}? => ( ( ( rule__Example__Group_1_1__0 ) ) ) )
                    // InternalPxManual.g:4589:4: {...}? => ( ( ( rule__Example__Group_1_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 1) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Example__UnorderedGroup_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 1)");
                    }
                    // InternalPxManual.g:4589:103: ( ( ( rule__Example__Group_1_1__0 ) ) )
                    // InternalPxManual.g:4590:5: ( ( rule__Example__Group_1_1__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 1);
                    selected = true;
                    // InternalPxManual.g:4596:5: ( ( rule__Example__Group_1_1__0 ) )
                    // InternalPxManual.g:4597:6: ( rule__Example__Group_1_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExampleAccess().getGroup_1_1()); 
                    }
                    // InternalPxManual.g:4598:6: ( rule__Example__Group_1_1__0 )
                    // InternalPxManual.g:4598:7: rule__Example__Group_1_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Example__Group_1_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExampleAccess().getGroup_1_1()); 
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getExampleAccess().getUnorderedGroup_1());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__UnorderedGroup_1__Impl"


    // $ANTLR start "rule__Example__UnorderedGroup_1__0"
    // InternalPxManual.g:4611:1: rule__Example__UnorderedGroup_1__0 : rule__Example__UnorderedGroup_1__Impl ( rule__Example__UnorderedGroup_1__1 )? ;
    public final void rule__Example__UnorderedGroup_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4615:1: ( rule__Example__UnorderedGroup_1__Impl ( rule__Example__UnorderedGroup_1__1 )? )
            // InternalPxManual.g:4616:2: rule__Example__UnorderedGroup_1__Impl ( rule__Example__UnorderedGroup_1__1 )?
            {
            pushFollow(FOLLOW_38);
            rule__Example__UnorderedGroup_1__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4617:2: ( rule__Example__UnorderedGroup_1__1 )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( ( LA42_0 == RULE_STRING || LA42_0 >= 39 && LA42_0 <= 40 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 0) ) {
                alt42=1;
            }
            else if ( LA42_0 == 37 && getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 1) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalPxManual.g:4617:2: rule__Example__UnorderedGroup_1__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Example__UnorderedGroup_1__1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__UnorderedGroup_1__0"


    // $ANTLR start "rule__Example__UnorderedGroup_1__1"
    // InternalPxManual.g:4623:1: rule__Example__UnorderedGroup_1__1 : rule__Example__UnorderedGroup_1__Impl ;
    public final void rule__Example__UnorderedGroup_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4627:1: ( rule__Example__UnorderedGroup_1__Impl )
            // InternalPxManual.g:4628:2: rule__Example__UnorderedGroup_1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Example__UnorderedGroup_1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__UnorderedGroup_1__1"


    // $ANTLR start "rule__Usage__UnorderedGroup_1"
    // InternalPxManual.g:4635:1: rule__Usage__UnorderedGroup_1 : ( rule__Usage__UnorderedGroup_1__0 )? ;
    public final void rule__Usage__UnorderedGroup_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getUsageAccess().getUnorderedGroup_1());
        	
        try {
            // InternalPxManual.g:4640:1: ( ( rule__Usage__UnorderedGroup_1__0 )? )
            // InternalPxManual.g:4641:2: ( rule__Usage__UnorderedGroup_1__0 )?
            {
            // InternalPxManual.g:4641:2: ( rule__Usage__UnorderedGroup_1__0 )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( ( LA43_0 == RULE_STRING || LA43_0 >= 39 && LA43_0 <= 40 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 0) ) {
                alt43=1;
            }
            else if ( LA43_0 == 37 && getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 1) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // InternalPxManual.g:4641:2: rule__Usage__UnorderedGroup_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Usage__UnorderedGroup_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getUsageAccess().getUnorderedGroup_1());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__UnorderedGroup_1"


    // $ANTLR start "rule__Usage__UnorderedGroup_1__Impl"
    // InternalPxManual.g:4649:1: rule__Usage__UnorderedGroup_1__Impl : ( ({...}? => ( ( ( rule__Usage__TitleAssignment_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Usage__Group_1_1__0 ) ) ) ) ) ;
    public final void rule__Usage__UnorderedGroup_1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
        	
        try {
            // InternalPxManual.g:4654:1: ( ( ({...}? => ( ( ( rule__Usage__TitleAssignment_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Usage__Group_1_1__0 ) ) ) ) ) )
            // InternalPxManual.g:4655:3: ( ({...}? => ( ( ( rule__Usage__TitleAssignment_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Usage__Group_1_1__0 ) ) ) ) )
            {
            // InternalPxManual.g:4655:3: ( ({...}? => ( ( ( rule__Usage__TitleAssignment_1_0 ) ) ) ) | ({...}? => ( ( ( rule__Usage__Group_1_1__0 ) ) ) ) )
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( ( LA44_0 == RULE_STRING || LA44_0 >= 39 && LA44_0 <= 40 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 0) ) {
                alt44=1;
            }
            else if ( LA44_0 == 37 && getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 1) ) {
                alt44=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;
            }
            switch (alt44) {
                case 1 :
                    // InternalPxManual.g:4656:3: ({...}? => ( ( ( rule__Usage__TitleAssignment_1_0 ) ) ) )
                    {
                    // InternalPxManual.g:4656:3: ({...}? => ( ( ( rule__Usage__TitleAssignment_1_0 ) ) ) )
                    // InternalPxManual.g:4657:4: {...}? => ( ( ( rule__Usage__TitleAssignment_1_0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 0) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Usage__UnorderedGroup_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 0)");
                    }
                    // InternalPxManual.g:4657:101: ( ( ( rule__Usage__TitleAssignment_1_0 ) ) )
                    // InternalPxManual.g:4658:5: ( ( rule__Usage__TitleAssignment_1_0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 0);
                    selected = true;
                    // InternalPxManual.g:4664:5: ( ( rule__Usage__TitleAssignment_1_0 ) )
                    // InternalPxManual.g:4665:6: ( rule__Usage__TitleAssignment_1_0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getUsageAccess().getTitleAssignment_1_0()); 
                    }
                    // InternalPxManual.g:4666:6: ( rule__Usage__TitleAssignment_1_0 )
                    // InternalPxManual.g:4666:7: rule__Usage__TitleAssignment_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Usage__TitleAssignment_1_0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getUsageAccess().getTitleAssignment_1_0()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:4671:3: ({...}? => ( ( ( rule__Usage__Group_1_1__0 ) ) ) )
                    {
                    // InternalPxManual.g:4671:3: ({...}? => ( ( ( rule__Usage__Group_1_1__0 ) ) ) )
                    // InternalPxManual.g:4672:4: {...}? => ( ( ( rule__Usage__Group_1_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 1) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Usage__UnorderedGroup_1__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 1)");
                    }
                    // InternalPxManual.g:4672:101: ( ( ( rule__Usage__Group_1_1__0 ) ) )
                    // InternalPxManual.g:4673:5: ( ( rule__Usage__Group_1_1__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 1);
                    selected = true;
                    // InternalPxManual.g:4679:5: ( ( rule__Usage__Group_1_1__0 ) )
                    // InternalPxManual.g:4680:6: ( rule__Usage__Group_1_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getUsageAccess().getGroup_1_1()); 
                    }
                    // InternalPxManual.g:4681:6: ( rule__Usage__Group_1_1__0 )
                    // InternalPxManual.g:4681:7: rule__Usage__Group_1_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Usage__Group_1_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getUsageAccess().getGroup_1_1()); 
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getUsageAccess().getUnorderedGroup_1());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__UnorderedGroup_1__Impl"


    // $ANTLR start "rule__Usage__UnorderedGroup_1__0"
    // InternalPxManual.g:4694:1: rule__Usage__UnorderedGroup_1__0 : rule__Usage__UnorderedGroup_1__Impl ( rule__Usage__UnorderedGroup_1__1 )? ;
    public final void rule__Usage__UnorderedGroup_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4698:1: ( rule__Usage__UnorderedGroup_1__Impl ( rule__Usage__UnorderedGroup_1__1 )? )
            // InternalPxManual.g:4699:2: rule__Usage__UnorderedGroup_1__Impl ( rule__Usage__UnorderedGroup_1__1 )?
            {
            pushFollow(FOLLOW_38);
            rule__Usage__UnorderedGroup_1__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4700:2: ( rule__Usage__UnorderedGroup_1__1 )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( ( LA45_0 == RULE_STRING || LA45_0 >= 39 && LA45_0 <= 40 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 0) ) {
                alt45=1;
            }
            else if ( LA45_0 == 37 && getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 1) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalPxManual.g:4700:2: rule__Usage__UnorderedGroup_1__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Usage__UnorderedGroup_1__1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__UnorderedGroup_1__0"


    // $ANTLR start "rule__Usage__UnorderedGroup_1__1"
    // InternalPxManual.g:4706:1: rule__Usage__UnorderedGroup_1__1 : rule__Usage__UnorderedGroup_1__Impl ;
    public final void rule__Usage__UnorderedGroup_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4710:1: ( rule__Usage__UnorderedGroup_1__Impl )
            // InternalPxManual.g:4711:2: rule__Usage__UnorderedGroup_1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Usage__UnorderedGroup_1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__UnorderedGroup_1__1"


    // $ANTLR start "rule__Keyword__UnorderedGroup_4"
    // InternalPxManual.g:4718:1: rule__Keyword__UnorderedGroup_4 : ( rule__Keyword__UnorderedGroup_4__0 )? ;
    public final void rule__Keyword__UnorderedGroup_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getKeywordAccess().getUnorderedGroup_4());
        	
        try {
            // InternalPxManual.g:4723:1: ( ( rule__Keyword__UnorderedGroup_4__0 )? )
            // InternalPxManual.g:4724:2: ( rule__Keyword__UnorderedGroup_4__0 )?
            {
            // InternalPxManual.g:4724:2: ( rule__Keyword__UnorderedGroup_4__0 )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( LA46_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0) ) {
                alt46=1;
            }
            else if ( LA46_0 == 43 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1) ) {
                alt46=1;
            }
            else if ( ( LA46_0 == 36 || LA46_0 == 38 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2) ) {
                alt46=1;
            }
            else if ( LA46_0 == 45 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3) ) {
                alt46=1;
            }
            else if ( LA46_0 == 44 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalPxManual.g:4724:2: rule__Keyword__UnorderedGroup_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Keyword__UnorderedGroup_4__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getKeywordAccess().getUnorderedGroup_4());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__UnorderedGroup_4"


    // $ANTLR start "rule__Keyword__UnorderedGroup_4__Impl"
    // InternalPxManual.g:4732:1: rule__Keyword__UnorderedGroup_4__Impl : ( ({...}? => ( ( ( rule__Keyword__Group_4_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Keyword__Group_4_1__0 ) ) ) ) | ({...}? => ( ( ( ( rule__Keyword__ExamplesAssignment_4_2 ) ) ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* ) ) ) ) | ({...}? => ( ( ( ( rule__Keyword__AttributesAssignment_4_3 ) ) ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* ) ) ) ) | ({...}? => ( ( ( rule__Keyword__Group_4_4__0 ) ) ) ) ) ;
    public final void rule__Keyword__UnorderedGroup_4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
        	
        try {
            // InternalPxManual.g:4737:1: ( ( ({...}? => ( ( ( rule__Keyword__Group_4_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Keyword__Group_4_1__0 ) ) ) ) | ({...}? => ( ( ( ( rule__Keyword__ExamplesAssignment_4_2 ) ) ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* ) ) ) ) | ({...}? => ( ( ( ( rule__Keyword__AttributesAssignment_4_3 ) ) ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* ) ) ) ) | ({...}? => ( ( ( rule__Keyword__Group_4_4__0 ) ) ) ) ) )
            // InternalPxManual.g:4738:3: ( ({...}? => ( ( ( rule__Keyword__Group_4_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Keyword__Group_4_1__0 ) ) ) ) | ({...}? => ( ( ( ( rule__Keyword__ExamplesAssignment_4_2 ) ) ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* ) ) ) ) | ({...}? => ( ( ( ( rule__Keyword__AttributesAssignment_4_3 ) ) ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* ) ) ) ) | ({...}? => ( ( ( rule__Keyword__Group_4_4__0 ) ) ) ) )
            {
            // InternalPxManual.g:4738:3: ( ({...}? => ( ( ( rule__Keyword__Group_4_0__0 ) ) ) ) | ({...}? => ( ( ( rule__Keyword__Group_4_1__0 ) ) ) ) | ({...}? => ( ( ( ( rule__Keyword__ExamplesAssignment_4_2 ) ) ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* ) ) ) ) | ({...}? => ( ( ( ( rule__Keyword__AttributesAssignment_4_3 ) ) ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* ) ) ) ) | ({...}? => ( ( ( rule__Keyword__Group_4_4__0 ) ) ) ) )
            int alt49=5;
            int LA49_0 = input.LA(1);

            if ( LA49_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0) ) {
                alt49=1;
            }
            else if ( LA49_0 == 43 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1) ) {
                alt49=2;
            }
            else if ( ( LA49_0 == 36 || LA49_0 == 38 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2) ) {
                alt49=3;
            }
            else if ( LA49_0 == 45 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3) ) {
                alt49=4;
            }
            else if ( LA49_0 == 44 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4) ) {
                alt49=5;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 49, 0, input);

                throw nvae;
            }
            switch (alt49) {
                case 1 :
                    // InternalPxManual.g:4739:3: ({...}? => ( ( ( rule__Keyword__Group_4_0__0 ) ) ) )
                    {
                    // InternalPxManual.g:4739:3: ({...}? => ( ( ( rule__Keyword__Group_4_0__0 ) ) ) )
                    // InternalPxManual.g:4740:4: {...}? => ( ( ( rule__Keyword__Group_4_0__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Keyword__UnorderedGroup_4__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0)");
                    }
                    // InternalPxManual.g:4740:103: ( ( ( rule__Keyword__Group_4_0__0 ) ) )
                    // InternalPxManual.g:4741:5: ( ( rule__Keyword__Group_4_0__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0);
                    selected = true;
                    // InternalPxManual.g:4747:5: ( ( rule__Keyword__Group_4_0__0 ) )
                    // InternalPxManual.g:4748:6: ( rule__Keyword__Group_4_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAccess().getGroup_4_0()); 
                    }
                    // InternalPxManual.g:4749:6: ( rule__Keyword__Group_4_0__0 )
                    // InternalPxManual.g:4749:7: rule__Keyword__Group_4_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Keyword__Group_4_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAccess().getGroup_4_0()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:4754:3: ({...}? => ( ( ( rule__Keyword__Group_4_1__0 ) ) ) )
                    {
                    // InternalPxManual.g:4754:3: ({...}? => ( ( ( rule__Keyword__Group_4_1__0 ) ) ) )
                    // InternalPxManual.g:4755:4: {...}? => ( ( ( rule__Keyword__Group_4_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Keyword__UnorderedGroup_4__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1)");
                    }
                    // InternalPxManual.g:4755:103: ( ( ( rule__Keyword__Group_4_1__0 ) ) )
                    // InternalPxManual.g:4756:5: ( ( rule__Keyword__Group_4_1__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1);
                    selected = true;
                    // InternalPxManual.g:4762:5: ( ( rule__Keyword__Group_4_1__0 ) )
                    // InternalPxManual.g:4763:6: ( rule__Keyword__Group_4_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAccess().getGroup_4_1()); 
                    }
                    // InternalPxManual.g:4764:6: ( rule__Keyword__Group_4_1__0 )
                    // InternalPxManual.g:4764:7: rule__Keyword__Group_4_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Keyword__Group_4_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAccess().getGroup_4_1()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPxManual.g:4769:3: ({...}? => ( ( ( ( rule__Keyword__ExamplesAssignment_4_2 ) ) ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* ) ) ) )
                    {
                    // InternalPxManual.g:4769:3: ({...}? => ( ( ( ( rule__Keyword__ExamplesAssignment_4_2 ) ) ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* ) ) ) )
                    // InternalPxManual.g:4770:4: {...}? => ( ( ( ( rule__Keyword__ExamplesAssignment_4_2 ) ) ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Keyword__UnorderedGroup_4__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2)");
                    }
                    // InternalPxManual.g:4770:103: ( ( ( ( rule__Keyword__ExamplesAssignment_4_2 ) ) ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* ) ) )
                    // InternalPxManual.g:4771:5: ( ( ( rule__Keyword__ExamplesAssignment_4_2 ) ) ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2);
                    selected = true;
                    // InternalPxManual.g:4777:5: ( ( ( rule__Keyword__ExamplesAssignment_4_2 ) ) ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* ) )
                    // InternalPxManual.g:4778:6: ( ( rule__Keyword__ExamplesAssignment_4_2 ) ) ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* )
                    {
                    // InternalPxManual.g:4778:6: ( ( rule__Keyword__ExamplesAssignment_4_2 ) )
                    // InternalPxManual.g:4779:7: ( rule__Keyword__ExamplesAssignment_4_2 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAccess().getExamplesAssignment_4_2()); 
                    }
                    // InternalPxManual.g:4780:7: ( rule__Keyword__ExamplesAssignment_4_2 )
                    // InternalPxManual.g:4780:8: rule__Keyword__ExamplesAssignment_4_2
                    {
                    pushFollow(FOLLOW_39);
                    rule__Keyword__ExamplesAssignment_4_2();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAccess().getExamplesAssignment_4_2()); 
                    }

                    }

                    // InternalPxManual.g:4783:6: ( ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )* )
                    // InternalPxManual.g:4784:7: ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )*
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAccess().getExamplesAssignment_4_2()); 
                    }
                    // InternalPxManual.g:4785:7: ( ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2 )*
                    loop47:
                    do {
                        int alt47=2;
                        int LA47_0 = input.LA(1);

                        if ( (LA47_0==36) ) {
                            int LA47_3 = input.LA(2);

                            if ( (synpred1_InternalPxManual()) ) {
                                alt47=1;
                            }


                        }
                        else if ( (LA47_0==38) ) {
                            int LA47_4 = input.LA(2);

                            if ( (synpred1_InternalPxManual()) ) {
                                alt47=1;
                            }


                        }


                        switch (alt47) {
                    	case 1 :
                    	    // InternalPxManual.g:4785:8: ( rule__Keyword__ExamplesAssignment_4_2 )=> rule__Keyword__ExamplesAssignment_4_2
                    	    {
                    	    pushFollow(FOLLOW_39);
                    	    rule__Keyword__ExamplesAssignment_4_2();

                    	    state._fsp--;
                    	    if (state.failed) return ;

                    	    }
                    	    break;

                    	default :
                    	    break loop47;
                        }
                    } while (true);

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAccess().getExamplesAssignment_4_2()); 
                    }

                    }


                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalPxManual.g:4791:3: ({...}? => ( ( ( ( rule__Keyword__AttributesAssignment_4_3 ) ) ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* ) ) ) )
                    {
                    // InternalPxManual.g:4791:3: ({...}? => ( ( ( ( rule__Keyword__AttributesAssignment_4_3 ) ) ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* ) ) ) )
                    // InternalPxManual.g:4792:4: {...}? => ( ( ( ( rule__Keyword__AttributesAssignment_4_3 ) ) ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Keyword__UnorderedGroup_4__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3)");
                    }
                    // InternalPxManual.g:4792:103: ( ( ( ( rule__Keyword__AttributesAssignment_4_3 ) ) ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* ) ) )
                    // InternalPxManual.g:4793:5: ( ( ( rule__Keyword__AttributesAssignment_4_3 ) ) ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3);
                    selected = true;
                    // InternalPxManual.g:4799:5: ( ( ( rule__Keyword__AttributesAssignment_4_3 ) ) ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* ) )
                    // InternalPxManual.g:4800:6: ( ( rule__Keyword__AttributesAssignment_4_3 ) ) ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* )
                    {
                    // InternalPxManual.g:4800:6: ( ( rule__Keyword__AttributesAssignment_4_3 ) )
                    // InternalPxManual.g:4801:7: ( rule__Keyword__AttributesAssignment_4_3 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAccess().getAttributesAssignment_4_3()); 
                    }
                    // InternalPxManual.g:4802:7: ( rule__Keyword__AttributesAssignment_4_3 )
                    // InternalPxManual.g:4802:8: rule__Keyword__AttributesAssignment_4_3
                    {
                    pushFollow(FOLLOW_40);
                    rule__Keyword__AttributesAssignment_4_3();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAccess().getAttributesAssignment_4_3()); 
                    }

                    }

                    // InternalPxManual.g:4805:6: ( ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )* )
                    // InternalPxManual.g:4806:7: ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )*
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAccess().getAttributesAssignment_4_3()); 
                    }
                    // InternalPxManual.g:4807:7: ( ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3 )*
                    loop48:
                    do {
                        int alt48=2;
                        int LA48_0 = input.LA(1);

                        if ( (LA48_0==45) ) {
                            int LA48_5 = input.LA(2);

                            if ( (synpred2_InternalPxManual()) ) {
                                alt48=1;
                            }


                        }


                        switch (alt48) {
                    	case 1 :
                    	    // InternalPxManual.g:4807:8: ( rule__Keyword__AttributesAssignment_4_3 )=> rule__Keyword__AttributesAssignment_4_3
                    	    {
                    	    pushFollow(FOLLOW_40);
                    	    rule__Keyword__AttributesAssignment_4_3();

                    	    state._fsp--;
                    	    if (state.failed) return ;

                    	    }
                    	    break;

                    	default :
                    	    break loop48;
                        }
                    } while (true);

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAccess().getAttributesAssignment_4_3()); 
                    }

                    }


                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalPxManual.g:4813:3: ({...}? => ( ( ( rule__Keyword__Group_4_4__0 ) ) ) )
                    {
                    // InternalPxManual.g:4813:3: ({...}? => ( ( ( rule__Keyword__Group_4_4__0 ) ) ) )
                    // InternalPxManual.g:4814:4: {...}? => ( ( ( rule__Keyword__Group_4_4__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__Keyword__UnorderedGroup_4__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4)");
                    }
                    // InternalPxManual.g:4814:103: ( ( ( rule__Keyword__Group_4_4__0 ) ) )
                    // InternalPxManual.g:4815:5: ( ( rule__Keyword__Group_4_4__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4);
                    selected = true;
                    // InternalPxManual.g:4821:5: ( ( rule__Keyword__Group_4_4__0 ) )
                    // InternalPxManual.g:4822:6: ( rule__Keyword__Group_4_4__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAccess().getGroup_4_4()); 
                    }
                    // InternalPxManual.g:4823:6: ( rule__Keyword__Group_4_4__0 )
                    // InternalPxManual.g:4823:7: rule__Keyword__Group_4_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Keyword__Group_4_4__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAccess().getGroup_4_4()); 
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getKeywordAccess().getUnorderedGroup_4());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__UnorderedGroup_4__Impl"


    // $ANTLR start "rule__Keyword__UnorderedGroup_4__0"
    // InternalPxManual.g:4836:1: rule__Keyword__UnorderedGroup_4__0 : rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__1 )? ;
    public final void rule__Keyword__UnorderedGroup_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4840:1: ( rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__1 )? )
            // InternalPxManual.g:4841:2: rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__1 )?
            {
            pushFollow(FOLLOW_41);
            rule__Keyword__UnorderedGroup_4__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4842:2: ( rule__Keyword__UnorderedGroup_4__1 )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( LA50_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0) ) {
                alt50=1;
            }
            else if ( LA50_0 == 43 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1) ) {
                alt50=1;
            }
            else if ( ( LA50_0 == 36 || LA50_0 == 38 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2) ) {
                alt50=1;
            }
            else if ( LA50_0 == 45 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3) ) {
                alt50=1;
            }
            else if ( LA50_0 == 44 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // InternalPxManual.g:4842:2: rule__Keyword__UnorderedGroup_4__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Keyword__UnorderedGroup_4__1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__UnorderedGroup_4__0"


    // $ANTLR start "rule__Keyword__UnorderedGroup_4__1"
    // InternalPxManual.g:4848:1: rule__Keyword__UnorderedGroup_4__1 : rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__2 )? ;
    public final void rule__Keyword__UnorderedGroup_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4852:1: ( rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__2 )? )
            // InternalPxManual.g:4853:2: rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__2 )?
            {
            pushFollow(FOLLOW_41);
            rule__Keyword__UnorderedGroup_4__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4854:2: ( rule__Keyword__UnorderedGroup_4__2 )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( LA51_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0) ) {
                alt51=1;
            }
            else if ( LA51_0 == 43 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1) ) {
                alt51=1;
            }
            else if ( ( LA51_0 == 36 || LA51_0 == 38 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2) ) {
                alt51=1;
            }
            else if ( LA51_0 == 45 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3) ) {
                alt51=1;
            }
            else if ( LA51_0 == 44 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // InternalPxManual.g:4854:2: rule__Keyword__UnorderedGroup_4__2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Keyword__UnorderedGroup_4__2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__UnorderedGroup_4__1"


    // $ANTLR start "rule__Keyword__UnorderedGroup_4__2"
    // InternalPxManual.g:4860:1: rule__Keyword__UnorderedGroup_4__2 : rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__3 )? ;
    public final void rule__Keyword__UnorderedGroup_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4864:1: ( rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__3 )? )
            // InternalPxManual.g:4865:2: rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__3 )?
            {
            pushFollow(FOLLOW_41);
            rule__Keyword__UnorderedGroup_4__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4866:2: ( rule__Keyword__UnorderedGroup_4__3 )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( LA52_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0) ) {
                alt52=1;
            }
            else if ( LA52_0 == 43 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1) ) {
                alt52=1;
            }
            else if ( ( LA52_0 == 36 || LA52_0 == 38 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2) ) {
                alt52=1;
            }
            else if ( LA52_0 == 45 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3) ) {
                alt52=1;
            }
            else if ( LA52_0 == 44 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalPxManual.g:4866:2: rule__Keyword__UnorderedGroup_4__3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Keyword__UnorderedGroup_4__3();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__UnorderedGroup_4__2"


    // $ANTLR start "rule__Keyword__UnorderedGroup_4__3"
    // InternalPxManual.g:4872:1: rule__Keyword__UnorderedGroup_4__3 : rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__4 )? ;
    public final void rule__Keyword__UnorderedGroup_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4876:1: ( rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__4 )? )
            // InternalPxManual.g:4877:2: rule__Keyword__UnorderedGroup_4__Impl ( rule__Keyword__UnorderedGroup_4__4 )?
            {
            pushFollow(FOLLOW_41);
            rule__Keyword__UnorderedGroup_4__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4878:2: ( rule__Keyword__UnorderedGroup_4__4 )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( LA53_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0) ) {
                alt53=1;
            }
            else if ( LA53_0 == 43 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1) ) {
                alt53=1;
            }
            else if ( ( LA53_0 == 36 || LA53_0 == 38 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2) ) {
                alt53=1;
            }
            else if ( LA53_0 == 45 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3) ) {
                alt53=1;
            }
            else if ( LA53_0 == 44 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // InternalPxManual.g:4878:2: rule__Keyword__UnorderedGroup_4__4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Keyword__UnorderedGroup_4__4();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__UnorderedGroup_4__3"


    // $ANTLR start "rule__Keyword__UnorderedGroup_4__4"
    // InternalPxManual.g:4884:1: rule__Keyword__UnorderedGroup_4__4 : rule__Keyword__UnorderedGroup_4__Impl ;
    public final void rule__Keyword__UnorderedGroup_4__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4888:1: ( rule__Keyword__UnorderedGroup_4__Impl )
            // InternalPxManual.g:4889:2: rule__Keyword__UnorderedGroup_4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Keyword__UnorderedGroup_4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__UnorderedGroup_4__4"


    // $ANTLR start "rule__KeywordAttribute__UnorderedGroup_3"
    // InternalPxManual.g:4896:1: rule__KeywordAttribute__UnorderedGroup_3 : ( rule__KeywordAttribute__UnorderedGroup_3__0 )? ;
    public final void rule__KeywordAttribute__UnorderedGroup_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3());
        	
        try {
            // InternalPxManual.g:4901:1: ( ( rule__KeywordAttribute__UnorderedGroup_3__0 )? )
            // InternalPxManual.g:4902:2: ( rule__KeywordAttribute__UnorderedGroup_3__0 )?
            {
            // InternalPxManual.g:4902:2: ( rule__KeywordAttribute__UnorderedGroup_3__0 )?
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( LA54_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0) ) {
                alt54=1;
            }
            else if ( ( LA54_0 == 36 || LA54_0 == 38 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1) ) {
                alt54=1;
            }
            else if ( LA54_0 == 47 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2) ) {
                alt54=1;
            }
            else if ( LA54_0 == 46 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3) ) {
                alt54=1;
            }
            switch (alt54) {
                case 1 :
                    // InternalPxManual.g:4902:2: rule__KeywordAttribute__UnorderedGroup_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__KeywordAttribute__UnorderedGroup_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__UnorderedGroup_3"


    // $ANTLR start "rule__KeywordAttribute__UnorderedGroup_3__Impl"
    // InternalPxManual.g:4910:1: rule__KeywordAttribute__UnorderedGroup_3__Impl : ( ({...}? => ( ( ( rule__KeywordAttribute__Group_3_0__0 ) ) ) ) | ({...}? => ( ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) ) ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* ) ) ) ) | ({...}? => ( ( ( rule__KeywordAttribute__OptionalAssignment_3_2 ) ) ) ) | ({...}? => ( ( ( rule__KeywordAttribute__Group_3_3__0 ) ) ) ) ) ;
    public final void rule__KeywordAttribute__UnorderedGroup_3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
        	
        try {
            // InternalPxManual.g:4915:1: ( ( ({...}? => ( ( ( rule__KeywordAttribute__Group_3_0__0 ) ) ) ) | ({...}? => ( ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) ) ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* ) ) ) ) | ({...}? => ( ( ( rule__KeywordAttribute__OptionalAssignment_3_2 ) ) ) ) | ({...}? => ( ( ( rule__KeywordAttribute__Group_3_3__0 ) ) ) ) ) )
            // InternalPxManual.g:4916:3: ( ({...}? => ( ( ( rule__KeywordAttribute__Group_3_0__0 ) ) ) ) | ({...}? => ( ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) ) ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* ) ) ) ) | ({...}? => ( ( ( rule__KeywordAttribute__OptionalAssignment_3_2 ) ) ) ) | ({...}? => ( ( ( rule__KeywordAttribute__Group_3_3__0 ) ) ) ) )
            {
            // InternalPxManual.g:4916:3: ( ({...}? => ( ( ( rule__KeywordAttribute__Group_3_0__0 ) ) ) ) | ({...}? => ( ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) ) ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* ) ) ) ) | ({...}? => ( ( ( rule__KeywordAttribute__OptionalAssignment_3_2 ) ) ) ) | ({...}? => ( ( ( rule__KeywordAttribute__Group_3_3__0 ) ) ) ) )
            int alt56=4;
            int LA56_0 = input.LA(1);

            if ( LA56_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0) ) {
                alt56=1;
            }
            else if ( ( LA56_0 == 36 || LA56_0 == 38 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1) ) {
                alt56=2;
            }
            else if ( LA56_0 == 47 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2) ) {
                alt56=3;
            }
            else if ( LA56_0 == 46 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3) ) {
                alt56=4;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 56, 0, input);

                throw nvae;
            }
            switch (alt56) {
                case 1 :
                    // InternalPxManual.g:4917:3: ({...}? => ( ( ( rule__KeywordAttribute__Group_3_0__0 ) ) ) )
                    {
                    // InternalPxManual.g:4917:3: ({...}? => ( ( ( rule__KeywordAttribute__Group_3_0__0 ) ) ) )
                    // InternalPxManual.g:4918:4: {...}? => ( ( ( rule__KeywordAttribute__Group_3_0__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__KeywordAttribute__UnorderedGroup_3__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0)");
                    }
                    // InternalPxManual.g:4918:112: ( ( ( rule__KeywordAttribute__Group_3_0__0 ) ) )
                    // InternalPxManual.g:4919:5: ( ( rule__KeywordAttribute__Group_3_0__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0);
                    selected = true;
                    // InternalPxManual.g:4925:5: ( ( rule__KeywordAttribute__Group_3_0__0 ) )
                    // InternalPxManual.g:4926:6: ( rule__KeywordAttribute__Group_3_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAttributeAccess().getGroup_3_0()); 
                    }
                    // InternalPxManual.g:4927:6: ( rule__KeywordAttribute__Group_3_0__0 )
                    // InternalPxManual.g:4927:7: rule__KeywordAttribute__Group_3_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__KeywordAttribute__Group_3_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAttributeAccess().getGroup_3_0()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:4932:3: ({...}? => ( ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) ) ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* ) ) ) )
                    {
                    // InternalPxManual.g:4932:3: ({...}? => ( ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) ) ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* ) ) ) )
                    // InternalPxManual.g:4933:4: {...}? => ( ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) ) ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__KeywordAttribute__UnorderedGroup_3__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1)");
                    }
                    // InternalPxManual.g:4933:112: ( ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) ) ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* ) ) )
                    // InternalPxManual.g:4934:5: ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) ) ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1);
                    selected = true;
                    // InternalPxManual.g:4940:5: ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) ) ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* ) )
                    // InternalPxManual.g:4941:6: ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) ) ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* )
                    {
                    // InternalPxManual.g:4941:6: ( ( rule__KeywordAttribute__ExampleAssignment_3_1 ) )
                    // InternalPxManual.g:4942:7: ( rule__KeywordAttribute__ExampleAssignment_3_1 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAttributeAccess().getExampleAssignment_3_1()); 
                    }
                    // InternalPxManual.g:4943:7: ( rule__KeywordAttribute__ExampleAssignment_3_1 )
                    // InternalPxManual.g:4943:8: rule__KeywordAttribute__ExampleAssignment_3_1
                    {
                    pushFollow(FOLLOW_39);
                    rule__KeywordAttribute__ExampleAssignment_3_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAttributeAccess().getExampleAssignment_3_1()); 
                    }

                    }

                    // InternalPxManual.g:4946:6: ( ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )* )
                    // InternalPxManual.g:4947:7: ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )*
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAttributeAccess().getExampleAssignment_3_1()); 
                    }
                    // InternalPxManual.g:4948:7: ( ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1 )*
                    loop55:
                    do {
                        int alt55=2;
                        int LA55_0 = input.LA(1);

                        if ( (LA55_0==36) ) {
                            int LA55_2 = input.LA(2);

                            if ( (synpred3_InternalPxManual()) ) {
                                alt55=1;
                            }


                        }
                        else if ( (LA55_0==38) ) {
                            int LA55_3 = input.LA(2);

                            if ( (synpred3_InternalPxManual()) ) {
                                alt55=1;
                            }


                        }


                        switch (alt55) {
                    	case 1 :
                    	    // InternalPxManual.g:4948:8: ( rule__KeywordAttribute__ExampleAssignment_3_1 )=> rule__KeywordAttribute__ExampleAssignment_3_1
                    	    {
                    	    pushFollow(FOLLOW_39);
                    	    rule__KeywordAttribute__ExampleAssignment_3_1();

                    	    state._fsp--;
                    	    if (state.failed) return ;

                    	    }
                    	    break;

                    	default :
                    	    break loop55;
                        }
                    } while (true);

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAttributeAccess().getExampleAssignment_3_1()); 
                    }

                    }


                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPxManual.g:4954:3: ({...}? => ( ( ( rule__KeywordAttribute__OptionalAssignment_3_2 ) ) ) )
                    {
                    // InternalPxManual.g:4954:3: ({...}? => ( ( ( rule__KeywordAttribute__OptionalAssignment_3_2 ) ) ) )
                    // InternalPxManual.g:4955:4: {...}? => ( ( ( rule__KeywordAttribute__OptionalAssignment_3_2 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__KeywordAttribute__UnorderedGroup_3__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2)");
                    }
                    // InternalPxManual.g:4955:112: ( ( ( rule__KeywordAttribute__OptionalAssignment_3_2 ) ) )
                    // InternalPxManual.g:4956:5: ( ( rule__KeywordAttribute__OptionalAssignment_3_2 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2);
                    selected = true;
                    // InternalPxManual.g:4962:5: ( ( rule__KeywordAttribute__OptionalAssignment_3_2 ) )
                    // InternalPxManual.g:4963:6: ( rule__KeywordAttribute__OptionalAssignment_3_2 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAttributeAccess().getOptionalAssignment_3_2()); 
                    }
                    // InternalPxManual.g:4964:6: ( rule__KeywordAttribute__OptionalAssignment_3_2 )
                    // InternalPxManual.g:4964:7: rule__KeywordAttribute__OptionalAssignment_3_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__KeywordAttribute__OptionalAssignment_3_2();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAttributeAccess().getOptionalAssignment_3_2()); 
                    }

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalPxManual.g:4969:3: ({...}? => ( ( ( rule__KeywordAttribute__Group_3_3__0 ) ) ) )
                    {
                    // InternalPxManual.g:4969:3: ({...}? => ( ( ( rule__KeywordAttribute__Group_3_3__0 ) ) ) )
                    // InternalPxManual.g:4970:4: {...}? => ( ( ( rule__KeywordAttribute__Group_3_3__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3) ) {
                        if (state.backtracking>0) {state.failed=true; return ;}
                        throw new FailedPredicateException(input, "rule__KeywordAttribute__UnorderedGroup_3__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3)");
                    }
                    // InternalPxManual.g:4970:112: ( ( ( rule__KeywordAttribute__Group_3_3__0 ) ) )
                    // InternalPxManual.g:4971:5: ( ( rule__KeywordAttribute__Group_3_3__0 ) )
                    {
                    getUnorderedGroupHelper().select(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3);
                    selected = true;
                    // InternalPxManual.g:4977:5: ( ( rule__KeywordAttribute__Group_3_3__0 ) )
                    // InternalPxManual.g:4978:6: ( rule__KeywordAttribute__Group_3_3__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getKeywordAttributeAccess().getGroup_3_3()); 
                    }
                    // InternalPxManual.g:4979:6: ( rule__KeywordAttribute__Group_3_3__0 )
                    // InternalPxManual.g:4979:7: rule__KeywordAttribute__Group_3_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__KeywordAttribute__Group_3_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getKeywordAttributeAccess().getGroup_3_3()); 
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__UnorderedGroup_3__Impl"


    // $ANTLR start "rule__KeywordAttribute__UnorderedGroup_3__0"
    // InternalPxManual.g:4992:1: rule__KeywordAttribute__UnorderedGroup_3__0 : rule__KeywordAttribute__UnorderedGroup_3__Impl ( rule__KeywordAttribute__UnorderedGroup_3__1 )? ;
    public final void rule__KeywordAttribute__UnorderedGroup_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:4996:1: ( rule__KeywordAttribute__UnorderedGroup_3__Impl ( rule__KeywordAttribute__UnorderedGroup_3__1 )? )
            // InternalPxManual.g:4997:2: rule__KeywordAttribute__UnorderedGroup_3__Impl ( rule__KeywordAttribute__UnorderedGroup_3__1 )?
            {
            pushFollow(FOLLOW_42);
            rule__KeywordAttribute__UnorderedGroup_3__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:4998:2: ( rule__KeywordAttribute__UnorderedGroup_3__1 )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( LA57_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0) ) {
                alt57=1;
            }
            else if ( ( LA57_0 == 36 || LA57_0 == 38 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1) ) {
                alt57=1;
            }
            else if ( LA57_0 == 47 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2) ) {
                alt57=1;
            }
            else if ( LA57_0 == 46 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalPxManual.g:4998:2: rule__KeywordAttribute__UnorderedGroup_3__1
                    {
                    pushFollow(FOLLOW_2);
                    rule__KeywordAttribute__UnorderedGroup_3__1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__UnorderedGroup_3__0"


    // $ANTLR start "rule__KeywordAttribute__UnorderedGroup_3__1"
    // InternalPxManual.g:5004:1: rule__KeywordAttribute__UnorderedGroup_3__1 : rule__KeywordAttribute__UnorderedGroup_3__Impl ( rule__KeywordAttribute__UnorderedGroup_3__2 )? ;
    public final void rule__KeywordAttribute__UnorderedGroup_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5008:1: ( rule__KeywordAttribute__UnorderedGroup_3__Impl ( rule__KeywordAttribute__UnorderedGroup_3__2 )? )
            // InternalPxManual.g:5009:2: rule__KeywordAttribute__UnorderedGroup_3__Impl ( rule__KeywordAttribute__UnorderedGroup_3__2 )?
            {
            pushFollow(FOLLOW_42);
            rule__KeywordAttribute__UnorderedGroup_3__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:5010:2: ( rule__KeywordAttribute__UnorderedGroup_3__2 )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( LA58_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0) ) {
                alt58=1;
            }
            else if ( ( LA58_0 == 36 || LA58_0 == 38 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1) ) {
                alt58=1;
            }
            else if ( LA58_0 == 47 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2) ) {
                alt58=1;
            }
            else if ( LA58_0 == 46 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // InternalPxManual.g:5010:2: rule__KeywordAttribute__UnorderedGroup_3__2
                    {
                    pushFollow(FOLLOW_2);
                    rule__KeywordAttribute__UnorderedGroup_3__2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__UnorderedGroup_3__1"


    // $ANTLR start "rule__KeywordAttribute__UnorderedGroup_3__2"
    // InternalPxManual.g:5016:1: rule__KeywordAttribute__UnorderedGroup_3__2 : rule__KeywordAttribute__UnorderedGroup_3__Impl ( rule__KeywordAttribute__UnorderedGroup_3__3 )? ;
    public final void rule__KeywordAttribute__UnorderedGroup_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5020:1: ( rule__KeywordAttribute__UnorderedGroup_3__Impl ( rule__KeywordAttribute__UnorderedGroup_3__3 )? )
            // InternalPxManual.g:5021:2: rule__KeywordAttribute__UnorderedGroup_3__Impl ( rule__KeywordAttribute__UnorderedGroup_3__3 )?
            {
            pushFollow(FOLLOW_42);
            rule__KeywordAttribute__UnorderedGroup_3__Impl();

            state._fsp--;
            if (state.failed) return ;
            // InternalPxManual.g:5022:2: ( rule__KeywordAttribute__UnorderedGroup_3__3 )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( LA59_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0) ) {
                alt59=1;
            }
            else if ( ( LA59_0 == 36 || LA59_0 == 38 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1) ) {
                alt59=1;
            }
            else if ( LA59_0 == 47 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2) ) {
                alt59=1;
            }
            else if ( LA59_0 == 46 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // InternalPxManual.g:5022:2: rule__KeywordAttribute__UnorderedGroup_3__3
                    {
                    pushFollow(FOLLOW_2);
                    rule__KeywordAttribute__UnorderedGroup_3__3();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__UnorderedGroup_3__2"


    // $ANTLR start "rule__KeywordAttribute__UnorderedGroup_3__3"
    // InternalPxManual.g:5028:1: rule__KeywordAttribute__UnorderedGroup_3__3 : rule__KeywordAttribute__UnorderedGroup_3__Impl ;
    public final void rule__KeywordAttribute__UnorderedGroup_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5032:1: ( rule__KeywordAttribute__UnorderedGroup_3__Impl )
            // InternalPxManual.g:5033:2: rule__KeywordAttribute__UnorderedGroup_3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__KeywordAttribute__UnorderedGroup_3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__UnorderedGroup_3__3"


    // $ANTLR start "rule__Document__TitleAssignment_2_0"
    // InternalPxManual.g:5040:1: rule__Document__TitleAssignment_2_0 : ( ruleText ) ;
    public final void rule__Document__TitleAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5044:1: ( ( ruleText ) )
            // InternalPxManual.g:5045:2: ( ruleText )
            {
            // InternalPxManual.g:5045:2: ( ruleText )
            // InternalPxManual.g:5046:3: ruleText
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getTitleTextParserRuleCall_2_0_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleText();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getTitleTextParserRuleCall_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__TitleAssignment_2_0"


    // $ANTLR start "rule__Document__IdAssignment_2_1_1"
    // InternalPxManual.g:5055:1: rule__Document__IdAssignment_2_1_1 : ( RULE_STRING ) ;
    public final void rule__Document__IdAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5059:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5060:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5060:2: ( RULE_STRING )
            // InternalPxManual.g:5061:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getIdSTRINGTerminalRuleCall_2_1_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getIdSTRINGTerminalRuleCall_2_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__IdAssignment_2_1_1"


    // $ANTLR start "rule__Document__FirstHeadingLevelAssignment_2_2_1"
    // InternalPxManual.g:5070:1: rule__Document__FirstHeadingLevelAssignment_2_2_1 : ( RULE_INT ) ;
    public final void rule__Document__FirstHeadingLevelAssignment_2_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5074:1: ( ( RULE_INT ) )
            // InternalPxManual.g:5075:2: ( RULE_INT )
            {
            // InternalPxManual.g:5075:2: ( RULE_INT )
            // InternalPxManual.g:5076:3: RULE_INT
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getFirstHeadingLevelINTTerminalRuleCall_2_2_1_0()); 
            }
            match(input,RULE_INT,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getFirstHeadingLevelINTTerminalRuleCall_2_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__FirstHeadingLevelAssignment_2_2_1"


    // $ANTLR start "rule__Document__BodyAssignment_3"
    // InternalPxManual.g:5085:1: rule__Document__BodyAssignment_3 : ( ruleDocBody ) ;
    public final void rule__Document__BodyAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5089:1: ( ( ruleDocBody ) )
            // InternalPxManual.g:5090:2: ( ruleDocBody )
            {
            // InternalPxManual.g:5090:2: ( ruleDocBody )
            // InternalPxManual.g:5091:3: ruleDocBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getBodyDocBodyParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDocBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getBodyDocBodyParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__BodyAssignment_3"


    // $ANTLR start "rule__DocBody__ElementsAssignment_2"
    // InternalPxManual.g:5100:1: rule__DocBody__ElementsAssignment_2 : ( ruleManualElement ) ;
    public final void rule__DocBody__ElementsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5104:1: ( ( ruleManualElement ) )
            // InternalPxManual.g:5105:2: ( ruleManualElement )
            {
            // InternalPxManual.g:5105:2: ( ruleManualElement )
            // InternalPxManual.g:5106:3: ruleManualElement
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocBodyAccess().getElementsManualElementParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleManualElement();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocBodyAccess().getElementsManualElementParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocBody__ElementsAssignment_2"


    // $ANTLR start "rule__Toc__TitleAssignment_2_0"
    // InternalPxManual.g:5115:1: rule__Toc__TitleAssignment_2_0 : ( ruleText ) ;
    public final void rule__Toc__TitleAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5119:1: ( ( ruleText ) )
            // InternalPxManual.g:5120:2: ( ruleText )
            {
            // InternalPxManual.g:5120:2: ( ruleText )
            // InternalPxManual.g:5121:3: ruleText
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTocAccess().getTitleTextParserRuleCall_2_0_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleText();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTocAccess().getTitleTextParserRuleCall_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__TitleAssignment_2_0"


    // $ANTLR start "rule__Toc__MaxLevelAssignment_2_1_1"
    // InternalPxManual.g:5130:1: rule__Toc__MaxLevelAssignment_2_1_1 : ( RULE_INT ) ;
    public final void rule__Toc__MaxLevelAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5134:1: ( ( RULE_INT ) )
            // InternalPxManual.g:5135:2: ( RULE_INT )
            {
            // InternalPxManual.g:5135:2: ( RULE_INT )
            // InternalPxManual.g:5136:3: RULE_INT
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTocAccess().getMaxLevelINTTerminalRuleCall_2_1_1_0()); 
            }
            match(input,RULE_INT,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTocAccess().getMaxLevelINTTerminalRuleCall_2_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Toc__MaxLevelAssignment_2_1_1"


    // $ANTLR start "rule__List__ItemsAssignment_3"
    // InternalPxManual.g:5145:1: rule__List__ItemsAssignment_3 : ( ruleListItem ) ;
    public final void rule__List__ItemsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5149:1: ( ( ruleListItem ) )
            // InternalPxManual.g:5150:2: ( ruleListItem )
            {
            // InternalPxManual.g:5150:2: ( ruleListItem )
            // InternalPxManual.g:5151:3: ruleListItem
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListAccess().getItemsListItemParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleListItem();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getListAccess().getItemsListItemParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__ItemsAssignment_3"


    // $ANTLR start "rule__ListItem__BodyAssignment_1"
    // InternalPxManual.g:5160:1: rule__ListItem__BodyAssignment_1 : ( ruleDocBody ) ;
    public final void rule__ListItem__BodyAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5164:1: ( ( ruleDocBody ) )
            // InternalPxManual.g:5165:2: ( ruleDocBody )
            {
            // InternalPxManual.g:5165:2: ( ruleDocBody )
            // InternalPxManual.g:5166:3: ruleDocBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getListItemAccess().getBodyDocBodyParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDocBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getListItemAccess().getBodyDocBodyParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListItem__BodyAssignment_1"


    // $ANTLR start "rule__SamePage__BodyAssignment_1"
    // InternalPxManual.g:5175:1: rule__SamePage__BodyAssignment_1 : ( ruleDocBody ) ;
    public final void rule__SamePage__BodyAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5179:1: ( ( ruleDocBody ) )
            // InternalPxManual.g:5180:2: ( ruleDocBody )
            {
            // InternalPxManual.g:5180:2: ( ruleDocBody )
            // InternalPxManual.g:5181:3: ruleDocBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSamePageAccess().getBodyDocBodyParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDocBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSamePageAccess().getBodyDocBodyParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SamePage__BodyAssignment_1"


    // $ANTLR start "rule__Table__StyleAssignment_2_0_1"
    // InternalPxManual.g:5190:1: rule__Table__StyleAssignment_2_0_1 : ( RULE_STRING ) ;
    public final void rule__Table__StyleAssignment_2_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5194:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5195:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5195:2: ( RULE_STRING )
            // InternalPxManual.g:5196:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getStyleSTRINGTerminalRuleCall_2_0_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getStyleSTRINGTerminalRuleCall_2_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__StyleAssignment_2_0_1"


    // $ANTLR start "rule__Table__ClassNameAssignment_2_1_1"
    // InternalPxManual.g:5205:1: rule__Table__ClassNameAssignment_2_1_1 : ( RULE_STRING ) ;
    public final void rule__Table__ClassNameAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5209:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5210:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5210:2: ( RULE_STRING )
            // InternalPxManual.g:5211:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getClassNameSTRINGTerminalRuleCall_2_1_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getClassNameSTRINGTerminalRuleCall_2_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__ClassNameAssignment_2_1_1"


    // $ANTLR start "rule__Table__RowsAssignment_4"
    // InternalPxManual.g:5220:1: rule__Table__RowsAssignment_4 : ( ruleRow ) ;
    public final void rule__Table__RowsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5224:1: ( ( ruleRow ) )
            // InternalPxManual.g:5225:2: ( ruleRow )
            {
            // InternalPxManual.g:5225:2: ( ruleRow )
            // InternalPxManual.g:5226:3: ruleRow
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTableAccess().getRowsRowParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleRow();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTableAccess().getRowsRowParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__RowsAssignment_4"


    // $ANTLR start "rule__Row__StyleAssignment_2_0_1"
    // InternalPxManual.g:5235:1: rule__Row__StyleAssignment_2_0_1 : ( RULE_STRING ) ;
    public final void rule__Row__StyleAssignment_2_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5239:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5240:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5240:2: ( RULE_STRING )
            // InternalPxManual.g:5241:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getStyleSTRINGTerminalRuleCall_2_0_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getStyleSTRINGTerminalRuleCall_2_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__StyleAssignment_2_0_1"


    // $ANTLR start "rule__Row__ClassNameAssignment_2_1_1"
    // InternalPxManual.g:5250:1: rule__Row__ClassNameAssignment_2_1_1 : ( RULE_STRING ) ;
    public final void rule__Row__ClassNameAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5254:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5255:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5255:2: ( RULE_STRING )
            // InternalPxManual.g:5256:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getClassNameSTRINGTerminalRuleCall_2_1_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getClassNameSTRINGTerminalRuleCall_2_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__ClassNameAssignment_2_1_1"


    // $ANTLR start "rule__Row__CellsAssignment_4"
    // InternalPxManual.g:5265:1: rule__Row__CellsAssignment_4 : ( ruleCell ) ;
    public final void rule__Row__CellsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5269:1: ( ( ruleCell ) )
            // InternalPxManual.g:5270:2: ( ruleCell )
            {
            // InternalPxManual.g:5270:2: ( ruleCell )
            // InternalPxManual.g:5271:3: ruleCell
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRowAccess().getCellsCellParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleCell();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRowAccess().getCellsCellParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__CellsAssignment_4"


    // $ANTLR start "rule__Cell__StyleAssignment_1_0_1"
    // InternalPxManual.g:5280:1: rule__Cell__StyleAssignment_1_0_1 : ( RULE_STRING ) ;
    public final void rule__Cell__StyleAssignment_1_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5284:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5285:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5285:2: ( RULE_STRING )
            // InternalPxManual.g:5286:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getStyleSTRINGTerminalRuleCall_1_0_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getStyleSTRINGTerminalRuleCall_1_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__StyleAssignment_1_0_1"


    // $ANTLR start "rule__Cell__ClassNameAssignment_1_1_1"
    // InternalPxManual.g:5295:1: rule__Cell__ClassNameAssignment_1_1_1 : ( RULE_STRING ) ;
    public final void rule__Cell__ClassNameAssignment_1_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5299:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5300:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5300:2: ( RULE_STRING )
            // InternalPxManual.g:5301:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getClassNameSTRINGTerminalRuleCall_1_1_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getClassNameSTRINGTerminalRuleCall_1_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__ClassNameAssignment_1_1_1"


    // $ANTLR start "rule__Cell__SpanAssignment_1_2_1"
    // InternalPxManual.g:5310:1: rule__Cell__SpanAssignment_1_2_1 : ( RULE_INT ) ;
    public final void rule__Cell__SpanAssignment_1_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5314:1: ( ( RULE_INT ) )
            // InternalPxManual.g:5315:2: ( RULE_INT )
            {
            // InternalPxManual.g:5315:2: ( RULE_INT )
            // InternalPxManual.g:5316:3: RULE_INT
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getSpanINTTerminalRuleCall_1_2_1_0()); 
            }
            match(input,RULE_INT,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getSpanINTTerminalRuleCall_1_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__SpanAssignment_1_2_1"


    // $ANTLR start "rule__Cell__WidthAssignment_1_3_1"
    // InternalPxManual.g:5325:1: rule__Cell__WidthAssignment_1_3_1 : ( RULE_STRING ) ;
    public final void rule__Cell__WidthAssignment_1_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5329:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5330:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5330:2: ( RULE_STRING )
            // InternalPxManual.g:5331:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getWidthSTRINGTerminalRuleCall_1_3_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getWidthSTRINGTerminalRuleCall_1_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__WidthAssignment_1_3_1"


    // $ANTLR start "rule__Cell__VAlignAssignment_1_4_1"
    // InternalPxManual.g:5340:1: rule__Cell__VAlignAssignment_1_4_1 : ( ruleVerticalAlignment ) ;
    public final void rule__Cell__VAlignAssignment_1_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5344:1: ( ( ruleVerticalAlignment ) )
            // InternalPxManual.g:5345:2: ( ruleVerticalAlignment )
            {
            // InternalPxManual.g:5345:2: ( ruleVerticalAlignment )
            // InternalPxManual.g:5346:3: ruleVerticalAlignment
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getVAlignVerticalAlignmentEnumRuleCall_1_4_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleVerticalAlignment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getVAlignVerticalAlignmentEnumRuleCall_1_4_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__VAlignAssignment_1_4_1"


    // $ANTLR start "rule__Cell__BodyAssignment_2"
    // InternalPxManual.g:5355:1: rule__Cell__BodyAssignment_2 : ( ruleDocBody ) ;
    public final void rule__Cell__BodyAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5359:1: ( ( ruleDocBody ) )
            // InternalPxManual.g:5360:2: ( ruleDocBody )
            {
            // InternalPxManual.g:5360:2: ( ruleDocBody )
            // InternalPxManual.g:5361:3: ruleDocBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCellAccess().getBodyDocBodyParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDocBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCellAccess().getBodyDocBodyParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cell__BodyAssignment_2"


    // $ANTLR start "rule__Image__NameAssignment_1"
    // InternalPxManual.g:5370:1: rule__Image__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Image__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5374:1: ( ( RULE_ID ) )
            // InternalPxManual.g:5375:2: ( RULE_ID )
            {
            // InternalPxManual.g:5375:2: ( RULE_ID )
            // InternalPxManual.g:5376:3: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__NameAssignment_1"


    // $ANTLR start "rule__Image__PathAssignment_2"
    // InternalPxManual.g:5385:1: rule__Image__PathAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Image__PathAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5389:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5390:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5390:2: ( RULE_STRING )
            // InternalPxManual.g:5391:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getPathSTRINGTerminalRuleCall_2_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getPathSTRINGTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__PathAssignment_2"


    // $ANTLR start "rule__Image__StyleAssignment_3_0_1"
    // InternalPxManual.g:5400:1: rule__Image__StyleAssignment_3_0_1 : ( RULE_STRING ) ;
    public final void rule__Image__StyleAssignment_3_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5404:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5405:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5405:2: ( RULE_STRING )
            // InternalPxManual.g:5406:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getStyleSTRINGTerminalRuleCall_3_0_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getStyleSTRINGTerminalRuleCall_3_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__StyleAssignment_3_0_1"


    // $ANTLR start "rule__Image__ClassNameAssignment_3_1_1"
    // InternalPxManual.g:5415:1: rule__Image__ClassNameAssignment_3_1_1 : ( RULE_STRING ) ;
    public final void rule__Image__ClassNameAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5419:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5420:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5420:2: ( RULE_STRING )
            // InternalPxManual.g:5421:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getClassNameSTRINGTerminalRuleCall_3_1_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getClassNameSTRINGTerminalRuleCall_3_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__ClassNameAssignment_3_1_1"


    // $ANTLR start "rule__Image__WidthAssignment_3_2_1"
    // InternalPxManual.g:5430:1: rule__Image__WidthAssignment_3_2_1 : ( RULE_INT ) ;
    public final void rule__Image__WidthAssignment_3_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5434:1: ( ( RULE_INT ) )
            // InternalPxManual.g:5435:2: ( RULE_INT )
            {
            // InternalPxManual.g:5435:2: ( RULE_INT )
            // InternalPxManual.g:5436:3: RULE_INT
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImageAccess().getWidthINTTerminalRuleCall_3_2_1_0()); 
            }
            match(input,RULE_INT,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImageAccess().getWidthINTTerminalRuleCall_3_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__WidthAssignment_3_2_1"


    // $ANTLR start "rule__Section__NameAssignment_1"
    // InternalPxManual.g:5445:1: rule__Section__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Section__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5449:1: ( ( RULE_ID ) )
            // InternalPxManual.g:5450:2: ( RULE_ID )
            {
            // InternalPxManual.g:5450:2: ( RULE_ID )
            // InternalPxManual.g:5451:3: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSectionAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSectionAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__NameAssignment_1"


    // $ANTLR start "rule__Section__TitleAssignment_2"
    // InternalPxManual.g:5460:1: rule__Section__TitleAssignment_2 : ( ruleText ) ;
    public final void rule__Section__TitleAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5464:1: ( ( ruleText ) )
            // InternalPxManual.g:5465:2: ( ruleText )
            {
            // InternalPxManual.g:5465:2: ( ruleText )
            // InternalPxManual.g:5466:3: ruleText
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSectionAccess().getTitleTextParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleText();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSectionAccess().getTitleTextParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__TitleAssignment_2"


    // $ANTLR start "rule__Section__ElementsAssignment_4"
    // InternalPxManual.g:5475:1: rule__Section__ElementsAssignment_4 : ( ruleManualElement ) ;
    public final void rule__Section__ElementsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5479:1: ( ( ruleManualElement ) )
            // InternalPxManual.g:5480:2: ( ruleManualElement )
            {
            // InternalPxManual.g:5480:2: ( ruleManualElement )
            // InternalPxManual.g:5481:3: ruleManualElement
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSectionAccess().getElementsManualElementParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleManualElement();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSectionAccess().getElementsManualElementParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Section__ElementsAssignment_4"


    // $ANTLR start "rule__Page__NameAssignment_1"
    // InternalPxManual.g:5490:1: rule__Page__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Page__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5494:1: ( ( RULE_ID ) )
            // InternalPxManual.g:5495:2: ( RULE_ID )
            {
            // InternalPxManual.g:5495:2: ( RULE_ID )
            // InternalPxManual.g:5496:3: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPageAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPageAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__NameAssignment_1"


    // $ANTLR start "rule__Page__TitleAssignment_2"
    // InternalPxManual.g:5505:1: rule__Page__TitleAssignment_2 : ( ruleText ) ;
    public final void rule__Page__TitleAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5509:1: ( ( ruleText ) )
            // InternalPxManual.g:5510:2: ( ruleText )
            {
            // InternalPxManual.g:5510:2: ( ruleText )
            // InternalPxManual.g:5511:3: ruleText
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPageAccess().getTitleTextParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleText();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPageAccess().getTitleTextParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__TitleAssignment_2"


    // $ANTLR start "rule__Page__ElementsAssignment_4"
    // InternalPxManual.g:5520:1: rule__Page__ElementsAssignment_4 : ( ruleManualElement ) ;
    public final void rule__Page__ElementsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5524:1: ( ( ruleManualElement ) )
            // InternalPxManual.g:5525:2: ( ruleManualElement )
            {
            // InternalPxManual.g:5525:2: ( ruleManualElement )
            // InternalPxManual.g:5526:3: ruleManualElement
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPageAccess().getElementsManualElementParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleManualElement();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPageAccess().getElementsManualElementParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Page__ElementsAssignment_4"


    // $ANTLR start "rule__Example__TitleAssignment_1_0"
    // InternalPxManual.g:5535:1: rule__Example__TitleAssignment_1_0 : ( ruleText ) ;
    public final void rule__Example__TitleAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5539:1: ( ( ruleText ) )
            // InternalPxManual.g:5540:2: ( ruleText )
            {
            // InternalPxManual.g:5540:2: ( ruleText )
            // InternalPxManual.g:5541:3: ruleText
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExampleAccess().getTitleTextParserRuleCall_1_0_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleText();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExampleAccess().getTitleTextParserRuleCall_1_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__TitleAssignment_1_0"


    // $ANTLR start "rule__Example__ScreenshotAssignment_1_1_1"
    // InternalPxManual.g:5550:1: rule__Example__ScreenshotAssignment_1_1_1 : ( RULE_STRING ) ;
    public final void rule__Example__ScreenshotAssignment_1_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5554:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5555:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5555:2: ( RULE_STRING )
            // InternalPxManual.g:5556:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExampleAccess().getScreenshotSTRINGTerminalRuleCall_1_1_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExampleAccess().getScreenshotSTRINGTerminalRuleCall_1_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__ScreenshotAssignment_1_1_1"


    // $ANTLR start "rule__Example__BodyAssignment_2"
    // InternalPxManual.g:5565:1: rule__Example__BodyAssignment_2 : ( ruleDocBody ) ;
    public final void rule__Example__BodyAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5569:1: ( ( ruleDocBody ) )
            // InternalPxManual.g:5570:2: ( ruleDocBody )
            {
            // InternalPxManual.g:5570:2: ( ruleDocBody )
            // InternalPxManual.g:5571:3: ruleDocBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExampleAccess().getBodyDocBodyParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDocBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExampleAccess().getBodyDocBodyParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Example__BodyAssignment_2"


    // $ANTLR start "rule__Usage__TitleAssignment_1_0"
    // InternalPxManual.g:5580:1: rule__Usage__TitleAssignment_1_0 : ( ruleText ) ;
    public final void rule__Usage__TitleAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5584:1: ( ( ruleText ) )
            // InternalPxManual.g:5585:2: ( ruleText )
            {
            // InternalPxManual.g:5585:2: ( ruleText )
            // InternalPxManual.g:5586:3: ruleText
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUsageAccess().getTitleTextParserRuleCall_1_0_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleText();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUsageAccess().getTitleTextParserRuleCall_1_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__TitleAssignment_1_0"


    // $ANTLR start "rule__Usage__ScreenshotAssignment_1_1_1"
    // InternalPxManual.g:5595:1: rule__Usage__ScreenshotAssignment_1_1_1 : ( RULE_STRING ) ;
    public final void rule__Usage__ScreenshotAssignment_1_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5599:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5600:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5600:2: ( RULE_STRING )
            // InternalPxManual.g:5601:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUsageAccess().getScreenshotSTRINGTerminalRuleCall_1_1_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUsageAccess().getScreenshotSTRINGTerminalRuleCall_1_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__ScreenshotAssignment_1_1_1"


    // $ANTLR start "rule__Usage__BodyAssignment_2"
    // InternalPxManual.g:5610:1: rule__Usage__BodyAssignment_2 : ( ruleDocBody ) ;
    public final void rule__Usage__BodyAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5614:1: ( ( ruleDocBody ) )
            // InternalPxManual.g:5615:2: ( ruleDocBody )
            {
            // InternalPxManual.g:5615:2: ( ruleDocBody )
            // InternalPxManual.g:5616:3: ruleDocBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUsageAccess().getBodyDocBodyParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDocBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUsageAccess().getBodyDocBodyParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Usage__BodyAssignment_2"


    // $ANTLR start "rule__Text__LanguageAssignment_0_1"
    // InternalPxManual.g:5625:1: rule__Text__LanguageAssignment_0_1 : ( RULE_ID ) ;
    public final void rule__Text__LanguageAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5629:1: ( ( RULE_ID ) )
            // InternalPxManual.g:5630:2: ( RULE_ID )
            {
            // InternalPxManual.g:5630:2: ( RULE_ID )
            // InternalPxManual.g:5631:3: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextAccess().getLanguageIDTerminalRuleCall_0_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextAccess().getLanguageIDTerminalRuleCall_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__LanguageAssignment_0_1"


    // $ANTLR start "rule__Text__ClassNameAssignment_1_1"
    // InternalPxManual.g:5640:1: rule__Text__ClassNameAssignment_1_1 : ( RULE_ID ) ;
    public final void rule__Text__ClassNameAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5644:1: ( ( RULE_ID ) )
            // InternalPxManual.g:5645:2: ( RULE_ID )
            {
            // InternalPxManual.g:5645:2: ( RULE_ID )
            // InternalPxManual.g:5646:3: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextAccess().getClassNameIDTerminalRuleCall_1_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextAccess().getClassNameIDTerminalRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__ClassNameAssignment_1_1"


    // $ANTLR start "rule__Text__BodyAssignment_2"
    // InternalPxManual.g:5655:1: rule__Text__BodyAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Text__BodyAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5659:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5660:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5660:2: ( RULE_STRING )
            // InternalPxManual.g:5661:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTextAccess().getBodySTRINGTerminalRuleCall_2_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTextAccess().getBodySTRINGTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Text__BodyAssignment_2"


    // $ANTLR start "rule__Keyword__NameAssignment_1"
    // InternalPxManual.g:5670:1: rule__Keyword__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Keyword__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5674:1: ( ( RULE_ID ) )
            // InternalPxManual.g:5675:2: ( RULE_ID )
            {
            // InternalPxManual.g:5675:2: ( RULE_ID )
            // InternalPxManual.g:5676:3: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__NameAssignment_1"


    // $ANTLR start "rule__Keyword__KeywordNameAssignment_2"
    // InternalPxManual.g:5685:1: rule__Keyword__KeywordNameAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Keyword__KeywordNameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5689:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5690:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5690:2: ( RULE_STRING )
            // InternalPxManual.g:5691:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getKeywordNameSTRINGTerminalRuleCall_2_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getKeywordNameSTRINGTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__KeywordNameAssignment_2"


    // $ANTLR start "rule__Keyword__DocumentationAssignment_4_0_1"
    // InternalPxManual.g:5700:1: rule__Keyword__DocumentationAssignment_4_0_1 : ( ruleDocBody ) ;
    public final void rule__Keyword__DocumentationAssignment_4_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5704:1: ( ( ruleDocBody ) )
            // InternalPxManual.g:5705:2: ( ruleDocBody )
            {
            // InternalPxManual.g:5705:2: ( ruleDocBody )
            // InternalPxManual.g:5706:3: ruleDocBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getDocumentationDocBodyParserRuleCall_4_0_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDocBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getDocumentationDocBodyParserRuleCall_4_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__DocumentationAssignment_4_0_1"


    // $ANTLR start "rule__Keyword__PrettyTitleAssignment_4_1_1"
    // InternalPxManual.g:5715:1: rule__Keyword__PrettyTitleAssignment_4_1_1 : ( ruleText ) ;
    public final void rule__Keyword__PrettyTitleAssignment_4_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5719:1: ( ( ruleText ) )
            // InternalPxManual.g:5720:2: ( ruleText )
            {
            // InternalPxManual.g:5720:2: ( ruleText )
            // InternalPxManual.g:5721:3: ruleText
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getPrettyTitleTextParserRuleCall_4_1_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleText();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getPrettyTitleTextParserRuleCall_4_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__PrettyTitleAssignment_4_1_1"


    // $ANTLR start "rule__Keyword__ExamplesAssignment_4_2"
    // InternalPxManual.g:5730:1: rule__Keyword__ExamplesAssignment_4_2 : ( ruleAbstractExample ) ;
    public final void rule__Keyword__ExamplesAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5734:1: ( ( ruleAbstractExample ) )
            // InternalPxManual.g:5735:2: ( ruleAbstractExample )
            {
            // InternalPxManual.g:5735:2: ( ruleAbstractExample )
            // InternalPxManual.g:5736:3: ruleAbstractExample
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getExamplesAbstractExampleParserRuleCall_4_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAbstractExample();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getExamplesAbstractExampleParserRuleCall_4_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__ExamplesAssignment_4_2"


    // $ANTLR start "rule__Keyword__AttributesAssignment_4_3"
    // InternalPxManual.g:5745:1: rule__Keyword__AttributesAssignment_4_3 : ( ruleKeywordAttribute ) ;
    public final void rule__Keyword__AttributesAssignment_4_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5749:1: ( ( ruleKeywordAttribute ) )
            // InternalPxManual.g:5750:2: ( ruleKeywordAttribute )
            {
            // InternalPxManual.g:5750:2: ( ruleKeywordAttribute )
            // InternalPxManual.g:5751:3: ruleKeywordAttribute
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getAttributesKeywordAttributeParserRuleCall_4_3_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleKeywordAttribute();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getAttributesKeywordAttributeParserRuleCall_4_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__AttributesAssignment_4_3"


    // $ANTLR start "rule__Keyword__ConclusionAssignment_4_4_1"
    // InternalPxManual.g:5760:1: rule__Keyword__ConclusionAssignment_4_4_1 : ( ruleDocBody ) ;
    public final void rule__Keyword__ConclusionAssignment_4_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5764:1: ( ( ruleDocBody ) )
            // InternalPxManual.g:5765:2: ( ruleDocBody )
            {
            // InternalPxManual.g:5765:2: ( ruleDocBody )
            // InternalPxManual.g:5766:3: ruleDocBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAccess().getConclusionDocBodyParserRuleCall_4_4_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDocBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAccess().getConclusionDocBodyParserRuleCall_4_4_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Keyword__ConclusionAssignment_4_4_1"


    // $ANTLR start "rule__KeywordAttribute__AttNameAssignment_1"
    // InternalPxManual.g:5775:1: rule__KeywordAttribute__AttNameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__KeywordAttribute__AttNameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5779:1: ( ( RULE_STRING ) )
            // InternalPxManual.g:5780:2: ( RULE_STRING )
            {
            // InternalPxManual.g:5780:2: ( RULE_STRING )
            // InternalPxManual.g:5781:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getAttNameSTRINGTerminalRuleCall_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getAttNameSTRINGTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__AttNameAssignment_1"


    // $ANTLR start "rule__KeywordAttribute__DocumentationAssignment_3_0_1"
    // InternalPxManual.g:5790:1: rule__KeywordAttribute__DocumentationAssignment_3_0_1 : ( ruleDocBody ) ;
    public final void rule__KeywordAttribute__DocumentationAssignment_3_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5794:1: ( ( ruleDocBody ) )
            // InternalPxManual.g:5795:2: ( ruleDocBody )
            {
            // InternalPxManual.g:5795:2: ( ruleDocBody )
            // InternalPxManual.g:5796:3: ruleDocBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getDocumentationDocBodyParserRuleCall_3_0_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDocBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getDocumentationDocBodyParserRuleCall_3_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__DocumentationAssignment_3_0_1"


    // $ANTLR start "rule__KeywordAttribute__ExampleAssignment_3_1"
    // InternalPxManual.g:5805:1: rule__KeywordAttribute__ExampleAssignment_3_1 : ( ruleAbstractExample ) ;
    public final void rule__KeywordAttribute__ExampleAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5809:1: ( ( ruleAbstractExample ) )
            // InternalPxManual.g:5810:2: ( ruleAbstractExample )
            {
            // InternalPxManual.g:5810:2: ( ruleAbstractExample )
            // InternalPxManual.g:5811:3: ruleAbstractExample
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getExampleAbstractExampleParserRuleCall_3_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAbstractExample();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getExampleAbstractExampleParserRuleCall_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__ExampleAssignment_3_1"


    // $ANTLR start "rule__KeywordAttribute__OptionalAssignment_3_2"
    // InternalPxManual.g:5820:1: rule__KeywordAttribute__OptionalAssignment_3_2 : ( ( 'optional' ) ) ;
    public final void rule__KeywordAttribute__OptionalAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5824:1: ( ( ( 'optional' ) ) )
            // InternalPxManual.g:5825:2: ( ( 'optional' ) )
            {
            // InternalPxManual.g:5825:2: ( ( 'optional' ) )
            // InternalPxManual.g:5826:3: ( 'optional' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getOptionalOptionalKeyword_3_2_0()); 
            }
            // InternalPxManual.g:5827:3: ( 'optional' )
            // InternalPxManual.g:5828:4: 'optional'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getOptionalOptionalKeyword_3_2_0()); 
            }
            match(input,47,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getOptionalOptionalKeyword_3_2_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getOptionalOptionalKeyword_3_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__OptionalAssignment_3_2"


    // $ANTLR start "rule__KeywordAttribute__PossibleValuesAssignment_3_3_1"
    // InternalPxManual.g:5839:1: rule__KeywordAttribute__PossibleValuesAssignment_3_3_1 : ( ruleDocBody ) ;
    public final void rule__KeywordAttribute__PossibleValuesAssignment_3_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPxManual.g:5843:1: ( ( ruleDocBody ) )
            // InternalPxManual.g:5844:2: ( ruleDocBody )
            {
            // InternalPxManual.g:5844:2: ( ruleDocBody )
            // InternalPxManual.g:5845:3: ruleDocBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getKeywordAttributeAccess().getPossibleValuesDocBodyParserRuleCall_3_3_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleDocBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getKeywordAttributeAccess().getPossibleValuesDocBodyParserRuleCall_3_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__KeywordAttribute__PossibleValuesAssignment_3_3_1"

    // $ANTLR start synpred1_InternalPxManual
    public final void synpred1_InternalPxManual_fragment() throws RecognitionException {   
        // InternalPxManual.g:4785:8: ( rule__Keyword__ExamplesAssignment_4_2 )
        // InternalPxManual.g:4785:9: rule__Keyword__ExamplesAssignment_4_2
        {
        pushFollow(FOLLOW_2);
        rule__Keyword__ExamplesAssignment_4_2();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred1_InternalPxManual

    // $ANTLR start synpred2_InternalPxManual
    public final void synpred2_InternalPxManual_fragment() throws RecognitionException {   
        // InternalPxManual.g:4807:8: ( rule__Keyword__AttributesAssignment_4_3 )
        // InternalPxManual.g:4807:9: rule__Keyword__AttributesAssignment_4_3
        {
        pushFollow(FOLLOW_2);
        rule__Keyword__AttributesAssignment_4_3();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred2_InternalPxManual

    // $ANTLR start synpred3_InternalPxManual
    public final void synpred3_InternalPxManual_fragment() throws RecognitionException {   
        // InternalPxManual.g:4948:8: ( rule__KeywordAttribute__ExampleAssignment_3_1 )
        // InternalPxManual.g:4948:9: rule__KeywordAttribute__ExampleAssignment_3_1
        {
        pushFollow(FOLLOW_2);
        rule__KeywordAttribute__ExampleAssignment_3_1();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred3_InternalPxManual

    // Delegated rules

    public final boolean synpred1_InternalPxManual() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalPxManual_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred2_InternalPxManual() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred2_InternalPxManual_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred3_InternalPxManual() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred3_InternalPxManual_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000018000030010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000003DE03580010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x000003DE03500012L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x000003DE03500010L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000018000200010L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000880000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000000C000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000010080000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000020080000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x00000001CC000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000007800L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x000000008C000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000018000000050L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x000001A000000010L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000018000000010L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x00003C5000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000C45000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000018000030012L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000018000200012L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x000000000C000002L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x00000001CC000002L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x000000008C000002L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x000001A000000012L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000005000000002L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000200000000002L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x00003C5000000002L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000C45000000002L});

}
