package org.pragmaticmodeling.pxmanual.gen.files;

import com.google.inject.Injector;
import java.util.List;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.TextFileAccess;
import org.pragmaticmodeling.pxgen.runtime.IContext;
import org.pragmaticmodeling.pxmanual.gen.IDocumentationProjectContext;
import org.pragmaticmodeling.pxmanual.pxManual.Document;
import org.pragmaticmodeling.pxmanual.pxManual.Page;
import org.pragmaticmodeling.pxmanual.util.PxManualHelper;

@SuppressWarnings("all")
public class TocFile extends TextFileAccess implements IGuiceAwareGeneratorComponent, IContext {
  private IDocumentationProjectContext context;
  
  public TocFile(final IDocumentationProjectContext context, final Document document) {
    super();
    this.context = context;
    this.document = document;
  }
  
  public IDocumentationProjectContext getContext() {
    return this.context;
  }
  
  public void setContext(final IDocumentationProjectContext context) {
    this.context = context;
  }
  
  private Document document;
  
  public Document getDocument() {
    return this.document;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
  }
  
  public String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<?NLS TYPE=\"org.eclipse.help.toc\"?>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<toc label=\"");
    String _body = this.document.getTitle().getBody();
    _builder.append(_body, "\t\t");
    _builder.append("\" topic=\"html/");
    String _fileName = this.context.fileName(this.document);
    _builder.append(_fileName, "\t\t");
    _builder.append(".html\">");
    _builder.newLineIfNotEmpty();
    {
      List<Page> _pages = PxManualHelper.getPages(this.document);
      for(final Page page : _pages) {
        _builder.append("\t\t");
        _builder.append("<topic label=\"");
        String _body_1 = page.getTitle().getBody();
        _builder.append(_body_1, "\t\t");
        _builder.append("\"  href=\"html/");
        String _name = page.getName();
        _builder.append(_name, "\t\t");
        _builder.append(".html\"/> ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append("</toc>");
    _builder.newLine();
    return _builder.toString();
  }
}
