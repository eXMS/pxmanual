package org.pragmaticmodeling.pxmanual.gen;

import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;
import org.pragmaticmodeling.pxmanual.gen.DocumentationProject;
import org.pragmaticmodeling.pxmanual.gen.IPxManualsContext;

@SuppressWarnings("all")
public interface IEclipseManualContext extends IPxManualsContext {
  @PxGenParameter
  public abstract String getProjectName();
  
  @PxGenParameter
  public abstract void setProjectName(final String projectName);
  
  public abstract DocumentationProject getDocumentationProject();
}
