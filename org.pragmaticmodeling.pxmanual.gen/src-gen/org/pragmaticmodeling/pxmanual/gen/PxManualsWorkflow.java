package org.pragmaticmodeling.pxmanual.gen;

import org.pragmaticmodeling.pxgen.runtime.AbstractPxGenWorkflowComponent;
import org.pragmaticmodeling.pxgen.runtime.IGenerator;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorModule;

@SuppressWarnings("all")
public class PxManualsWorkflow extends AbstractPxGenWorkflowComponent {
  private IGenerator generator;
  
  private IGeneratorModule configuration = new org.pragmaticmodeling.pxmanual.gen.PxManualsModule();
  
  public PxManualsWorkflow() {
    new org.pragmaticmodeling.pxmanual.gen.PxManualsStandaloneSetup().createInjectorAndDoEMFRegistration();
  }
  
  public void doGenerate() {
    getGenerator().generate();
  }
  
  public IGenerator getGenerator() {
    return this.generator;
  }
  
  public void setGenerator(final IGenerator generator) {
    this.generator = generator;
  }
  
  public IGeneratorModule getConfiguration() {
    return this.configuration;
  }
  
  public void setConfiguration(final IGeneratorModule configuration) {
    this.configuration = configuration;
  }
}
