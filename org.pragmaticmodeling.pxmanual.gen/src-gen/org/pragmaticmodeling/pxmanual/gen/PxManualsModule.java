package org.pragmaticmodeling.pxmanual.gen;

import org.pragmaticmodeling.pxgen.runtime.IGenerator;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorModule;
import org.pragmaticmodeling.pxgen.runtime.launch.AbstractGeneratorModule;

@SuppressWarnings("all")
public class PxManualsModule extends AbstractGeneratorModule implements IGeneratorModule {
  public Class<? extends IGeneratorModule> bindIGeneratorModule() {
    return org.pragmaticmodeling.pxmanual.gen.PxManualsModule.class;
  }
  
  public Class<? extends IGenerator> bindIGenerator2() {
    return org.pragmaticmodeling.pxmanual.gen.PxManuals.class;
  }
}
