package org.pragmaticmodeling.pxmanual.gen;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import com.google.inject.Injector;
import java.util.List;
import org.apache.commons.lang.StringEscapeUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.pragmaticmodeling.pxgen.runtime.AbstractGenerator;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;
import org.pragmaticmodeling.pxmanual.PxManualStandaloneSetup;
import org.pragmaticmodeling.pxmanual.gen.IPxManualsContext;
import org.pragmaticmodeling.pxmanual.pxManual.AbstractSection;
import org.pragmaticmodeling.pxmanual.pxManual.Cell;
import org.pragmaticmodeling.pxmanual.pxManual.DocBody;
import org.pragmaticmodeling.pxmanual.pxManual.Document;
import org.pragmaticmodeling.pxmanual.pxManual.Example;
import org.pragmaticmodeling.pxmanual.pxManual.Image;
import org.pragmaticmodeling.pxmanual.pxManual.Keyword;
import org.pragmaticmodeling.pxmanual.pxManual.KeywordAttribute;
import org.pragmaticmodeling.pxmanual.pxManual.ManualElement;
import org.pragmaticmodeling.pxmanual.pxManual.Page;
import org.pragmaticmodeling.pxmanual.pxManual.Row;
import org.pragmaticmodeling.pxmanual.pxManual.SamePage;
import org.pragmaticmodeling.pxmanual.pxManual.Section;
import org.pragmaticmodeling.pxmanual.pxManual.Table;
import org.pragmaticmodeling.pxmanual.pxManual.Text;
import org.pragmaticmodeling.pxmanual.pxManual.Usage;
import org.pragmaticmodeling.pxmanual.pxManual.VerticalAlignment;

@SuppressWarnings("all")
public class PxManuals extends AbstractGenerator implements IPxManualsContext {
  public PxManuals() {
    super();
  }
  
  private String basePackage;
  
  @PxGenParameter
  public String getBasePackage() {
    return basePackage;
  }
  
  @PxGenParameter
  public void setBasePackage(final String basePackage) {
    this.basePackage = basePackage;
  }
  
  private String pxManualUri;
  
  @PxGenParameter
  public String getPxManualUri() {
    return pxManualUri;
  }
  
  @PxGenParameter
  public void setPxManualUri(final String pxManualUri) {
    this.pxManualUri = pxManualUri;
  }
  
  private Document document;
  
  @PxGenParameter
  public void setDocument(final Document document) {
    this.document = document;
  }
  
  private Integer depth;
  
  private Integer initDepth() {
    return Integer.valueOf(0);
  }
  
  @PxGenParameter
  public Integer getDepth() {
    if (depth == null) {
    	depth = initDepth();	
    }
    return depth;
  }
  
  @PxGenParameter
  public void setDepth(final Integer depth) {
    this.depth = depth;
  }
  
  @Inject
  private XtextResourceSet resourceSet;
  
  @PxGenParameter
  public XtextResourceSet getResourceSet() {
    return resourceSet;
  }
  
  @PxGenParameter
  public void setResourceSet(final XtextResourceSet resourceSet) {
    this.resourceSet = resourceSet;
  }
  
  public String toHtml(final ManualElement e, final boolean withGutter) {
    boolean _matched = false;
    if (e instanceof Page) {
      _matched=true;
      return this.toPageLink(((Page)e));
    }
    if (!_matched) {
      if (e instanceof Section) {
        _matched=true;
        return this.toHtmlSection(((Section)e));
      }
    }
    if (!_matched) {
      if (e instanceof Keyword) {
        _matched=true;
        return this.toHtmlKeyword(((Keyword)e));
      }
    }
    if (!_matched) {
      if (e instanceof Text) {
        _matched=true;
        return this.toHtmlDocumentation(((Text)e), withGutter);
      }
    }
    if (!_matched) {
      if (e instanceof Image) {
        _matched=true;
        return this.toHtmlImage(((Image)e));
      }
    }
    if (!_matched) {
      if (e instanceof Example) {
        _matched=true;
        return this.toBasicExample(((Example)e));
      }
    }
    if (!_matched) {
      if (e instanceof Usage) {
        _matched=true;
        return this.toUsageExample(((Usage)e));
      }
    }
    if (!_matched) {
      if (e instanceof Table) {
        _matched=true;
        return this.toHtmlTable(((Table)e));
      }
    }
    if (!_matched) {
      if (e instanceof SamePage) {
        _matched=true;
        return this.toHtml(((SamePage)e).getBody());
      }
    }
    return "";
  }
  
  public String toUsageExample(final Usage usage) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<p class=UsageOfpxDoc>Usage : ");
    Text _title = usage.getTitle();
    String _body = null;
    if (_title!=null) {
      _body=_title.getBody();
    }
    _builder.append(_body);
    _builder.append("</p>");
    _builder.newLineIfNotEmpty();
    String _html = this.toHtml(usage.getBody());
    _builder.append(_html);
    _builder.newLineIfNotEmpty();
    _builder.append("<br/>");
    return _builder.toString();
  }
  
  public String toAttributeRow(final KeywordAttribute attribute) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<tr style=\'mso-yfti-irow:1\'>");
    _builder.newLine();
    _builder.append("\t\t  ");
    _builder.append("<td width=157 valign=top>");
    _builder.newLine();
    {
      Iterable<Example> _filter = Iterables.<Example>filter(attribute.getExample(), Example.class);
      for(final Example e : _filter) {
        _builder.append("\t\t  ");
        String _basicExample = this.toBasicExample(e);
        _builder.append(_basicExample, "\t\t  ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t  ");
    _builder.append("</td>");
    _builder.newLine();
    _builder.append("\t\t  ");
    _builder.append("<td width=157 valign=top>");
    _builder.newLine();
    _builder.append("\t\t  ");
    String _html = this.toHtml(attribute.getDocumentation());
    _builder.append(_html, "\t\t  ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t  ");
    _builder.append("</td>");
    _builder.newLine();
    _builder.append("\t\t  ");
    _builder.append("<td width=157 valign=top>");
    _builder.newLine();
    _builder.append("\t\t  ");
    String _html_1 = this.toHtml(attribute.getPossibleValues());
    _builder.append(_html_1, "\t\t  ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t  ");
    _builder.append("</td>");
    _builder.newLine();
    _builder.append("\t\t  ");
    _builder.append("<td width=157 valign=top>");
    _builder.newLine();
    _builder.append("\t\t  ");
    _builder.append("<p>");
    {
      boolean _isOptional = attribute.isOptional();
      if (_isOptional) {
        _builder.append("Yes");
      } else {
        _builder.append("No");
      }
    }
    _builder.append("</p>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t  ");
    _builder.append("</td>");
    _builder.newLine();
    _builder.append("\t\t ");
    _builder.append("</tr>");
    return _builder.toString();
  }
  
  public String toBasicExample(final Example example) {
    StringConcatenation _builder = new StringConcatenation();
    String _htmlDocBody = this.toHtmlDocBody(example.getBody(), false);
    _builder.append(_htmlDocBody);
    return _builder.toString();
  }
  
  public String toHtml(final DocBody docBody) {
    return this.toHtmlDocBody(docBody, true);
  }
  
  public String toHtmlDocBody(final DocBody docBody, final boolean withGutter) {
    String _xblockexpression = null;
    {
      String result = "";
      if ((docBody != null)) {
        EList<ManualElement> _elements = docBody.getElements();
        for (final ManualElement e : _elements) {
          String _result = result;
          String _html = this.toHtml(e, withGutter);
          result = (_result + _html);
        }
      }
      _xblockexpression = result;
    }
    return _xblockexpression;
  }
  
  public String toHtmlDocumentation(final Text documentation, final boolean withGutter) {
    String _xifexpression = null;
    if ((documentation != null)) {
      String _xblockexpression = null;
      {
        String text = StringEscapeUtils.escapeHtml(documentation.getBody());
        StringConcatenation _builder = new StringConcatenation();
        {
          if (((documentation.getLanguage() != null) && (!documentation.getLanguage().isEmpty()))) {
            _builder.newLineIfNotEmpty();
            {
              String _language = documentation.getLanguage();
              boolean _equals = Objects.equal(_language, "html");
              if (_equals) {
                {
                  String _className = documentation.getClassName();
                  boolean _tripleNotEquals = (_className != null);
                  if (_tripleNotEquals) {
                    _builder.append("<div class=\"");
                    String _className_1 = documentation.getClassName();
                    _builder.append(_className_1);
                    _builder.append("\">");
                    _builder.newLineIfNotEmpty();
                    String _body = documentation.getBody();
                    _builder.append(_body);
                    _builder.newLineIfNotEmpty();
                    _builder.append("</div>");
                    _builder.newLine();
                  } else {
                    String _body_1 = documentation.getBody();
                    _builder.append(_body_1);
                    _builder.newLineIfNotEmpty();
                  }
                }
              } else {
                String _language_1 = documentation.getLanguage();
                boolean _equals_1 = Objects.equal(_language_1, "pxdoc");
                if (_equals_1) {
                  _builder.append("<pre class=\"brush: pxdoc");
                  {
                    if ((!withGutter)) {
                      _builder.append("; gutter: false;");
                    }
                  }
                  _builder.append("\" style=\"white-space:pre-wrap;\">");
                  _builder.newLineIfNotEmpty();
                  _builder.append(text);
                  _builder.append("</pre>");
                  _builder.newLineIfNotEmpty();
                }
              }
            }
          } else {
            {
              String _className_2 = documentation.getClassName();
              boolean _equals_2 = Objects.equal(_className_2, "note");
              if (_equals_2) {
                _builder.append("<div class=\"");
                String _className_3 = documentation.getClassName();
                _builder.append(_className_3);
                _builder.append("\">");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("<p>");
                _builder.append(text, "\t");
                _builder.append("</p>");
                _builder.newLineIfNotEmpty();
                _builder.append("</div>");
                _builder.newLine();
              } else {
                _builder.append("<p>");
                _builder.append(text);
                _builder.append("</p>");
                _builder.newLineIfNotEmpty();
              }
            }
          }
        }
        _xblockexpression = _builder.toString();
      }
      _xifexpression = _xblockexpression;
    } else {
      _xifexpression = "";
    }
    return _xifexpression;
  }
  
  public String toPageLink(final Page page) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<a href=\"");
    String _fileName = this.fileName(page);
    _builder.append(_fileName);
    _builder.append(".html\">");
    String _body = page.getTitle().getBody();
    _builder.append(_body);
    _builder.append("</a>");
    return _builder.toString();
  }
  
  public String toHtmlTable(final Table table) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<table cellpadding=\"10\">");
    _builder.newLine();
    {
      EList<Row> _rows = table.getRows();
      for(final Row row : _rows) {
        _builder.append("\t\t");
        String _htmlRow = this.toHtmlRow(row);
        _builder.append(_htmlRow, "\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append("</table>");
    return _builder.toString();
  }
  
  public String toHtmlRow(final Row row) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<tr>");
    _builder.newLine();
    {
      EList<Cell> _cells = row.getCells();
      for(final Cell cell : _cells) {
        _builder.append("\t\t");
        String _htmlCell = this.toHtmlCell(cell);
        _builder.append(_htmlCell, "\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append("</tr>");
    return _builder.toString();
  }
  
  public String toHtmlCell(final Cell cell) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<td vAlign=\"");
    VerticalAlignment _vAlign = cell.getVAlign();
    _builder.append(_vAlign);
    _builder.append("\"");
    {
      String _width = cell.getWidth();
      boolean _tripleNotEquals = (_width != null);
      if (_tripleNotEquals) {
        _builder.append(" style=\"width:");
        String _width_1 = cell.getWidth();
        _builder.append(_width_1);
        _builder.append("\"");
      }
    }
    _builder.append(">");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    String _html = this.toHtml(cell.getBody());
    _builder.append(_html, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("</td>");
    return _builder.toString();
  }
  
  public String toHtmlImage(final Image image) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<img src=\"");
    String _path = image.getPath();
    _builder.append(_path);
    _builder.append("\"");
    String _imageWidth = this.imageWidth(image);
    _builder.append(_imageWidth);
    _builder.append("/>");
    return _builder.toString();
  }
  
  public String getDocName(final EObject object) {
    {
      Document doc = EcoreUtil2.<Document>getContainerOfType(object, Document.class);
      return this.fileName(doc);
    }
  }
  
  public String imageWidth(final Image image) {
    String _xblockexpression = null;
    {
      String width = "";
      int _width = image.getWidth();
      boolean _greaterThan = (_width > 0);
      if (_greaterThan) {
        int _width_1 = image.getWidth();
        String _plus = (" width=\"" + Integer.valueOf(_width_1));
        String _plus_1 = (_plus + "\"");
        width = _plus_1;
      }
      _xblockexpression = width;
    }
    return _xblockexpression;
  }
  
  public String toHtmlSection(final Section section) {
    StringConcatenation _builder = new StringConcatenation();
    String _enterSection = this.enterSection();
    _builder.append(_enterSection);
    _builder.append("<h");
    _builder.append(this.depth);
    _builder.append(" id=\"");
    String _id = this.getId(section);
    _builder.append(_id);
    _builder.append("\">");
    String _body = section.getTitle().getBody();
    _builder.append(_body);
    _builder.append("</h");
    _builder.append(this.depth);
    _builder.append(">");
    _builder.newLineIfNotEmpty();
    {
      EList<ManualElement> _elements = section.getElements();
      for(final ManualElement e : _elements) {
        _builder.append("\t\t");
        String _html = this.toHtml(e, true);
        _builder.append(_html, "\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    String _leaveSection = this.leaveSection();
    _builder.append(_leaveSection, "\t\t");
    return _builder.toString();
  }
  
  public String leaveSection() {
    String _xblockexpression = null;
    {
      this.depth--;
      _xblockexpression = "";
    }
    return _xblockexpression;
  }
  
  public String enterSection() {
    String _xblockexpression = null;
    {
      this.depth++;
      _xblockexpression = "";
    }
    return _xblockexpression;
  }
  
  public String toHtmlKeyword(final Keyword keyword) {
    String _xblockexpression = null;
    {
      Iterable<Example> basicExamples = Iterables.<Example>filter(keyword.getExamples(), Example.class);
      Iterable<Usage> usageExamples = Iterables.<Usage>filter(keyword.getExamples(), Usage.class);
      StringConcatenation _builder = new StringConcatenation();
      String _enterSection = this.enterSection();
      _builder.append(_enterSection);
      _builder.append("<h");
      _builder.append(this.depth);
      _builder.append(" id=\"");
      String _id = this.getId(keyword);
      _builder.append(_id);
      _builder.append("\">");
      String _title = this.title(keyword);
      _builder.append(_title);
      _builder.append("</h");
      _builder.append(this.depth);
      _builder.append(">");
      _builder.newLineIfNotEmpty();
      {
        DocBody _documentation = keyword.getDocumentation();
        boolean _tripleNotEquals = (_documentation != null);
        if (_tripleNotEquals) {
          _builder.append("\t\t\t");
          String _html = this.toHtml(keyword.getDocumentation());
          _builder.append(_html, "\t\t\t");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        for(final Example example : basicExamples) {
          _builder.append("\t\t\t");
          String _basicExample = this.toBasicExample(example);
          _builder.append(_basicExample, "\t\t\t");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        boolean _isEmpty = keyword.getAttributes().isEmpty();
        boolean _not = (!_isEmpty);
        if (_not) {
          _builder.append("\t\t\t");
          _builder.append("<table border=1 cellspacing=0 cellpadding=0 width=0 cellpadding=\"10\">");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append(" ");
          _builder.append("<thead>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("  ");
          _builder.append("<tr style=\'mso-yfti-irow:0;mso-yfti-firstrow:yes\'>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("<td class=header width=157 valign=top>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("<p>Attributes</p>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("</td>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("<td class=header width=157 valign=top>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("<p>Description</p>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("</td>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("<td class=header width=157 valign=top>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("<p>Possible values (if any)</p>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("</td>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("<td class=header width=157 valign=top>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("<p>Optional</p>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("   ");
          _builder.append("</td>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("  ");
          _builder.append("</tr>");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append(" ");
          _builder.append("</thead>");
          _builder.newLine();
          {
            EList<KeywordAttribute> _attributes = keyword.getAttributes();
            for(final KeywordAttribute a : _attributes) {
              _builder.append("\t\t\t");
              _builder.append(" ");
              String _attributeRow = this.toAttributeRow(a);
              _builder.append(_attributeRow, "\t\t\t ");
              _builder.newLineIfNotEmpty();
            }
          }
        }
      }
      _builder.append("\t\t\t\t");
      _builder.append("</table>");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("<br/>");
      _builder.newLine();
      {
        DocBody _conclusion = keyword.getConclusion();
        boolean _tripleNotEquals_1 = (_conclusion != null);
        if (_tripleNotEquals_1) {
          _builder.append("\t\t\t\t");
          String _html_1 = this.toHtml(keyword.getConclusion());
          _builder.append(_html_1, "\t\t\t\t");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        for(final Usage usage : usageExamples) {
          _builder.append("\t\t\t\t");
          String _usageExample = this.toUsageExample(usage);
          _builder.append(_usageExample, "\t\t\t\t");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t\t\t");
        }
      }
      String _leaveSection = this.leaveSection();
      _builder.append(_leaveSection, "\t\t\t\t");
      _builder.newLineIfNotEmpty();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  public List<ManualElement> getElements(final EObject object) {
    EList<ManualElement> _switchResult = null;
    boolean _matched = false;
    if (object instanceof Document) {
      _matched=true;
      _switchResult = ((Document)object).getBody().getElements();
    }
    if (!_matched) {
      if (object instanceof Page) {
        _matched=true;
        _switchResult = ((Page)object).getElements();
      }
    }
    return _switchResult;
  }
  
  /**
   * Table of contents
   */
  public String toToc(final ManualElement object) {
    boolean _matched = false;
    if (object instanceof AbstractSection) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<li><a href=\"");
      String _href = this.getHref(((AbstractSection)object));
      _builder.append(_href);
      _builder.append("\">");
      String _body = ((AbstractSection)object).getTitle().getBody();
      _builder.append(_body);
      _builder.append("</a>");
      {
        boolean _hasSubTitles = this.hasSubTitles(((AbstractSection)object));
        if (_hasSubTitles) {
          _builder.append("<ul>");
          {
            EList<ManualElement> _elements = ((AbstractSection)object).getElements();
            for(final ManualElement e : _elements) {
              _builder.newLineIfNotEmpty();
              String _toc = this.toToc(e);
              _builder.append(_toc);
              _builder.newLineIfNotEmpty();
              _builder.append("\t\t\t\t\t");
            }
          }
          _builder.append("</ul>");
        }
      }
      _builder.append("</li>");
      return _builder.toString();
    }
    if (!_matched) {
      if (object instanceof Keyword) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("<li><a href=\"");
        String _href = this.getHref(((Keyword)object));
        _builder.append(_href);
        _builder.append("\">");
        String _title = this.title(((Keyword)object));
        _builder.append(_title);
        _builder.append("</a></li>");
        return _builder.toString();
      }
    }
    return "";
  }
  
  public String toc(final EObject object) {
    boolean _matched = false;
    if (object instanceof Document) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<ul>");
      {
        EList<ManualElement> _elements = ((Document)object).getBody().getElements();
        for(final ManualElement e : _elements) {
          _builder.newLineIfNotEmpty();
          String _toc = this.toToc(e);
          _builder.append(_toc);
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t\t");
        }
      }
      _builder.append("</ul>");
      return _builder.toString();
    }
    if (!_matched) {
      if (object instanceof Page) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("<ul>");
        {
          EList<ManualElement> _elements = ((Page)object).getElements();
          for(final ManualElement e : _elements) {
            _builder.newLineIfNotEmpty();
            String _toc = this.toToc(e);
            _builder.append(_toc);
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t\t");
          }
        }
        _builder.append("</ul>");
        return _builder.toString();
      }
    }
    return "";
  }
  
  public String getId(final AbstractSection section) {
    String _name = section.getName();
    boolean _tripleNotEquals = (_name != null);
    if (_tripleNotEquals) {
      return section.getName();
    } else {
      return section.getTitle().getBody().replaceAll("\\W+", "");
    }
  }
  
  public String getId(final Keyword keyword) {
    String _name = keyword.getName();
    boolean _tripleNotEquals = (_name != null);
    if (_tripleNotEquals) {
      return keyword.getName();
    } else {
      return keyword.getKeywordName().replaceAll("\\W+", "");
    }
  }
  
  public String getHref(final AbstractSection section) {
    {
      Page parentPage = EcoreUtil2.<Page>getContainerOfType(section, Page.class);
      String _xifexpression = null;
      if ((parentPage != null)) {
        _xifexpression = this.getHtmlPage(section);
      } else {
        _xifexpression = "";
      }
      String pageRef = _xifexpression;
      String _id = this.getId(section);
      return ((pageRef + "#") + _id);
    }
  }
  
  public String getHtmlPage(final EObject object) {
    {
      Page page = EcoreUtil2.<Page>getContainerOfType(object, Page.class);
      if ((page != null)) {
        String _fileName = this.fileName(page);
        return (_fileName + ".html");
      }
      Document doc = EcoreUtil2.<Document>getContainerOfType(object, Document.class);
      String _fileName_1 = this.fileName(doc);
      return (_fileName_1 + ".html");
    }
  }
  
  public String fileName(final Document document) {
    String _xblockexpression = null;
    {
      String _id = document.getId();
      boolean _tripleNotEquals = (_id != null);
      if (_tripleNotEquals) {
        return document.getId();
      }
      _xblockexpression = document.getTitle().getBody().replaceAll("\\W+", "");
    }
    return _xblockexpression;
  }
  
  public String fileName(final Page page) {
    String _xblockexpression = null;
    {
      String _name = page.getName();
      boolean _tripleNotEquals = (_name != null);
      if (_tripleNotEquals) {
        return page.getName();
      }
      _xblockexpression = page.getTitle().getBody().replaceAll("\\W+", "");
    }
    return _xblockexpression;
  }
  
  public String getHref(final Keyword keyword) {
    String _htmlPage = this.getHtmlPage(keyword);
    String _plus = (_htmlPage + "#");
    String _id = this.getId(keyword);
    return (_plus + _id);
  }
  
  public boolean hasSubTitles(final AbstractSection section) {
    final Function1<ManualElement, Boolean> _function = (ManualElement e) -> {
      return this.isTitle(e);
    };
    boolean _isEmpty = IterableExtensions.isEmpty(IterableExtensions.<ManualElement>filter(section.getElements(), _function));
    return (!_isEmpty);
  }
  
  public Boolean isTitle(final ManualElement element) {
    boolean _matched = false;
    if (element instanceof AbstractSection) {
      _matched=true;
      return Boolean.valueOf(true);
    }
    if (!_matched) {
      if (element instanceof Keyword) {
        _matched=true;
        return Boolean.valueOf(true);
      }
    }
    return Boolean.valueOf(false);
  }
  
  public String title(final Keyword keyword) {
    {
      Text _prettyTitle = keyword.getPrettyTitle();
      boolean _tripleNotEquals = (_prettyTitle != null);
      if (_tripleNotEquals) {
        return this.toHtmlDocumentation(keyword.getPrettyTitle(), true);
      }
      String _keywordName = keyword.getKeywordName();
      String _plus = ("Keyword \'" + _keywordName);
      return (_plus + "\'");
    }
  }
  
  public Document getDocument() {
    {
      if ((this.document == null)) {
        this.document = this.loadDocument();
      }
      return this.document;
    }
  }
  
  public Document loadDocument() {
    {
      Injector injector = new PxManualStandaloneSetup().createInjectorAndDoEMFRegistration();
      XtextResourceSet resourceSet = injector.<XtextResourceSet>getInstance(XtextResourceSet.class);
      resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
      Resource resource = resourceSet.getResource(URI.createURI(this.pxManualUri), true);
      EObject _get = resource.getContents().get(0);
      Document model = ((Document) _get);
      return model;
    }
  }
  
  public String localStyles(final Document document) {
    String _xblockexpression = null;
    {
      int h1 = this.startingHeadingLevel(document);
      int h2 = (h1 + 1);
      int h3 = (h2 + 1);
      int h4 = (h3 + 1);
      int h5 = (h4 + 1);
      int h6 = (h5 + 1);
      int h7 = (h6 + 1);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("\t\t");
      _builder.append("<style type=\'text/css\'>");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("body {counter-reset: h");
      _builder.append(h2, "\t\t\t\t");
      _builder.append("}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t  ");
      _builder.append("h");
      _builder.append(h2, "\t\t\t\t  ");
      _builder.append(" {counter-reset: h");
      _builder.append(h3, "\t\t\t\t  ");
      _builder.append("}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t  ");
      _builder.append("h");
      _builder.append(h3, "\t\t\t\t  ");
      _builder.append(" {counter-reset: h");
      _builder.append(h4, "\t\t\t\t  ");
      _builder.append("}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t  ");
      _builder.append("h");
      _builder.append(h4, "\t\t\t\t  ");
      _builder.append(" {counter-reset: h");
      _builder.append(h5, "\t\t\t\t  ");
      _builder.append("}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t  ");
      _builder.append("h");
      _builder.append(h5, "\t\t\t\t  ");
      _builder.append(" {counter-reset: h");
      _builder.append(h6, "\t\t\t\t  ");
      _builder.append("}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t  ");
      _builder.append("h");
      _builder.append(h6, "\t\t\t\t  ");
      _builder.append(" {counter-reset: h");
      _builder.append(h7, "\t\t\t\t  ");
      _builder.append("}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t");
      _builder.newLine();
      _builder.append("\t\t\t\t  ");
      _builder.append("h");
      _builder.append(h2, "\t\t\t\t  ");
      _builder.append(":before {counter-increment: h");
      _builder.append(h2, "\t\t\t\t  ");
      _builder.append("; content: counter(h");
      _builder.append(h2, "\t\t\t\t  ");
      _builder.append(") \". \"}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t  ");
      _builder.append("h");
      _builder.append(h3, "\t\t\t\t  ");
      _builder.append(":before {counter-increment: h");
      _builder.append(h3, "\t\t\t\t  ");
      _builder.append("; content: counter(h");
      _builder.append(h2, "\t\t\t\t  ");
      _builder.append(") \".\" counter(h");
      _builder.append(h3, "\t\t\t\t  ");
      _builder.append(") \". \"}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t  ");
      _builder.append("h");
      _builder.append(h4, "\t\t\t\t  ");
      _builder.append(":before {counter-increment: h");
      _builder.append(h4, "\t\t\t\t  ");
      _builder.append("; content: counter(h");
      _builder.append(h2, "\t\t\t\t  ");
      _builder.append(") \".\" counter(h");
      _builder.append(h3, "\t\t\t\t  ");
      _builder.append(") \".\" counter(h");
      _builder.append(h4, "\t\t\t\t  ");
      _builder.append(") \". \"}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t  ");
      _builder.append("h");
      _builder.append(h5, "\t\t\t\t  ");
      _builder.append(":before {counter-increment: h");
      _builder.append(h5, "\t\t\t\t  ");
      _builder.append("; content: counter(h");
      _builder.append(h2, "\t\t\t\t  ");
      _builder.append(") \".\" counter(h");
      _builder.append(h3, "\t\t\t\t  ");
      _builder.append(") \".\" counter(h");
      _builder.append(h4, "\t\t\t\t  ");
      _builder.append(") \".\" counter(h");
      _builder.append(h5, "\t\t\t\t  ");
      _builder.append(") \". \"}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t  ");
      _builder.append("h");
      _builder.append(h6, "\t\t\t\t  ");
      _builder.append(":before {counter-increment: h");
      _builder.append(h6, "\t\t\t\t  ");
      _builder.append("; content: counter(h");
      _builder.append(h2, "\t\t\t\t  ");
      _builder.append(") \".\" counter(h");
      _builder.append(h3, "\t\t\t\t  ");
      _builder.append(") \".\" counter(h");
      _builder.append(h4, "\t\t\t\t  ");
      _builder.append(") \".\" counter(h");
      _builder.append(h5, "\t\t\t\t  ");
      _builder.append(") \".\" counter(h");
      _builder.append(h6, "\t\t\t\t  ");
      _builder.append(") \". \"}");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t");
      _builder.newLine();
      _builder.append("\t\t\t\t  ");
      _builder.append("h");
      _builder.append(h2, "\t\t\t\t  ");
      _builder.append(".nocount:before, h");
      _builder.append(h3, "\t\t\t\t  ");
      _builder.append(".nocount:before, h");
      _builder.append(h4, "\t\t\t\t  ");
      _builder.append(".nocount:before, h");
      _builder.append(h5, "\t\t\t\t  ");
      _builder.append(".nocount:before, h");
      _builder.append(h6, "\t\t\t\t  ");
      _builder.append(".nocount:before { content: \"\"; counter-increment: none } ");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t");
      _builder.append("</style>");
      _builder.newLine();
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  public int startingHeadingLevel(final Document document) {
    int _xifexpression = (int) 0;
    int _firstHeadingLevel = document.getFirstHeadingLevel();
    boolean _greaterThan = (_firstHeadingLevel > 0);
    if (_greaterThan) {
      _xifexpression = document.getFirstHeadingLevel();
    } else {
      _xifexpression = 1;
    }
    return _xifexpression;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
    for (ProjectDescriptor project : getProjects()) {
    	project.initialize(injector);
    }
    for (IGeneratorFragment f : getFragments()) {
    	f.initialize(injector);
    	//injector.injectMembers(f);
    }
    if (this.depth == null) {
    	this.depth = initDepth();
    }
  }
  
  @Override
  public void doGenerate() {
  }
}
