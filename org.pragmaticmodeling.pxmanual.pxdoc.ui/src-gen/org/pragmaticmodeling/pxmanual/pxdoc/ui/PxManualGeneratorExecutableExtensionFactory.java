package org.pragmaticmodeling.pxmanual.pxdoc.ui;

import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import fr.pragmaticmodeling.pxdoc.runtime.eclipse.guice.AbstractGuiceAwareExecutableExtensionFactory;

public class PxManualGeneratorExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	   protected Bundle getBundle() {
	       return PxManualGeneratorActivator.getDefault().getBundle();
	   }
	    
	   @Override
	   protected Injector getInjector() {
	       return PxManualGeneratorActivator.getInstance().getInjector();
	   }
	   
}
	
