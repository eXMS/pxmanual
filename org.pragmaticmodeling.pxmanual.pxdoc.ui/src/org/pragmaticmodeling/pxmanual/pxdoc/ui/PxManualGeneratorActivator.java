package org.pragmaticmodeling.pxmanual.pxdoc.ui;
		
import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;

import com.google.inject.Guice;

import fr.pragmaticmodeling.common.guice.Modules2;
import org.pragmaticmodeling.pxmanual.pxdoc.PxManualGeneratorModule;
import fr.pragmaticmodeling.pxdoc.runtime.eclipse.AbstractPxGuiceAwarePlugin;
import fr.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxGuiceAwareUiPlugin;
		
/**
* The activator class controls the plug-in life cycle
*/
public class PxManualGeneratorActivator extends AbstractPxGuiceAwareUiPlugin {
	
	// The plug-in ID
	public static final String PLUGIN_ID = "org.pragmaticmodeling.pxmanual.pxdoc.ui";
	public static final String GENERATOR_PLUGIN_ID = "org.pragmaticmodeling.pxmanual.pxdoc";
	
	private static Logger logger = Logger.getLogger(AbstractPxGuiceAwarePlugin.class);
	
	private static PxManualGeneratorActivator instance;
	
			
	/**
	 * The constructor
	 */
	public PxManualGeneratorActivator() {
		super();
		instance = this;
	}
	
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		try {
			setInjector(Guice.createInjector(Modules2.mixin(new PxManualGeneratorUiModule(this), new PxManualGeneratorModule())));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
	}
	
	protected String getPluginId() {
		return PLUGIN_ID;	
	}
	
	public String getPxDocGeneratorPluginId() {
		return GENERATOR_PLUGIN_ID;	
	}
	
	public static PxManualGeneratorActivator getInstance() {
		return instance;
	}
		
}