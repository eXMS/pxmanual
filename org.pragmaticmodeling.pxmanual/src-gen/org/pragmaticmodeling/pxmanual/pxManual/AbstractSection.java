/**
 * generated by Xtext 2.11.0
 */
package org.pragmaticmodeling.pxmanual.pxManual;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.pragmaticmodeling.pxmanual.pxManual.AbstractSection#getName <em>Name</em>}</li>
 *   <li>{@link org.pragmaticmodeling.pxmanual.pxManual.AbstractSection#getTitle <em>Title</em>}</li>
 *   <li>{@link org.pragmaticmodeling.pxmanual.pxManual.AbstractSection#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see org.pragmaticmodeling.pxmanual.pxManual.PxManualPackage#getAbstractSection()
 * @model
 * @generated
 */
public interface AbstractSection extends ManualElement
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.pragmaticmodeling.pxmanual.pxManual.PxManualPackage#getAbstractSection_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.pragmaticmodeling.pxmanual.pxManual.AbstractSection#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Title</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Title</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Title</em>' containment reference.
   * @see #setTitle(Text)
   * @see org.pragmaticmodeling.pxmanual.pxManual.PxManualPackage#getAbstractSection_Title()
   * @model containment="true"
   * @generated
   */
  Text getTitle();

  /**
   * Sets the value of the '{@link org.pragmaticmodeling.pxmanual.pxManual.AbstractSection#getTitle <em>Title</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Title</em>' containment reference.
   * @see #getTitle()
   * @generated
   */
  void setTitle(Text value);

  /**
   * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
   * The list contents are of type {@link org.pragmaticmodeling.pxmanual.pxManual.ManualElement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elements</em>' containment reference list.
   * @see org.pragmaticmodeling.pxmanual.pxManual.PxManualPackage#getAbstractSection_Elements()
   * @model containment="true"
   * @generated
   */
  EList<ManualElement> getElements();

} // AbstractSection
