package org.pragmaticmodeling.pxmanual.util

import java.util.List
import org.eclipse.emf.ecore.EObject
import org.pragmaticmodeling.pxmanual.pxManual.Document
import org.pragmaticmodeling.pxmanual.pxManual.Page
import org.eclipse.xtext.EcoreUtil2

class PxManualHelper {
	
	
	def public static List<Page> getPages(Document doc) {
		doc.body.elements.filter(Page).toList
	}
	
	def static Document getParentDocument(EObject eObject) {
		return EcoreUtil2.getContainerOfType(eObject, Document)
	}
	
	def static String title(EObject eObject) {
		switch eObject {
			Document: {
				return eObject.title.body
			}
			Page: {
				return eObject.title.body
			}
		}
		return "FIXME"
	}
}