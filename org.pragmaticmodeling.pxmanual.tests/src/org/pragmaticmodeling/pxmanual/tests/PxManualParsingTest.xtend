/*
 * generated by Xtext 2.11.0
 */
package org.pragmaticmodeling.pxmanual.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.pragmaticmodeling.pxmanual.pxManual.Document

@RunWith(XtextRunner)
@InjectWith(PxManualInjectorProvider)
class PxManualParsingTest {
	@Inject
	ParseHelper<Document> parseHelper
	
	@Test
	def void loadModel() {
		val result = parseHelper.parse('''
			document {
				
				
			}
		''')
		Assert.assertNotNull(result)
		Assert.assertTrue(result.eResource.errors.isEmpty)
	}
}
