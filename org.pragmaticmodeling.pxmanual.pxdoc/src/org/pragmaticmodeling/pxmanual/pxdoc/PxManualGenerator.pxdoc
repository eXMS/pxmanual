package org.pragmaticmodeling.pxmanual.pxdoc

import fr.pragmaticmodeling.pxdoc.runtime.IPxDocRendererExtension
import java.io.File
import java.util.List
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.IWorkspaceRoot
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.Path
import org.pragmaticmodeling.pxmanual.pxManual.AbstractExample
import org.pragmaticmodeling.pxmanual.pxManual.AbstractSection
import org.pragmaticmodeling.pxmanual.pxManual.Cell
import org.pragmaticmodeling.pxmanual.pxManual.DocBody
import org.pragmaticmodeling.pxmanual.pxManual.Document
import org.pragmaticmodeling.pxmanual.pxManual.Example
import org.pragmaticmodeling.pxmanual.pxManual.Image
import org.pragmaticmodeling.pxmanual.pxManual.Keyword
import org.pragmaticmodeling.pxmanual.pxManual.KeywordAttribute
import org.pragmaticmodeling.pxmanual.pxManual.ManualElement
import org.pragmaticmodeling.pxmanual.pxManual.Page
import org.pragmaticmodeling.pxmanual.pxManual.Row
import org.pragmaticmodeling.pxmanual.pxManual.SamePage
import org.pragmaticmodeling.pxmanual.pxManual.Section
import org.pragmaticmodeling.pxmanual.pxManual.Table
import org.pragmaticmodeling.pxmanual.pxManual.Text
import org.pragmaticmodeling.pxmanual.pxManual.Usage

/**
 * PxManualGenerator empty pxDoc generator. 
 */
pxDocGenerator PxManualGenerator stylesheet:RefDoc
{ 
  
	var String modelDirectory
	var IPxDocRendererExtension htmlExtension = new HtmlExtension(generator)
	/** 
	 * Entry point. 
	 */
	root template main(Document model) {
		modelDirectory = model.modelDirectory

		document attachedTemplate:'C:/Users/Public/pxdoc.dotm'{
			//subDocument 'C:/Users/thoms/Documents/Professionnel/eXms/Tutos Doc de Reference etc/Reference Guide/ReferenceGuide resources/1ere page et stylesheet/firstpage.docx'
			subDocument 'templates/firstpage.docx'
			newPage
			toc
			§
			apply toDoc(model.body.elements, 1)

		}
	}

	function String getModelDirectory(Document model) {
		var String projectDir = null
		var uri = model.eResource.URI
		if (uri.hasAbsolutePath) {
			var File file = null
			if (uri.isPlatformResource) {
				var IWorkspaceRoot lWR = ResourcesPlugin.getWorkspace().getRoot();
				var String path = uri.toPlatformString(true);
				var Path _path = new Path(path);
				var IResource ifile = lWR.findMember(_path);
				file = new File(ifile.location.toString)
			} else {
				file = new File(uri.toFileString)
			}
			projectDir = getParentProjectDirectory(file);
		}
		projectDir
	}

	function String getParentProjectDirectory(File file) {
		var parentDir = file.parentFile
		if (parentDir !== null) {
			if (getProjectFile(parentDir) !== null) {
				return parentDir.absolutePath
			} else {
				return getParentProjectDirectory(parentDir)
			}

		} else {
			return null;
		}
	}

	function File getProjectFile(File directory) {
		for (file : directory.listFiles) {
			if (file.name == ".project") {
				return file
			}
		}
		return null
	}

	template toDoc(ManualElement element) {
		apply toDoc(element, 0)
	}

	template toDoc(ManualElement element, int level) {
		switch element {
			Text: {
				apply toText(element)
			}
			Page: {
				apply toTitle(element, level)
			}
			Section: {
				apply toTitle(element, level)
			}
			Image: {
				apply toImage(element)
			}
			Keyword: {
				apply toKeyword(element, level)
			}
			Usage: {
				apply toUsageExample(element)
			}
			Table: {
				table borders(size:0) {
					apply tableRow(element.rows)
				}
			}
			SamePage: {
				samePage {
					apply toDoc(element.body)
				}
			}
		}
	}

	template tableRow(Row tableRow) {
		row {
			apply tableCell(tableRow.cells)
		}
	}

	template tableCell(Cell tableCell) {
		cell {
			apply toDoc(tableCell.body)
		}
	}

	template toKeyword(Keyword element, int level) {
		HeadingN level:level { bookmark element.keywordName { apply title(element) } }
		if (element.attributes.empty) {
			apply toBasicExample(element.basicExamples) separator {§ }
		}
		if (element.documentation !== null) {
			apply toDoc(element.documentation, level)
		}
		if (!element.attributes.empty) {
			apply toBasicExample(element.basicExamples) separator {§ }
			table [ width:30pc | width:25pc | width:30pc | width:15pc ] {
				row header shadingColor:"0,0,0" {
					cell {"Attributes" }
					cell {"Description" }
					cell {"Possible values (if any)" }
					cell {"Optional" }
				}
				apply attributeRow(element.attributes)
			}
			§
		}
		if (element.conclusion !== null) {
			apply toDoc(element.conclusion, level)
		}
		var usageExamples = element.usageExamples
		if (!usageExamples.empty) {
			apply toUsageExample(usageExamples)
		}
	}

	function String toBookmarkId(String rawName) {
	}

	template title(Keyword keyword) {
		if (keyword.prettyTitle !== null) {
			apply toText(keyword.prettyTitle)
		} else {
			"Keyword '"
			keyword.keywordName
			"'"
		}
	}

	function List<Usage> getUsageExamples(Keyword keyword) {
		keyword.examples.filter(Usage).toList
	}

	template toUsageExample(Usage e) {
		#UsageOfpxDoc keepWithNext {e.title.body }
		apply toDoc(e.body.elements)
		§
	}

	template attributeRow(KeywordAttribute attribute) {
		row {
			cell { apply toBasicExample(attribute.example) }
			cell [ merge:automatic] { apply toDoc(attribute.documentation) }
			cell [ merge:automatic] { apply toDoc(attribute.possibleValues) }
			cell {if(attribute.optional) "Yes" else "No" }
		}
	}

	template toDoc(DocBody docBody) {
		if (docBody !== null) {
			apply toDoc(docBody.elements, -1)
		}
	}

	template toDoc(DocBody docBody, int level) {
		apply toDoc(docBody.elements, level)
	}

	template toBasicExample(AbstractExample basicExample) {
		apply toDoc(basicExample.body.elements)
	}

	function List<Example> getBasicExamples(Keyword keyword) {
		keyword.examples.filter(Example).toList
	}

	template toImage(Image element) {
		image path:modelDirectory + "/" + element.path
		§
	}

	template toTitle(AbstractSection element, int level) {
		HeadingN level:level { apply toDoc(element.title) }
		apply toDoc(element.elements, level + 1)
	}

	template toText(Text element) {
		if (element.^language == "html") {
			if (element.className == "note") {
				§
				#Quote {language html extension:htmlExtension element.body }
				§
			} else if (element.className == "citation") {
				apply toCitation(element, true)
			} else {
				language html extension:htmlExtension element.body
			}
		} else if (element.^language == "pxdoc") {
			language pxdoc element.body.trimLastNewLine
		} else {
			if (element.className == "note") {
				§
				#Quote {element.body }
				§
			} else if (element.className == "citation") {
				apply toCitation(element, false)
			} else {
				element.body
				§ // FIXME #Normal {element.body} => si appelé depuis toTitle(Section) => deux SA imbriqués (HeadingN contient Normal => 2 paragraphes imbriqués)
			}
		}
	}

	function String trimLastNewLine(String text) {
		var result = text
		if (text.endsWith("\n"))
			result = text.substring(0, text.length - 1)
		result
	}

	template toCitation(Text element, boolean html) {
		table borders(size:0) shadingColor: "192,192,192"{
			row {
				cell {
					italic {
						if (html) {
							language html extension:htmlExtension element.body
						} else {
							element.body
						}
					}
				}
			}
		}
	}

}
