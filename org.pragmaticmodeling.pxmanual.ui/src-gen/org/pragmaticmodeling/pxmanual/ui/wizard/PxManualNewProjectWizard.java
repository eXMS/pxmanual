/*
 * generated by Xtext 2.11.0
 */
package org.pragmaticmodeling.pxmanual.ui.wizard;

import org.eclipse.xtext.ui.wizard.XtextNewProjectWizard;

import org.eclipse.xtext.ui.wizard.IExtendedProjectInfo;
import org.eclipse.xtext.ui.wizard.IProjectCreator;
import com.google.inject.Inject;

public class PxManualNewProjectWizard extends XtextNewProjectWizard {

	private PxManualWizardNewProjectCreationPage mainPage;

	@Inject
	public PxManualNewProjectWizard(IProjectCreator projectCreator) {
		super(projectCreator);
		setWindowTitle("New PxManual Project");
	}

	protected PxManualWizardNewProjectCreationPage getMainPage() {
		return mainPage;
	}

	/**
	 * Use this method to add pages to the wizard.
	 * The one-time generated version of this class will add a default new project page to the wizard.
	 */
	@Override
	public void addPages() {
		mainPage = createMainPage("basicNewProjectPage");
		mainPage.setTitle("PxManual Project");
		mainPage.setDescription("Create a new PxManual project.");
		addPage(mainPage);
	}

	protected PxManualWizardNewProjectCreationPage createMainPage(String pageName) {
		return new PxManualWizardNewProjectCreationPage(pageName);
	}

	/**
	 * Use this method to read the project settings from the wizard pages and feed them into the project info class.
	 */
	@Override
	protected IExtendedProjectInfo getProjectInfo() {
		PxManualProjectInfo projectInfo = new PxManualProjectInfo();
		projectInfo.setProjectName(mainPage.getProjectName());
		if (!mainPage.useDefaults()) {
			projectInfo.setLocationPath(mainPage.getLocationPath());
		}
		return projectInfo;
	}

}
